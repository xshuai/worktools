## 工程介绍

一些笔试题整理、工作中测试的示例代码

![码小帅](https://images.gitee.com/uploads/images/2021/0518/164321_b70506da_131538.jpeg "码小帅")


#### 百度AI

|![高清人像美化](https://images.gitee.com/uploads/images/2021/0708/110728_65ee02a8_131538.png "高清人像美化.png") [ 高清人像美化](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/sample/aisample/baidu/face/FaceBeautySample.java)   | ![车辆检测](https://images.gitee.com/uploads/images/2021/0723/105444_49defd63_131538.png "车辆检测.png") [车辆检测](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/sample/aisample/baidu/icr/VehicleDetectSample.java)  |![皮肤分析](https://images.gitee.com/uploads/images/2021/0726/120500_59daf29d_131538.png "皮肤分析.png")[皮肤分析](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/sample/aisample/baidu/face/SkinAnalyzeSample.java)  |
|---|---|---|



#### 工作小记录(小功能示例代码)

| ![DOC转PDF](https://images.gitee.com/uploads/images/2021/0708/134708_1facd0f0_131538.png "DOC转PDF.png") [DOC转PDF](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/DocToPDFSample.java)  | ![PDF转图片](https://images.gitee.com/uploads/images/2021/0708/134803_6f5226fa_131538.png "PDF转图片.png") [PDF转图片](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/PDFToImageSample.java)  | ![PDF骑缝章(Spire.PDF)](https://images.gitee.com/uploads/images/2021/0708/134822_3facda79_131538.png "PDF骑缝章(Spire.PDF).png") [PDF骑缝章(Spire.PDF)](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/PDFPagingSealSample.java)  |![PDF骑缝章(iText)](https://images.gitee.com/uploads/images/2021/0708/134822_3facda79_131538.png "PDF骑缝章(iText).png") [PDF骑缝章(iText)](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/PDFPagingSealiTextSample.java)  |
|---|---|---|---|
|![读取图片EXIF信息](https://images.gitee.com/uploads/images/2021/0708/182519_dcc4bdad_131538.png "读取图片EXIF信息.png") **[EXIF信息](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/ReadImageEXIFSample.java)** |![WEBP转换](https://images.gitee.com/uploads/images/2021/0708/182641_46b48ec3_131538.png "WEBP转换.png") **[WEBP转换](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/WebpSample.java)**  | ![MinIO](https://images.gitee.com/uploads/images/2021/0709/110041_1b7babdf_131538.png "MinIO.png")**[MinIO](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/sample/miniosample/MinIOSample.java)**|![信息读取](https://images.gitee.com/uploads/images/2021/0723/171247_56deb9ea_131538.png "信息读取.png") **[APK信息读取](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/AnalyticApkSample.java)** |
|![TrueLicense](https://images.gitee.com/uploads/images/2021/0813/112803_73bb5283_131538.png "license.png") **[TrueLicense](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/truelicense/TrueLicenseSample.java)** |![滴滴发票出行报销单合并成一个PDF](https://images.gitee.com/uploads/images/2021/0708/134803_6f5226fa_131538.png)**[滴滴发票行程单合并](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/pdf/PDFMergerSample.java)**| ![滑块验证码生成](src/main/resources/file/verifycode.png)**[滑块验证码生成](https://gitee.com/xshuai/worktools/blob/master/src/main/java/cn/ydxiaoshuai/workrecord/slideverifycode/SlideVerifyCodeGenerateUtil.java)**|-|




