package cn.ydxiaoshuai.workrecord;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * @author 小帅丶
 * @className PDFPagingSealiTextSample
 * @Description 骑缝章
 * @Date 2021-07-08-12:11
 **/
@Slf4j
public class PDFPagingSealiTextSample {

    /**
     * @Author 小帅丶
     * @Description PDF增加骑缝章
     * @Date  2021-07-08 12:53
     * @param source_pdf - PDF源文件
     * @param seal_path - 印章文件
     * @param target_pdf - 目标文件
     * @return void
     **/
    public static void generalPagingSeal(String source_pdf,String seal_path,String target_pdf){
        try {
            PdfReader sourcePDF = new PdfReader(source_pdf);
            PdfStamper targetPDF = new PdfStamper(sourcePDF,new FileOutputStream(target_pdf));
            //获得第一页 以及宽高
            Rectangle pageSize = sourcePDF.getPageSize(1);
            float width  = pageSize.getWidth();
            float height = pageSize.getHeight();

            int nums = sourcePDF.getNumberOfPages();

            //获取分割后的印章图片
            Image[] images = getSealImage(nums,
                    seal_path);
            for (int i = 0; i < nums; i++) {
                //页数从1开始
                PdfContentByte over = targetPDF.getOverContent(i+1);
                Image image = images[i];
                //控制图片位置-必须
                image.setAbsolutePosition(width-image.getWidth(),
                        height/2-image.getHeight()/2);
                over.addImage(image);
            }
            targetPDF.close();
        }catch (Exception e){
            log.info("generalPagingSeal() 出错了{}",e.getMessage());
        }
    }

    /**
     * @Author 小帅丶
     * @Description 定义getSealImage方法，根据PDF页数分割印章图片
     * @Date  2021-07-08 12:45
     * @param num - PDF页数
     * @param sealPath - 印章图片路径
     * @return com.itextpdf.text.Image[]
     **/
    public static Image[] getSealImage(int num, String sealPath) {
        try {
            String prefix = sealPath.substring(sealPath.lastIndexOf('.') + 1);
            Image[] nImage = new Image[num];
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            BufferedImage bufferedImage = ImageIO.read(new File(sealPath));
            int h = bufferedImage.getHeight();
            int w = bufferedImage.getWidth();

            int sw = w/num;

            for (int i = 0; i < num; i++) {
                BufferedImage subImg;
                //最后剩余部分
                if(i==num-1){
                    subImg = bufferedImage.getSubimage(i*sw,
                            0,
                            w-i*sw,
                            h);
                //前n-1快均匀分割印章
                }else{
                    subImg = bufferedImage.getSubimage(i*sw,
                            0,
                            sw,
                            h);
                }
                ImageIO.write(subImg,prefix, out);
                nImage[i] = Image.getInstance(out.toByteArray());
                out.flush();
                out.reset();
            }
            return nImage;
        }catch (Exception e){
            log.info("getSealImage() 出错了\t{}",e.getMessage());
            return null;
        }
    }
}
