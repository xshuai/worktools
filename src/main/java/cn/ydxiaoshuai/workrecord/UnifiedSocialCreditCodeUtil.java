package cn.ydxiaoshuai.workrecord;

import cn.hutool.core.util.CreditCodeUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Description 统一社会信用代码
 * ProjectName dtns-finance-bill
 * Created by 小帅丶 on 2023-02-01 13:47.
 * Version 1.0
 */

public class UnifiedSocialCreditCodeUtil {

    @Data
    public static class SlideVerifyCode {
        /**
         * 随机字符串
         **/
        private String nonceStr;
        /**
         * 验证值
         **/
        private String value;
        /**
         * 生成的画布的base64
         **/
        private String canvasSrc;
        /**
         * 画布宽度
         **/
        private Integer canvasWidth;
        /**
         * 画布高度
         **/
        private Integer canvasHeight;
        /**
         * 生成的阻塞块的base64
         **/
        private String blockSrc;
        /**
         * 阻塞块宽度
         **/
        private Integer blockWidth;
        /**
         * 阻塞块高度
         **/
        private Integer blockHeight;
        /**
         * 阻塞块凸凹半径
         **/
        private Integer blockRadius;
        /**
         * 阻塞块的横轴坐标
         **/
        private Integer blockX;
        /**
         * 阻塞块的纵轴坐标
         **/
        private Integer blockY;
        /**
         * 图片获取位置
         **/
        private Integer place;
    }


    private static final Logger logger = LoggerFactory.getLogger(UnifiedSocialCreditCodeUtil.class);
    /*
     * 从唯一、统一、共享、便民和低成本转换等角度综合考虑，统一代码设计为18位
     * @Date  2023/2/1 13:59
     **/
    private static final Integer MIN_LENGTH = 18;

    private static final String ERR_CODE = "216201";

    private static final String ERR_MSG = "统一社会信用代码 位数错误";

    /**
     * 根据 统一社会信用代码 获取所属省份
     * @param unifiedSocialCreditCode - 统一社会信用代码
     * @Author 小帅丶
     * @Date  2023/2/1 13:49
     * @return java.lang.String
     **/
    public static String getProvinceNameByUnifiedSocialCreditCode(String unifiedSocialCreditCode) throws Exception {
        boolean creditCode = CreditCodeUtil.isCreditCode(unifiedSocialCreditCode);
        logger.info("【】 【getProvinceByUnifiedSocialCreditCode】======> \t 是否是有效:{} \t 代码长度:{}",
                creditCode,unifiedSocialCreditCode.length());
        if(unifiedSocialCreditCode.length()<MIN_LENGTH){
            throw new Exception(ERR_MSG);
        }
        //第一部分（第1位）：登记管理部门代码，使用阿拉伯数字或英文字母表示。
        // 例如，机构编制、民政、工商三个登记管理部门分别使用 1、2、3表示，其他登记管理部门可使用相应阿拉伯数字或英文字母表示。

        //第二部分（第2位）：机构类别代码，使用阿拉伯 数字或英文字母表示。登记管理部门根据管理职能，确定在本部门登记的机构类别编码。
        // 例如，机构编制部门可用1表示机关单位，2表 示事业单位，3表示由中央编办直接管理机构编制的群众团体；民政部门可用1表示社会团体，
        // 2表示民办非企业单位，3表示基金会；工 商部门可用1表示企业，2表示个体工商户，3表示农民专业合作社。
        String provinceCode = unifiedSocialCreditCode.substring(2,4);
        //第三部分（第3—8位）：登记管理机关行政区划码，使 用阿拉伯数字表示。
        // 例如，国家用100000，北京用110000，注册登记时由系统自动生成，
        // 体现法人和其他组织注册登记及其登记管理机 关所在地，既满足登记管理部门按地区管理需求，也便于社会对注册登记主体所在区域进行识别。
        // （参照《中华人民共和国行政区划代 码》〔GB/T 2260—2007〕）

        //第四部分（第9—17位）：主体标识码（组织机构代码），使用阿拉伯数字或英文字母表示。
        // （参照《全国组织机构代码编制规则》〔GB 11714—1997〕）

        //第五部分（第18位）：校验码，使用阿拉伯数字或英文字母 表示。
        String provinceNameByCode = PROVINCE_NAME.getProvinceNameByCode(provinceCode);
        if(StrUtil.isEmpty(provinceNameByCode)){
            return "未标记地区";
        }else{
            return provinceNameByCode;
        }
    }

    //省份编码
    private enum PROVINCE_NAME{
        BEIJING("11","北京"),
        TIANJIN("12","天津"),
        HEBEI("13","河北"),
        SHANXI("14","山西"),
        NMG("15","内蒙古"),

        LN("21","辽宁"),
        JL("22","吉林"),
        HLJ("23","黑龙江"),

        SH("31","上海"),
        JS("32","江苏"),
        ZJ("33","浙江"),
        AH("34","安徽"),
        FJ("35","福建"),
        JX("36","江西"),
        SD("37","山东"),

        HENAN("41","河南"),
        HUBEI("42","湖北"),
        HUNAN("43","湖南"),
        GD("44","广东"),
        GX("45","广西"),
        HAINAN("46","海南"),

        CQ("50","重庆"),
        SC("51","四川"),
        GZ("52","贵州"),
        YN("53","云南"),
        ZX("54","西藏"),

        SHANNXI("61","陕西"),
        GS("62","甘肃"),
        QH("63","青海"),
        NX("64","宁夏"),
        XJ("65","新疆"),

        TW("71","台湾"),
        XG("81","香港"),
        AM("82","澳门"),

        HW("91","海外");
        private String provinceCode;
        private String provinceName;

        PROVINCE_NAME(String provinceCode, String provinceName) {
            this.provinceCode = provinceCode;
            this.provinceName = provinceName;
        }

        /**
         * @Description 根据2位代码查询所属省份
         * @Author 小帅丶
         * @Date  2023年2月1日14:23:40
         * @param provinceCode 省份代码
         * @return java.lang.String
         **/
        public static String getProvinceNameByCode(String provinceCode){
            for (PROVINCE_NAME provinceName : PROVINCE_NAME.values()) {
                if(provinceCode.equals(provinceName.getProvinceCode())){
                    return provinceName.getProvinceName();
                }
            }
            return null;
        }

        public String getProvinceCode() {
            return provinceCode;
        }

        public void setProvinceCode(String provinceCode) {
            this.provinceCode = provinceCode;
        }

        public String getProvinceName() {
            return provinceName;
        }

        public void setProvinceName(String provinceName) {
            this.provinceName = provinceName;
        }
    }
}
