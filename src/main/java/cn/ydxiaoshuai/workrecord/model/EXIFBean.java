package cn.ydxiaoshuai.workrecord.model;

import lombok.Data;

/**
 * @author 小帅丶
 * @className EXIFBean
 * @Description Exif对象
 * @Date 2021-07-08-15:23
 **/
@Data
public class EXIFBean {
    /**
     * GPS Version ID-GPS版本
     */
    private String GPS_VERSION_ID;
    /**
     * Make-厂商
     */
    private String MAKE;
    /**
     * Model-机型
     */
    private String MODEL;
    /**
     * Orientation-方向
     */
    private String ORIENTATION;
    /**
     * Software-软件
     */
    private String SOFTWARE;
    /**
     * Date/Time-修改时间
     */
    private String DATE;
    /**
     * Artist-作者
     */
    private String ARTIST;
    /**
     * YCbCr Positioning-YcbCr定位
     */
    private String YCBCR_POSITIONING;
    /**
     * Exposure Time-曝光时间
     */
    private String EXPOSURE_TIME;
    /**
     * F-Number-光圈
     */
    private String F_NUMBER;
    /**
     * Exposure Program-曝光程序
     */
    private String EXPOSURE_PROGRAM;
    /**
     * ISO Speed Ratings-ISO感光度
     */
    private String ISO_SPEED_RATINGS;
    /**
     * Exif Version-Exif版本
     */
    private String EXIF_VERSION;
    /**
     * Date/Time Original-拍摄时间
     */
    private String DATE_ORIGINAL;
    /**
     * Date/Time Digitized-数字化时间
     */
    private String DATE_DIGITIZED;
    /**
     * Components Configuration-成分构成
     */
    private String COMPONENTS_CONFIGURATION;
    /**
     * Shutter Speed Value-快门速度
     */
    private String SHUTTER_SPEED_VALUE;
    /**
     * Aperture Value-光圈值
     */
    private String APERTURE_VALUE;
    /**
     * Exposure Bias Value-曝光补偿
     */
    private String EXPOSURE_BIAS_VALUE;
    /**
     * Max Aperture Value-最大光圈
     */
    private String MAX_APERTURE_VALUE;
    /**
     * Metering Mode-测光模式
     */
    private String METERING_MODE;
    /**
     * Flash-闪光
     */
    private String FLASH;
    /**
     * Focal Length-焦距
     */
    private String FOCAL_LENGTH;
    /**
     * User Comment-用户注释
     */
    private String USER_COMMENT;
    /**
     * Sub-Sec Time-次秒（修改时间）
     */
    private String SUB_SEC_TIME;
    /**
     * Sub-Sec Time Original-次秒（拍摄时间）
     */
    private String SUB_SEC_TIME_ORIGINAL;
    /**
     * Sub-Sec Time Digitized-次秒（数字化时间）
     */
    private String SUB_SEC_TIME_DIGITIZED;
    /**
     * FlashPix Version-FlashPix版本
     */
    private String FLASHPIX_VERSION;
    /**
     * Color Space-色彩空间
     */
    private String COLOR_SPACE;
    /**
     * Exif Image Width-Exif图像宽度
     */
    private String EXIF_IMAGE_WIDTH;
    /**
     * Exif Image Height-Exif图像高度
     */
    private String EXIF_IMAGE_HEIGHT;
    /**
     * Focal Plane X Resolution-焦平面水平分辨率
     */
    private String FOCAL_PLANE_X_RESOLUTION;
    /**
     * Focal Plane Y Resolution-焦平面垂直分辨率
     */
    private String FOCAL_PLANE_Y_RESOLUTION;
    /**
     * Focal Plane Resolution Unit-焦平分辨率单位
     */
    private String FOCAL_PLANE_RESOLUTION_UNIT;
    /**
     * Custom Rendered-自定义补偿
     */
    private String CUSTOM_RENDERED;
    /**
     * Exposure Mode-曝光模式
     */
    private String EXPOSURE_MODE;
    /**
     * White Balance Mode-白平衡
     */
    private String WHITE_BALANCE_MODE;
    /**
     * Scene Capture Type-场景拍摄类型
     */
    private String SCENE_CAPTURE_TYPE;
    /**
     * Interoperability Index-可交换标准
     */
    private String INTEROPERABILITY_INDEX;
    /**
     * Interoperability Version-可交换版本
     */
    private String INTEROPERABILITY_VERSION;
    /**
     * Thumbnail Compression-压缩模式
     */
    private String THUMBNAIL_COMPRESSION;
    /**
     * Thumbnail Offset-JPEG缩略图起始位置
     */
    private String THUMBNAIL_OFFSET;
    /**
     * Thumbnail Length-JPEG缩略图数据长度
     */
    private String THUMBNAIL_LENGTH;
    /**
     * X Resolution-水平分辨率
     */
    private String X_RESOLUTION;
    /**
     * Y Resolution-垂直分辨率
     */
    private String Y_RESOLUTION;
    /**
     * Resolution Unit-分辨率单位
     */
    private String RESOLUTION_UNIT;
    /**
     * Lens Information-镜头信息
     */
    private String LENS_INFORMATION;
    /**
     * Lens-镜头
     */
    private String LENS;

}
