package cn.ydxiaoshuai.workrecord.model;

import lombok.Data;

/**
 * @author 小帅丶
 * @className OlympicMedals
 * @Description 奥运奖牌
 * @Date 2021-07-26-13:55
 **/
@Data
public class OlympicMedals{
    /** 金牌 */
    private Integer gold_medal;
    /** 银牌 */
    private Integer silver_medal;
    /** 铜牌 */
    private Integer bronze_medal;
    /** 国家名称 */
    private String  country_name;
    /** 国家英文名称 */
    private String  country_en_name;

    /** 并列排名 */
    private Integer  ranking_juxtapose;

    public OlympicMedals(Integer gold_medal, Integer silver_medal,
                         Integer bronze_medal, String country_name,String country_en_name) {
        this.gold_medal = gold_medal;
        this.silver_medal = silver_medal;
        this.bronze_medal = bronze_medal;
        this.country_name = country_name;
        this.country_en_name = country_en_name;
    }
    /** 总金牌计算 */
    public Integer allMedal() {
        Integer medal = gold_medal + silver_medal + bronze_medal;
        return medal;
    }
}
