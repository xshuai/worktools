package cn.ydxiaoshuai.workrecord;

import cn.ydxiaoshuai.workrecord.model.OlympicMedals;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 小帅丶
 * @className OlympicMedalsSample
 * @Description 奥运奖牌排序
 * @Date 2021-07-26-13:55
 **/
public class OlympicMedalsSample {
    public static void main(String[] args) {
        //全部国家数据
        List<OlympicMedals> olympicMedalsList = getMedalList();

        System.out.println("获的奖牌国家总计:" + olympicMedalsList.size());
        System.out.println("--------------------------奥运奖牌榜-------------------------");
        System.out.println("排名\t国家/地区\t\t\t\t金牌\t银牌\t铜牌\t总计");
        long startTime = System.currentTimeMillis();
        //进行奖牌排序
        List<OlympicMedals> collect = olympicMedalsList.stream()
                .sorted(Comparator.comparing(OlympicMedals::getGold_medal)
                        .thenComparing(OlympicMedals::getSilver_medal)
                        .thenComparing(OlympicMedals::getBronze_medal)
                        .reversed())
                .collect(Collectors.toList());
        //第一名名次开始值
        Integer rankingJuxtapose = 1;
        //进行排名计算，金银铜奖牌相同进行并列排名计算
        for (int i = 0; i < collect.size(); i++) {
            if (i == 0) {
                OlympicMedals medals = collect.get(i);
                medals.setRanking_juxtapose(rankingJuxtapose);
            } else {
                OlympicMedals medals = collect.get(i);
                OlympicMedals medalsPrevious = collect.get(i - 1);
                if (medals.getGold_medal().equals(medalsPrevious.getGold_medal()) &&
                        medals.getSilver_medal().equals(medalsPrevious.getSilver_medal()) &&
                        medals.getBronze_medal().equals(medalsPrevious.getBronze_medal())) {
                    medals.setRanking_juxtapose(medalsPrevious.getRanking_juxtapose());
                } else {
                    medals.setRanking_juxtapose(rankingJuxtapose);
                }
            }
            rankingJuxtapose++;
        }
        //对国家进行排序
        Comparator<Object> chinese = Collator.getInstance(Locale.ENGLISH);
        //根据排名分组
        Map<Integer, List<OlympicMedals>> sortMap = collect.stream().collect(Collectors.groupingBy(OlympicMedals::getRanking_juxtapose));
        //获取排名数据
        Set<Integer> integers = sortMap.keySet();
        //排名数据根据1-100进行排序
        Set<Integer> sortSet = new TreeSet<Integer>(Comparator.naturalOrder());
        sortSet.addAll(integers);
        //进行数据输出
        for (Integer integer : sortSet) {
            List<OlympicMedals> medalsList = sortMap.get(integer);
            if (medalsList.size() > 1) {
                //根据国家英文字母进行排序
                Collections.sort(medalsList, (c1, c2) -> {
                    if (chinese.compare(c1.getCountry_en_name(), c2.getCountry_en_name()) >= 0) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
            }
            medalsList.forEach(medals -> {
                String countryName;
                String country_name = medals.getCountry_name();
                if (country_name.length() <= 3) {
                    countryName = country_name + "\t\t\t";
                } else if (country_name.length() == 4) {
                    countryName = country_name + "\t\t\t";
                } else if (country_name.length() >= 5) {
                    countryName = country_name + "\t\t";
                } else {
                    countryName = country_name;
                }
                System.out.println(medals.getRanking_juxtapose() + "\t\t" + countryName +
                        "\t\t" + medals.getGold_medal() +
                        "\t\t" + medals.getSilver_medal() +
                        "\t\t" + medals.getBronze_medal() +
                        "\t\t" + medals.allMedal());
            });
        }
        long endTime = System.currentTimeMillis();
        System.out.println("排序耗时:"+(endTime-startTime));
    }

    /**
     * @return java.util.List<cn.ydxiaoshuai.workrecord.model.OlympicMedals>
     * @Author 小帅丶
     * @Description 国家奖牌数据-更新时间:2021年7月27日10:19:23
     * 只是做一个演示，具体数据根据各大平台进行修改即可
     * @Date 2021-07-26 18:44
     **/
    private static List<OlympicMedals> getMedalList() {
        long startTime = System.currentTimeMillis();
        //单个国家数据
        OlympicMedals china = new OlympicMedals(6, 5, 7, "中国", "CHN");
        OlympicMedals japan = new OlympicMedals(8, 2, 3, "日本", "JPN");
        OlympicMedals usa = new OlympicMedals(7, 3, 6, "美国", "USA");
        OlympicMedals yg = new OlympicMedals(4, 5, 1, "英国", "GBR");
        OlympicMedals korea = new OlympicMedals(3, 0, 4, "韩国", "KOR");
        OlympicMedals australia = new OlympicMedals(3, 1, 3, "澳大利亚", "AUS");
        OlympicMedals els = new OlympicMedals(4, 5, 3, "俄罗斯奥委会", "ROC");
        OlympicMedals ydl = new OlympicMedals(1, 4, 4, "意大利", "ITA");
        OlympicMedals jnd = new OlympicMedals(1, 3, 1, "加拿大", "CAN");
        OlympicMedals fg = new OlympicMedals(1, 2, 2, "法国", "FRA");
        OlympicMedals xyl = new OlympicMedals(1, 1, 0, "匈牙利", "HUN");
        OlympicMedals tns = new OlympicMedals(1, 1, 0, "突尼斯", "TUN");
        OlympicMedals adl = new OlympicMedals(1, 0, 0, "奥地利", "AUT");
        OlympicMedals egde = new OlympicMedals(1, 0, 0, "厄瓜多尔", "ECU");
        OlympicMedals yl = new OlympicMedals(1, 0, 0, "伊朗", "IRI");
        OlympicMedals ksw = new OlympicMedals(2, 0, 0, "科索沃", "KOS");
        OlympicMedals nw = new OlympicMedals(1, 0, 0, "挪威", "NOR");
        OlympicMedals tg = new OlympicMedals(1, 0, 0, "泰国", "THA");
        OlympicMedals wzbkst = new OlympicMedals(1, 0, 0, "乌兹别克斯坦", "UZB");
        OlympicMedals hl = new OlympicMedals(0, 3, 0, "荷兰", "NED");
        OlympicMedals bx = new OlympicMedals(0, 2, 2, "巴西", "BRA");
        OlympicMedals yn = new OlympicMedals(0, 1, 1, "印尼", "INA");
        OlympicMedals sewy = new OlympicMedals(0, 1, 1, "塞尔维亚", "SRB");
        OlympicMedals zgtb = new OlympicMedals(0, 2, 2, "中国台北", "TPE");
        OlympicMedals bls = new OlympicMedals(0, 1, 0, "比利时", "BEL");
        OlympicMedals bjly = new OlympicMedals(0, 1, 0, "保加利亚", "BUL");
        OlympicMedals glby = new OlympicMedals(0, 1, 0, "哥伦比亚", "COL");
        OlympicMedals xby = new OlympicMedals(0, 1, 1, "西班牙", "ESP");
        OlympicMedals gljy = new OlympicMedals(0, 2, 0, "格鲁吉亚", "GEO");
        OlympicMedals yd = new OlympicMedals(0, 1, 0, "印度", "IND");
        OlympicMedals lmny = new OlympicMedals(0, 1, 0, "罗马尼亚", "ROU");
        OlympicMedals dg = new OlympicMedals(0, 0, 3, "德国", "GER");
        OlympicMedals hskst = new OlympicMedals(0, 0, 3, "哈萨克斯坦", "KAZ");
        OlympicMedals teq = new OlympicMedals(0, 0, 2, "土耳其", "TUR");
        OlympicMedals wkl = new OlympicMedals(0, 0, 2, "乌克兰", "UKR");
        OlympicMedals asly = new OlympicMedals(0, 0, 1, "爱沙尼亚", "EST");
        OlympicMedals ysl = new OlympicMedals(0, 0, 1, "以色列", "ISR");
        OlympicMedals mxg = new OlympicMedals(0, 0, 1, "墨西哥", "MEX");
        OlympicMedals mg = new OlympicMedals(0, 0, 2, "蒙古", "MGL");
        OlympicMedals xxl = new OlympicMedals(0, 0, 1, "新西兰", "NZL");
        OlympicMedals xlwny = new OlympicMedals(1, 0, 1, "斯洛文尼亚", "SLO");
        OlympicMedals rs = new OlympicMedals(0, 1, 1, "瑞士", "SUI");
        OlympicMedals jk = new OlympicMedals(0, 1, 1, "捷克", "CZE");
        OlympicMedals kwt = new OlympicMedals(0, 0, 1, "科威特", "KUW");
        OlympicMedals dm = new OlympicMedals(0, 1, 0, "丹麦", "DEN");
        OlympicMedals bmd = new OlympicMedals(1, 0, 0, "百慕大", "BER");
        OlympicMedals zgxg = new OlympicMedals(1, 0, 0, "中国香港", "HKG");
        OlympicMedals flb = new OlympicMedals(1, 0, 0, "菲律宾", "PHI");
        OlympicMedals yued = new OlympicMedals(0, 1, 0, "约旦", "JOR");
        OlympicMedals kldy = new OlympicMedals(1, 0, 1, "克罗地亚", "CRO");
        OlympicMedals aj = new OlympicMedals(0, 0, 2, "埃及", "EGY");
        OlympicMedals ktdw = new OlympicMedals(0, 0, 1, "科特迪瓦", "CIV");
        //全部国家数据
        List<OlympicMedals> olympicMedalsList = new ArrayList<>();

        olympicMedalsList.add(bmd); olympicMedalsList.add(zgxg); olympicMedalsList.add(flb); olympicMedalsList.add(yued); olympicMedalsList.add(kldy);
        olympicMedalsList.add(aj);olympicMedalsList.add(ktdw);

        olympicMedalsList.add(china);
        olympicMedalsList.add(korea);
        olympicMedalsList.add(australia);
        olympicMedalsList.add(usa);
        olympicMedalsList.add(japan);
        olympicMedalsList.add(dg);
        olympicMedalsList.add(hskst);
        olympicMedalsList.add(teq);
        olympicMedalsList.add(wkl);
        olympicMedalsList.add(asly);
        olympicMedalsList.add(ysl);
        olympicMedalsList.add(mxg);
        olympicMedalsList.add(mg);
        olympicMedalsList.add(xxl);
        olympicMedalsList.add(xlwny);
        olympicMedalsList.add(ksw);
        olympicMedalsList.add(nw);
        olympicMedalsList.add(tg);
        olympicMedalsList.add(wzbkst);
        olympicMedalsList.add(hl);
        olympicMedalsList.add(bx);
        olympicMedalsList.add(yn);
        olympicMedalsList.add(rs);
        olympicMedalsList.add(els);
        olympicMedalsList.add(ydl);
        olympicMedalsList.add(yg);
        olympicMedalsList.add(jnd);
        olympicMedalsList.add(fg);
        olympicMedalsList.add(xyl);
        olympicMedalsList.add(tns);
        olympicMedalsList.add(adl);
        olympicMedalsList.add(egde);
        olympicMedalsList.add(yl);
        olympicMedalsList.add(sewy);
        olympicMedalsList.add(zgtb);
        olympicMedalsList.add(bls);
        olympicMedalsList.add(bjly);
        olympicMedalsList.add(glby);
        olympicMedalsList.add(xby);
        olympicMedalsList.add(gljy);
        olympicMedalsList.add(yd);
        olympicMedalsList.add(lmny);
        olympicMedalsList.add(jk);
        olympicMedalsList.add(kwt);
        olympicMedalsList.add(dm);
        long endTime = System.currentTimeMillis();
        System.out.println("数组组装耗时:"+(endTime-startTime));
        return olympicMedalsList;
    }
}
