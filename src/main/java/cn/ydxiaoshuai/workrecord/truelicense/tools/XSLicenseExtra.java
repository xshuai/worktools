package cn.ydxiaoshuai.workrecord.truelicense.tools;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 小帅丶
 * @className XSLicenseExtra
 * @Description 自定义需要校验的License参数，可以增加一些额外需要校验的参数
 * @Date 2021-08-12-14:30
 **/
@Data
public class XSLicenseExtra implements Serializable {

    private static final long serialVersionUID = -1765340618080108626L;

    /**
     * 可被允许的IP地址
     */
    private List<String> ipAddress;

    /**
     * 可被允许的MAC地址
     */
    private List<String> macAddress;

    /**
     * 可被允许的CPU序列号
     */
    private String cpuSerial;

    /**
     * 可被允许的主板序列号
     */
    private String mainBoardSerial;

    @Override
    public String toString() {
        return "XSLicenseExtra{" +
                "ipAddress=" + ipAddress +
                ", macAddress=" + macAddress +
                ", cpuSerial='" + cpuSerial + '\'' +
                ", mainBoardSerial='" + mainBoardSerial + '\'' +
                '}';
    }
}
