package cn.ydxiaoshuai.workrecord.truelicense.tools;

import cn.ydxiaoshuai.workrecord.truelicense.utils.CommonMethod;

import java.util.List;

/**
 * @author 小帅丶
 * @className WindowsServerInfos
 * @Description 用于获取客户Windows服务器的基本信息
 * @Date 2021-08-12-19:56
 **/
public class WindowsServerInfos extends AbstractServerInfos{
    @Override
    protected List<String> getIpAddress() throws Exception {
        return CommonMethod.getIpAddress();
    }

    @Override
    protected List<String> getMacAddress() throws Exception {
        return CommonMethod.getMacByInetAddress();
    }

    @Override
    protected String getCPUSerial() throws Exception {
        //使用WMIC获取CPU序列号
        Process process = Runtime.getRuntime().exec("wmic cpu get processorid");
        process.getOutputStream().close();
        return CommonMethod.getSerialNumber(process.getInputStream());
    }

    public static void main(String[] args) throws Exception{
        WindowsServerInfos method = new WindowsServerInfos();
        System.out.println(method.getIpAddress());
        System.out.println(method.getMacAddress());
        System.out.println(method.getCPUSerial());
        System.out.println(method.getMainBoardSerial());
    }

    @Override
    protected String getMainBoardSerial() throws Exception {
        //使用WMIC获取主板序列号
        Process process = Runtime.getRuntime().exec("wmic baseboard get serialnumber");
        process.getOutputStream().close();
        return CommonMethod.getSerialNumber(process.getInputStream());
    }
}
