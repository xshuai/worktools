package cn.ydxiaoshuai.workrecord.truelicense.tools;


import cn.ydxiaoshuai.workrecord.truelicense.tools.XSLicenseExtra;
import de.schlichtherle.license.CipherParam;
import de.schlichtherle.license.KeyStoreParam;
import de.schlichtherle.license.LicenseParam;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.prefs.Preferences;

/**
 * @author 小帅丶
 * @className XSLicenseCreatorParam
 * @Description License生成类需要的参数
 * @Date 2021-08-12-15:39
 **/
@Data
@NoArgsConstructor
public class XSLicenseCreatorParam implements Serializable {

    private static final long serialVersionUID = -4619223262208906459L;
    /** 默认用户类型 */
    private static String DEFAULT_CONSUMER_TYPE = "user";

    /** 证书subject */
    private String subject;
    /** 私钥别称 */
    private String private_alias;
    /** 私钥库密码 */
    private String key_cipher;
    /** 访问私钥库密码 */
    private String store_cipher;
    /** 证书生成路径 */
    private String license_path;
    /** 私钥库存储路径 */
    private String private_key_store_path;
    /** 证书生效时间 */
    private Date issued_time = new Date();
    /** 证失失效时间 */
    private Date expire_time;
    /** 用户类型 */
    private String consumer_type = DEFAULT_CONSUMER_TYPE;
    /** 用户数量 */
    private Integer consumer_amount = 1;
    /** 描述信息 */
    private String description = "";
    /** 额外的校验信息 */
    private XSLicenseExtra xsLicenseExtra;


}
