package cn.ydxiaoshuai.workrecord.truelicense.tools;

import cn.ydxiaoshuai.workrecord.truelicense.utils.CommonMethod;

import java.util.List;

/**
 * @author 小帅丶
 * @className LinuxServerInfos
 * @Description 用于获取客户Linux服务器的基本信息
 * @Date 2021-08-12-19:49
 **/
public class LinuxServerInfos extends AbstractServerInfos {
    @Override
    protected List<String> getIpAddress() throws Exception {
        return CommonMethod.getMacByInetAddress();
    }

    @Override
    protected List<String> getMacAddress() throws Exception {
        return CommonMethod.getMacByInetAddress();
    }

    @Override
    protected String getCPUSerial() throws Exception {
        //使用dmidecode命令获取CPU序列号
        String[] shell = {"/bin/bash","-c","dmidecode -t processor | grep 'ID' | awk -F ':' '{print $2}' | head -n 1"};
        return CommonMethod.getSerialNumber(shell);
    }

    @Override
    protected String getMainBoardSerial() throws Exception {
        //使用dmidecode命令获取主板序列号
        String[] shell = {"/bin/bash","-c","dmidecode | grep 'Serial Number' | awk -F ':' '{print $2}' | head -n 1"};
        return CommonMethod.getSerialNumber(shell);
    }
}
