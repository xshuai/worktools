package cn.ydxiaoshuai.workrecord.truelicense.tools;

import de.schlichtherle.license.AbstractKeyStoreParam;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author 小帅丶
 * @className XSKeyStoreParam
 * @Description 自定义KeyStoreParam
 * @Date 2021-08-12-14:25
 **/
public class XSKeyStoreParam extends AbstractKeyStoreParam {
    /** 私钥路径 */
    private String store_path;
    /** 别名 */
    private String alias;
    /** 访问私钥库密码 */
    private String store_cipher;
    /** 私钥库密码 */
    private String key_cipher;

    /**
     * Creates a new instance of AbstractKeyStoreParam which will look up
     * the given resource using the classloader of the given class when
     * calling {@link #getStream()}.
     *
     * @param clazz
     * @param resource
     */
    public XSKeyStoreParam(Class clazz, String resource,String alias,String store_cipher,String key_cipher) {
        super(clazz, resource);
        this.store_path = resource;
        this.alias = alias;
        this.store_cipher = store_cipher;
        this.key_cipher = key_cipher;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    @Override
    public String getStorePwd() {
        return store_cipher;
    }

    @Override
    public String getKeyPwd() {
        return key_cipher;
    }

    /**
     * @Author 小帅丶
     * @Description 用于将公私钥存储文件存放到其他磁盘位置而不是工程中
     * @Date  2021-08-12 14:29
     * @return java.io.InputStream
     **/
    @Override
    public InputStream getStream() throws IOException{
        return new FileInputStream(new File(store_path));
    }
}

