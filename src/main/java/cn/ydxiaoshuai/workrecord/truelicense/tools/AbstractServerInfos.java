package cn.ydxiaoshuai.workrecord.truelicense.tools;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author 小帅丶
 * @className AbstractServerInfos
 * @Description 用于获取客户服务器的基本信息，如：IP、Mac地址、CPU序列号、主板序列号等
 * @Date 2021-08-12-19:40
 * @blogurl https://blog.csdn.net/xmtblog/article/details/108177684
 **/
@Slf4j
public abstract class AbstractServerInfos {
    /** 组装需要额外校验的License参数 */
    public XSLicenseExtra getServerInfos(){
        XSLicenseExtra extra = new XSLicenseExtra();
        try {
            extra.setIpAddress(this.getIpAddress());
            extra.setMacAddress(this.getMacAddress());
            extra.setCpuSerial(this.getCPUSerial());
            extra.setMainBoardSerial(this.getMainBoardSerial());
        }catch (Exception e){
            log.error("------------------------------- 获取硬件信息失败 -------------------------------");
            log.error("错误{}", e);
        }
        return extra;
    }
    /**
     * 获取IP地址
     * @return java.util.List<java.lang.String>
     */
    protected abstract List<String> getIpAddress() throws Exception;
    /**
     * 获取Mac地址
     * @return java.util.List<java.lang.String>
     */
    protected abstract List<String> getMacAddress() throws Exception;
    /**
     * 获取CPU序列号
     */
    protected abstract String getCPUSerial() throws Exception;
    /**
     * 获取主板序列号
     */
    protected abstract String getMainBoardSerial() throws Exception;
}
