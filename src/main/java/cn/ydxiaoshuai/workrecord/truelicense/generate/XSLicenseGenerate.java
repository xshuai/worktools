package cn.ydxiaoshuai.workrecord.truelicense.generate;

import cn.ydxiaoshuai.workrecord.truelicense.tools.XSKeyStoreParam;
import cn.ydxiaoshuai.workrecord.truelicense.tools.XSLicenseCreatorParam;
import de.schlichtherle.license.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.text.MessageFormat;
import java.util.prefs.Preferences;

/**
 * @author 小帅丶
 * @className XSLicenseGenerate
 * @Description License的生成
 * @Date 2021-08-12-15:54
 **/
@Data
@Slf4j
public class XSLicenseGenerate {
    /**
     * OU   - 组织单位名称
     * O    - 组织命名成
     * L    - 城市或区域
     * ST   - 省份
     * C    - 国家
     * CN   - 公用名称
     * */
    private final static X500Principal DEFAULT_HOLDER_AND_ISSUER = new X500Principal("CN=XSLicense, " +
            "OU=xiaoshuai," +
            "O=XSGROUP, " +
            "L=Beijing, " +
            "ST=Beijing, " +
            "C=CN");

    private XSLicenseCreatorParam param;

    public XSLicenseGenerate(XSLicenseCreatorParam param){
        this.param = param;
    }

    /**
     * 生成License证书
     * */
    public boolean generateLicense(){
        try {
            LicenseContent licenseContent = initLicenseContent();
            LicenseManager licenseManager = new LicenseManager(initLicenseParam());
            licenseManager.store(licenseContent,new File(param.getLicense_path()));
            return true;
        }catch (Exception e){
            log.error(MessageFormat.format("证书生成失败:{0}",param), e);
            return false;
        }
    }

    /**
     * 初始化证书生成参数
     * */
    private LicenseParam initLicenseParam() {
       Preferences preferences = Preferences.userNodeForPackage(XSLicenseGenerate.class);
       //设置证书内容加密的私钥
       CipherParam cipherParam = new DefaultCipherParam(param.getStore_cipher());

       XSKeyStoreParam privateStoreParam = new XSKeyStoreParam(XSLicenseGenerate.class,
               param.getPrivate_key_store_path(),
               param.getPrivate_alias(),
               param.getStore_cipher(),
               param.getKey_cipher());

       return new DefaultLicenseParam(param.getSubject(),
               preferences,
               privateStoreParam,
               cipherParam);
    }

    /**
     * 设置证书生成正文信息
     * */
    private LicenseContent initLicenseContent() {
        LicenseContent licenseContent = new LicenseContent();
        licenseContent.setHolder(DEFAULT_HOLDER_AND_ISSUER);
        licenseContent.setIssuer(DEFAULT_HOLDER_AND_ISSUER);

        licenseContent.setSubject(param.getSubject());
        licenseContent.setIssued(param.getIssued_time());
        licenseContent.setNotBefore(param.getIssued_time());
        licenseContent.setNotAfter(param.getExpire_time());
        licenseContent.setConsumerType(param.getConsumer_type());
        licenseContent.setConsumerAmount(param.getConsumer_amount());
        licenseContent.setInfo(param.getDescription());

        //扩展校验，这里可以自定义一些额外的校验信息(也可以用json字符串保存)
        if (param.getXsLicenseExtra() != null) {
            licenseContent.setExtra(param.getXsLicenseExtra());
        }

        return licenseContent;
    }



}
