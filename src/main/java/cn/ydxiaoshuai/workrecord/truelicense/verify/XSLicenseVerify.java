package cn.ydxiaoshuai.workrecord.truelicense.verify;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.ydxiaoshuai.workrecord.truelicense.tools.XSLicenseManager;
import cn.ydxiaoshuai.workrecord.truelicense.tools.XSKeyStoreParam;
import com.alibaba.fastjson.JSON;
import de.schlichtherle.license.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.text.MessageFormat;

import java.util.prefs.Preferences;

/**
 * @author 小帅丶
 * @className LicenseVerify
 * @Description 校验类
 * @Date 2021-08-12-18:19
 **/
@Data
@Slf4j
public class XSLicenseVerify {

    /**
     * 证书subject
     */
    private String subject;
    /**
     * 公钥别称
     */
    private String public_alias;
    /**
     * 证书生成路径
     */
    private String license_path;
    /**
     * 访问公钥库的密码
     */
    private String store_cipher;
    /**
     * 公钥库存储路径
     */
    private String public_key_store_path;
    /**
     * LicenseManager
     */
    private LicenseManager licenseManager;
    /**
     * 标识证书是否安装成功
     */
    private boolean installSuccess;


    public XSLicenseVerify(String subject, String public_alias, String store_cipher, String license_path, String public_key_store_path) {
        this.subject = subject;
        this.public_alias = public_alias;
        this.store_cipher = store_cipher;
        this.license_path = license_path;
        this.public_key_store_path = public_key_store_path;
    }

    /**
     * 安装License证书，读取证书相关的信息, 在bean加入容器的时候自动调用
     */
    public void installLicense() {
        try {
            Preferences preferences = Preferences.userNodeForPackage(XSLicenseVerify.class);

            CipherParam cipherParam = new DefaultCipherParam(store_cipher);

            KeyStoreParam publicStoreParam = new XSKeyStoreParam(XSLicenseVerify.class,
                    public_key_store_path,
                    public_alias,
                    store_cipher,
                    null);
            LicenseParam licenseParam = new DefaultLicenseParam(subject, preferences, publicStoreParam, cipherParam);
            licenseManager = new XSLicenseManager(licenseParam);
            licenseManager.uninstall();
            LicenseContent licenseContent = licenseManager.install(new File(license_path));
            installSuccess = true;
            log.info("------------------------------- 证书安装成功 -------------------------------");
            log.info(MessageFormat.format("证书有效期：{0} - {1}",
                    DateUtil.format(licenseContent.getNotBefore(), DatePattern.NORM_DATETIME_PATTERN),
                    DateUtil.format(licenseContent.getNotAfter(), DatePattern.NORM_DATETIME_PATTERN)));
        } catch (Exception e) {
            installSuccess = false;
            log.error("------------------------------- 证书安装失败 -------------------------------");
            log.error("错误", e);
        }
    }

    /**
     * 卸载证书，在bean从容器移除的时候自动调用
     */
    public void unInstallLicense() {
        if (installSuccess) {
            try {
                licenseManager.uninstall();
            } catch (Exception e) {
                log.error("------------------------------- 证书卸载失败 -------------------------------");
                log.error("错误", e);
            }
        }
    }

    /**
     * 校验License证书
     */
    public boolean verify() {
        try {
            LicenseContent licenseContent = licenseManager.verify();
            log.info("LicenseContent--->{}", JSON.toJSONString(licenseContent));
            return true;
        } catch (Exception e) {
            log.error("------------------------------- 校验证书失败 -------------------------------");
            log.error("错误{}", e);
            return false;
        }
    }

}

