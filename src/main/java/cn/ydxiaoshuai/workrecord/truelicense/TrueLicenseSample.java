package cn.ydxiaoshuai.workrecord.truelicense;

import cn.ydxiaoshuai.workrecord.truelicense.generate.XSLicenseGenerate;
import cn.ydxiaoshuai.workrecord.truelicense.verify.XSLicenseVerify;
import cn.ydxiaoshuai.workrecord.truelicense.tools.XSLicenseCreatorParam;

import java.util.Calendar;

/**
 * @author 小帅丶
 * @className TrueLicenseSample
 * @Description 示例代码
 * @Date 2021-08-12-14:25
 **/
public class TrueLicenseSample {
    /**********************前提是先通过 keytool(cmd执行即可。前提安装并配置了JDK) 生成 公私钥对**********************/
    /** 1. 生成私匙库
    # validity：私钥的有效期多少天
    # alias：私钥别称
    # keystore: 指定私钥库文件的名称(生成在当前目录)
    # storepass：指定私钥库的密码(获取keystore信息所需的密码)
    # keypass：指定别名条目的密码(私钥的密码)*/
    //keytool -genkeypair -keysize 1024 -validity 3650 -alias "privateKey" -keystore "xs-privateKeys.keystore" -storepass "a123456" -keypass "a123456" -dname "CN=XSLicens, OU=xiaoshuai, O=XSGROUP, L=Beijing, ST=Beijing, C=CN"
    /**## 2. 把私匙库内的公匙导出到一个文件当中
     # alias：私钥别称
     # keystore：指定私钥库的名称(在当前目录查找)
     # storepass: 指定私钥库的密码
     # file：证书名称 */
    //keytool -exportcert -alias "privateKey" -keystore "xs-privateKeys.keystore" -storepass "a123456" -file "xs-cert.cer"
    /**## 3. 再把这个证书文件导入到公匙库
     # alias：公钥别称
     # file：证书名称
     # keystore：公钥文件名称
     # storepass：指定私钥库的密码 */
    //keytool -import -alias "publicCert" -file "xs-cert.cer" -keystore "incp-publicCerts.keystore" -storepass "a123456"

    public static void main(String[] args) {
        //generateLicense();
        licenseVerify();
    }

    //生成license
    public static void generateLicense(){
        XSLicenseCreatorParam param = new XSLicenseCreatorParam();
        param.setSubject("XS-Server");
        param.setPrivate_alias("privateKey");
        param.setKey_cipher("a123456");
        param.setStore_cipher("a123456");
        param.setLicense_path("F:\\testlicense\\XS-license.lic");
        param.setPrivate_key_store_path("F:\\testlicense\\XS-privateKeys.keystore");

        Calendar issueCalendar = Calendar.getInstance();
        param.setIssued_time(issueCalendar.getTime());

        Calendar expireCalendar = Calendar.getInstance();
        expireCalendar.set(2022,Calendar.DECEMBER,31,23,59,59);
        param.setExpire_time(expireCalendar.getTime());

        param.setConsumer_type("user");
        param.setConsumer_amount(1);
        param.setDescription("测试license");

        XSLicenseGenerate xsLicenseGenerate = new XSLicenseGenerate(param);


        xsLicenseGenerate.generateLicense();
    }

    //license校验
    public static void licenseVerify() {
        // 生成license需要的一些参数
        XSLicenseVerify param = new XSLicenseVerify("XS-Server","publicCert",
                "a123456",
                "F:\\testlicense\\XS-license.lic",
                "F:\\testlicense\\XS-publicCerts.keystore");
        //param.installLicense();
        System.out.println(param.verify());
        param.unInstallLicense();
    }
}
