package cn.ydxiaoshuai.workrecord;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

/**
 * @author 小帅丶
 * @className ReadImageEXIFSample
 * @Description 读取图片EXIF信息
 * @Date 2021-07-08-14:28
 **/
@Slf4j
public class ReadImageEXIFSample {
    /**
     * @Author 小帅丶
     * @Description 读取图片EXIF信息
     * @Date  2021-07-08 15:49
     * @param filePath - 文件路径
     * @return void
     **/
    public static void readImageEXIFInfo(String filePath){
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(new File(filePath));
            for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    String tagTypeHex = tag.getTagTypeHex();
                    System.out.print("标记类型:"+tag.getTagType());
                    System.out.print("\t16机制标记类型:"+tagTypeHex);
                    System.out.print("\t标签名称:"+tag.getTagName());
                    System.out.print("\t标签描述:"+tag.getDescription());
                    System.out.println();
                }
                if(directory.hasErrors()){
                    for (String error : directory.getErrors()) {
                        System.err.println("错误消息:"+error);
                    }
                }
            }

        }catch (Exception e){
            log.info("读取错误",e.getMessage());
        }
    }
}
