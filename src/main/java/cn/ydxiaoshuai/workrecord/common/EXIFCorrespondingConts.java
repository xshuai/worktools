package cn.ydxiaoshuai.workrecord.common;

/**
 * @author 小帅丶
 * @className EXIFCorrespondingConts
 * @Description 常见的Exif参数 HEX
 * @Date 2021-07-08-14:50
 **/
public class EXIFCorrespondingConts {
    /**
     * GPS Version ID-GPS版本
     */
    public static String GPS_VERSION_ID = "0x0000";
    /**
     * Make-厂商
     */
    public static String MAKE = "0x010f";
    /**
     * Model-机型
     */
    public static String MODEL = "0x0110";
    /**
     * Orientation-方向
     */
    public static String ORIENTATION = "0x0112";
    /**
     * Software-软件
     */
    public static String SOFTWARE = "0x0131";
    /**
     * Date/Time-修改时间
     */
    public static String DATE = "0x0132";
    /**
     * Artist-作者
     */
    public static String ARTIST = "0x013b";
    /**
     * YCbCr Positioning-YcbCr定位
     */
    public static String YCBCR_POSITIONING = "0x0213";
    /**
     * Exposure Time-曝光时间
     */
    public static String EXPOSURE_TIME = "0x829a";
    /**
     * F-Number-光圈
     */
    public static String F_NUMBER = "0x829d";
    /**
     * Exposure Program-曝光程序
     */
    public static String EXPOSURE_PROGRAM = "0x8822";
    /**
     * ISO Speed Ratings-ISO感光度
     */
    public static String ISO_SPEED_RATINGS = "0x8827";
    /**
     * Exif Version-Exif版本
     */
    public static String EXIF_VERSION = "0x9000";
    /**
     * Date/Time Original-拍摄时间
     */
    public static String DATE_ORIGINAL = "0x9003";
    /**
     * Date/Time Digitized-数字化时间
     */
    public static String DATE_DIGITIZED = "0x9004";
    /**
     * Components Configuration-成分构成
     */
    public static String COMPONENTS_CONFIGURATION = "0x9101";
    /**
     * Shutter Speed Value-快门速度
     */
    public static String SHUTTER_SPEED_VALUE = "0x9201";
    /**
     * Aperture Value-光圈值
     */
    public static String APERTURE_VALUE = "0x9202";
    /**
     * Exposure Bias Value-曝光补偿
     */
    public static String EXPOSURE_BIAS_VALUE = "0x9204";
    /**
     * Max Aperture Value-最大光圈
     */
    public static String MAX_APERTURE_VALUE = "0x9205";
    /**
     * Metering Mode-测光模式
     */
    public static String METERING_MODE = "0x9207";
    /**
     * Flash-闪光
     */
    public static String FLASH = "0x9209";
    /**
     * Focal Length-焦距
     */
    public static String FOCAL_LENGTH = "0x920a";
    /**
     * User Comment-用户注释
     */
    public static String USER_COMMENT = "0x9286";
    /**
     * Sub-Sec Time-次秒（修改时间）
     */
    public static String SUB_SEC_TIME = "0x9290";
    /**
     * Sub-Sec Time Original-次秒（拍摄时间）
     */
    public static String SUB_SEC_TIME_ORIGINAL = "0x9291";
    /**
     * Sub-Sec Time Digitized-次秒（数字化时间）
     */
    public static String SUB_SEC_TIME_DIGITIZED = "0x9292";
    /**
     * FlashPix Version-FlashPix版本
     */
    public static String FLASHPIX_VERSION = "0xa000";
    /**
     * Color Space-色彩空间
     */
    public static String COLOR_SPACE = "0xa001";
    /**
     * Exif Image Width-Exif图像宽度
     */
    public static String EXIF_IMAGE_WIDTH = "0xa002";
    /**
     * Exif Image Height-Exif图像高度
     */
    public static String EXIF_IMAGE_HEIGHT = "0xa003";
    /**
     * Focal Plane X Resolution-焦平面水平分辨率
     */
    public static String FOCAL_PLANE_X_RESOLUTION = "0xa20e";
    /**
     * Focal Plane Y Resolution-焦平面垂直分辨率
     */
    public static String FOCAL_PLANE_Y_RESOLUTION = "0xa20f";
    /**
     * Focal Plane Resolution Unit-焦平分辨率单位
     */
    public static String FOCAL_PLANE_RESOLUTION_UNIT = "0xa210";
    /**
     * Custom Rendered-自定义补偿
     */
    public static String CUSTOM_RENDERED = "0xa401";
    /**
     * Exposure Mode-曝光模式
     */
    public static String EXPOSURE_MODE = "0xa402";
    /**
     * White Balance Mode-白平衡
     */
    public static String WHITE_BALANCE_MODE = "0xa403";
    /**
     * Scene Capture Type-场景拍摄类型
     */
    public static String SCENE_CAPTURE_TYPE = "0xa406";
    /**
     * Interoperability Index-可交换标准
     */
    public static String INTEROPERABILITY_INDEX = "0x0001";
    /**
     * Interoperability Version-可交换版本
     */
    public static String INTEROPERABILITY_VERSION = "0x0002";
    /**
     * Thumbnail Compression-压缩模式
     */
    public static String THUMBNAIL_COMPRESSION = "0x0103";
    /**
     * Thumbnail Offset-JPEG缩略图起始位置
     */
    public static String THUMBNAIL_OFFSET = "0x0201";
    /**
     * Thumbnail Length-JPEG缩略图数据长度
     */
    public static String THUMBNAIL_LENGTH = "0x0202";
    /**
     * X Resolution-水平分辨率
     */
    public static String X_RESOLUTION = "0x011a";
    /**
     * Y Resolution-垂直分辨率
     */
    public static String Y_RESOLUTION = "0x011b";
    /**
     * Resolution Unit-分辨率单位
     */
    public static String RESOLUTION_UNIT = "0x0128";
    /**
     * Lens Information-镜头信息
     */
    public static String LENS_INFORMATION = "0x0006";
    /**
     * Lens-镜头
     */
    public static String LENS = "0x0007";


}
