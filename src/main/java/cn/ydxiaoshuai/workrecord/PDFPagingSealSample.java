package cn.ydxiaoshuai.workrecord;

import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.graphics.PdfGraphicsUnit;
import com.spire.pdf.graphics.PdfImage;
import com.spire.pdf.graphics.PdfUnitConvertor;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @author 小帅丶
 * @className PDFPagingSealSample
 * @Description 骑缝章
 * @Date 2021-07-08-12:11
 **/
@Slf4j
public class PDFPagingSealSample {
    /**
     * @Author 小帅丶
     * @Description PDF增加骑缝章
     * @Date  2021-07-08 12:53
     * @param source_pdf - PDF源文件
     * @param seal_path - 印章文件
     * @param target_pdf - 目标文件
     * @return void
     **/
    public static void generalPagingSeal(String source_pdf,String seal_path,String target_pdf){
        //加载测试文档
        PdfDocument doc = new PdfDocument();
        doc.loadFromFile(source_pdf);

        PdfUnitConvertor convert = new PdfUnitConvertor();
        PdfPageBase pageBase = null;

        //获取分割后的印章图片
        BufferedImage[] images = getSealImage(doc.getPages().getCount(),
                seal_path);
        float x = 0;
        float y = 0;
        if(null!=images){
            //将图片绘制到PDF页面上的指定位置
            for (int i = 0; i < doc.getPages().getCount(); i++) {
                BufferedImage image = images[i];
                pageBase = doc.getPages().get(i);
                x = (float) (pageBase.getSize().getWidth()) - convert.convertUnits(image.getWidth(),
                        PdfGraphicsUnit.Pixel,
                        PdfGraphicsUnit.Point);
                y = (float) pageBase.getSize().getHeight() / 2;
                pageBase.getCanvas().drawImage(PdfImage.fromImage(image), x, y);
            }
            //保存处理后文档
            doc.saveToFile(target_pdf);
        }else{
            log.info("添加骑缝章失败了");
        }
    }

    /**
     * @Author 小帅丶
     * @Description 定义getSealImage方法，根据PDF页数分割印章图片
     * @Date  2021-07-08 12:45
     * @param num - PDF页数
     * @param sealPath - 印章图片路径
     * @return java.awt.image.BufferedImage[]
     **/
    public static BufferedImage[] getSealImage(int num,String sealPath) {
        try {
            BufferedImage image = ImageIO.read(new File(sealPath));
            int rows = 1;
            int cols = num;
            int chunks = rows * cols;
            int chunkWidth = image.getWidth() / cols;
            int chunkHeight = image.getHeight() / rows;
            int count = 0;
            BufferedImage[] imgs = new BufferedImage[chunks];
            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    imgs[count] = new BufferedImage(chunkWidth,
                            chunkHeight,
                            image.getType());
                    Graphics2D gr = imgs[count++].createGraphics();
                    gr.drawImage(image,
                            0,
                            0,
                            chunkWidth,
                            chunkHeight,
                            chunkWidth * y,
                            chunkHeight * x,
                            chunkWidth * y + chunkWidth,
                            chunkHeight * x + chunkHeight,
                            Color.WHITE,
                            null);
                    gr.dispose();
                }
            }
            return imgs;
        }catch (Exception e){
            log.info("getSealImage() 出错了\t{}",e.getMessage());
            return null;
        }
    }
}
