package cn.ydxiaoshuai.excel;

import cn.ydxiaoshuai.util.ExcelDealUtil;

/**
 * Description 读取带密码的excel
 * ProjectName worktools
 * Created by 小帅丶 on 2022-04-18 19:20.
 * Version 1.0
 */

public class ExcelSample {
    public static void main(String[] args) throws Exception{
        //带密码的excel文件本地路径
        String excelPath = "";
        //excel的密码
        String password = "";
        ExcelDealUtil.getExcelData(excelPath, password, 2);
    }
}
