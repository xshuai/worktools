package cn.ydxiaoshuai.excel;

import com.monitorjbl.xlsx.StreamingReader;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;

/**
 * @author 小帅丶
 * @className SmallSample
 * @Description 缓存读取
 * @Date 2021-09-14-9:45
 **/
public class SmallSample {
    public static void main(String[] args) throws Exception{
        String source_path = "F:\\File\\2021-09\\11.xlsx";
        long startTime = System.currentTimeMillis();
        FileInputStream fileInputStream = new FileInputStream(source_path);

        Workbook wb = StreamingReader.builder()
                //缓存到内存中的行数，默认是10
                .rowCacheSize(100)
                //读取资源时，缓存到内存的字节大小，默认是1024
                .bufferSize(4096)
                .open(fileInputStream);

        Sheet sheetAt = wb.getSheetAt(0);

        long endTime = System.currentTimeMillis();
        System.out.println("读取文件耗时:"+(endTime-startTime));
        //遍历所有的行
        for (Row row : sheetAt) {
            System.out.println("开始遍历第" + row.getRowNum() + "行数据：");
            //遍历所有的列
            for (Cell cell : row) {
                System.out.print(cell.getStringCellValue() + " ");
            }
            System.out.println(" ");
        }
    }
}
