package cn.ydxiaoshuai.sample.miniosample;

import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

/**
 * @author 小帅丶
 * @className MinIOSample
 * @Description MinIOSample一些案例测试代码
 * @Date 2021-07-07-17:30
 **/
@Slf4j
public class MinIOSample {
    /** minimum allowed multipart size is 5MiB */
    private static final int MIN_MULTIPART_SIZE = 5 * 1024 * 1024;
    /**
     * MinIO服务的URL
     */
    private static String MINIO_URL = "http://192.168.8.88:9000";
    /**
     * MinIO服务的Access key
     */
    private static String MINIO_AK = "";
    /**
     * MinIO服务的Secret key
     */
    private static String MINIO_SK = "";
    /**
     * MinIO服务的存储桶
     */
    private static String MINIO_BUCKET = "";

    public static void main(String[] args) {
        //文件名称
        String file_name = System.currentTimeMillis() + ".jpg";
        //文件路径
        String file_path = "F://testvideo//";
        // 创建MinioClient对象
        MinioClient minioClient = MinioClient.builder()
                //HTTP协议无需额外参数
                .endpoint(MINIO_URL)
                //HTTPS 需要额外参数
                //.endpoint(MINIO_URL,443,true)
                .credentials(MINIO_AK, MINIO_SK)
                .build();
        try {
            InputStream inputStream = new FileInputStream(new File(file_path+file_name));
            //删除文件
            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(MINIO_BUCKET)
                    .object(file_name)
                    .build());
            //获取某个BUCKET下所有的文件
            Iterable<Result<Item>> myObjects = minioClient.listObjects(ListObjectsArgs.builder()
                    .bucket(MINIO_BUCKET)
                    .useApiVersion1(true)
                    .build());
            for (Result<Item> result : myObjects) {
                Item item = result.get();
                System.out.println(item.objectName());
            }
            //查询文件是否存在-不存在则抛出异常
            StatObjectResponse statObjectResponse = minioClient.statObject(StatObjectArgs.builder()
                    .bucket(MINIO_BUCKET)
                    .object(file_name)
                    .build());
            System.out.println("文件名称:" + statObjectResponse.object());
            //查询全部Bucket
            List<Bucket> buckets = minioClient.listBuckets();
            buckets.forEach(bucket -> {
                System.out.println(bucket.creationDate() + ", " + bucket.name());
            });
            //上传文件(会根据大小自动分片上传)
            ObjectWriteResponse objectWriteResponse = minioClient.putObject(PutObjectArgs.builder()
                    .bucket(MINIO_BUCKET)
                    .object(file_name)
                    .stream(inputStream,
                            inputStream.available(),
                            MIN_MULTIPART_SIZE)
                    .build());
            System.out.println("文件名称:" + objectWriteResponse.object());
            // 检查存储桶是否已经存在
            boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder()
                    .bucket(MINIO_BUCKET)
                    .build());
            System.out.println("存储桶是否已经存在:" + isExist);
            // 获取带有签名的对象访问地址
            String objectName = "xx/xx.jpg";
            String presignedObjectUrl = minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.GET)
                            .bucket(MINIO_BUCKET)
                            .object(objectName)
                            //过期时间 单位:秒
                            .expiry(60 * 60 * 24)
                            .build()
            );
            System.out.println("带有签名的对象访问地址:" + presignedObjectUrl);
        } catch (Exception e) {
            log.info("错误信息-{}", e.getMessage());
        }
    }
}
