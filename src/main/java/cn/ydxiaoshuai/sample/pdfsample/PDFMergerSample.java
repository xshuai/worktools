package cn.ydxiaoshuai.sample.pdfsample;


import cn.ydxiaoshuai.pdf.PDFMergeUtil;
import cn.ydxiaoshuai.pdf.PDFParams;
import cn.ydxiaoshuai.pdf.PDFType;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2022-05-12 19:04.
 * Version 1.0
 */

public class PDFMergerSample {

    //电子普通发票合并
    public static void main(String[] args) throws Exception {
        String SAVE_PATH = "D:\\zongxswork\\报销\\2023\\";
        FileOutputStream fileOutputStream = new FileOutputStream(SAVE_PATH + "1225001.pdf");
        List<PDFParams> pdfParams = new ArrayList<>();
        PDFParams invoice = new PDFParams(SAVE_PATH + "北京一卡通电子发票-901.pdf", PDFType.GENERAL_INVOICE);
        PDFParams travel = new PDFParams(SAVE_PATH + "餐费-3489.pdf", PDFType.GENERAL_INVOICE);
        pdfParams.add(invoice);
        pdfParams.add(travel);
        ByteArrayOutputStream byteArrayOutputStream = PDFMergeUtil.mergeInvoiceOnePDF(pdfParams, true);
        fileOutputStream.write(byteArrayOutputStream.toByteArray());
    }

    //滴滴行程单、电子发票合并
    public static void main2(String[] args) throws Exception {
        String str = "2021-11-01,2023-11-11";
        System.out.println(str.split(","));
        String SAVE_PATH = "E:\\testfile\\testpdf\\";
        FileOutputStream fileOutputStream = new FileOutputStream(SAVE_PATH + "1221001.pdf");
        List<PDFParams> pdfParams = new ArrayList<>();
        PDFParams invoice = new PDFParams(SAVE_PATH + "滴滴电子发票.pdf", PDFType.INVOICE);
        PDFParams travel = new PDFParams(SAVE_PATH + "滴滴出行行程报销单.pdf", PDFType.TRAVEL_ITINERARY);
        pdfParams.add(invoice);
        pdfParams.add(travel);
        ByteArrayOutputStream byteArrayOutputStream = PDFMergeUtil.mergeDiDiInvoiceAndTravelToOnePDF(pdfParams,
                false, true);
        fileOutputStream.write(byteArrayOutputStream.toByteArray());
    }

}
