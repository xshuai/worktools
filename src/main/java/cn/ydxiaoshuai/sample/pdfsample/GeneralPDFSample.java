package cn.ydxiaoshuai.sample.pdfsample;

import cn.ydxiaoshuai.pdf.PDFTemplateUtil;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2022-12-22 15:37.
 * Version 1.0
 */

public class GeneralPDFSample {

    public static void main(String[] args) throws Exception {
        ByteArrayOutputStream outputStream = PDFTemplateUtil.fillDataToPDF("小帅一点资讯", "140411199304108888",
                "内蒙古赤峰市翁牛特旗乌敦套海镇新北村村梅林地组",
                "新疆安群农业发展有限公司", "123456789098765432", "新疆阿克苏地区阿克苏市南疆农民",
                "新疆安群农业发展有限公司", "新疆安群农业发展有限公司", "HT2004", "2022-12-21",
                "88888888", "1", "99000", "99000", "123456789098765432",
                "123456789098765432", "YDXS-X8", "150000.00", "12",
                "从 2022年12月01日 至2023年12月01日止", "6.25", "6675.00",
                new BigDecimal("4.35"), 12, new BigDecimal("80"));
        FileOutputStream fileOutputStream = new FileOutputStream("F:\\testfile\\小帅一点资讯.pdf");
        fileOutputStream.write(outputStream.toByteArray());
    }
}
