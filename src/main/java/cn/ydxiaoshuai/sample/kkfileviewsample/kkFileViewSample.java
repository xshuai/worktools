package cn.ydxiaoshuai.sample.kkfileviewsample;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author 小帅丶
 * @className kkFileViewSample
 * @Description 生成文件预览地址示例代码
 * @Date 2021-07-07-17:40
 **/
public class kkFileViewSample {
    /**
     * kkFileView服务的URL
     */
    private static String KKFILEVIEW_URL = "http://192.168.8.88:8012/onlinePreview?url=";

    public static void main(String[] args) throws UnsupportedEncodingException {
        //要预览文件的地址
        String file_url = "http://192.168.8.88:9000/bgpoffshore/fd6eeb4e8171e9659f8ca6d52d1ab485.mp4";

        //对预览文件地址进行BASE64编码处理
        String urlBase64 = Base64.encode(file_url.getBytes(CharsetUtil.UTF_8));

        //对BASE64进行URL_ENCODE处理
        String encodeUrlBase64 = URLEncoder.encode(urlBase64, CharsetUtil.UTF_8);

        //拼接成kkFileView服务所需地址(需要urlencode处理哦)
        String viewUrl = KKFILEVIEW_URL+encodeUrlBase64;

        System.out.println("预览地址 = " + viewUrl);
    }
}
