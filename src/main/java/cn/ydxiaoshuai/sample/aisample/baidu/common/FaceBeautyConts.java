package cn.ydxiaoshuai.sample.aisample.baidu.common;

/**
 * @author 小帅丶
 * @className FaceBeautyConts
 * @Description 常量property的参数
 * @Date 2021-06-28-13:47
 **/
public class FaceBeautyConts {
    /**
     * 基础滤镜
     * memory 	回忆
     * gril 	少女
     * rosy 	红润
     * city 	都市
     * shimmer 	微光
     * redLips 	红唇
     * */
    public enum EntiretyStyle{
        memory,gril,rosy,city,shimmer,redLips;
    }

    /**
     * 画质颜色
     * brightness 亮度 	-1.0~1.0 	默认值0.1
     * contrast 对比度 	-1.0~1.0 	默认值0.1
     * saturation 饱和度 	-1.0~1.0 	默认值0.15
     * */
    public enum colorStyle{
        brightness,contrast,saturation;
    }

    /**
     * 美颜滤镜
     * white 	美白
     * smooth 	磨皮
     * darkcircles 	祛黑眼圈
     * */
    public enum skinStyle{
        white,smooth,darkcircles;
    }

    /**
     * 优化美肤
     * lawpattern 	祛法令纹
     * eyebags 	    祛眼袋
     * */
    public enum visskinStyle{
        lawpattern,eyebagss;
    }

    /**
     * 美妆滤镜
     * cheek  	    腮红
     * eyeball 	    美瞳
     * eyebrow  	眉毛
     * eyeliner  	眼线
     * eyeshadow  	眼影
     * lips  	    口红
     * trimming     修容
     * */
    public enum makeupKind{
        cheek,eyeball,eyebrow,eyeliner,eyeshadow,lips,trimming;
    }
    /**
     * 美型滤镜
     * defaultFace 	    默认脸0.0~1.0
     * babyFace 	    娃娃脸
     * exquisiteFace 	精致
     * freshMeetFace 	鲜肉
     * liveVideoFace 	网红
     * maleFace 	    型男
     * naturalFace 	    自然
     * */
    public enum shapeStyle{
        defaultFace,babyFace,exquisiteFace,freshMeetFace,liveVideoFace,maleFace,naturalFace;
    }
    /**
     * 美妆滤镜
     * cheek  	    腮红
     * */
    public static class MAKEUP_CHEEK{
        /**
         * naturePink  	    自然粉
         * */
        public static String naturePink = "naturePink";
        /**
         * japaneseRed  	日系红
         * */
        public static String japaneseRed = "japaneseRed";
        /**
         * highlights  	    亮点
         * */
        public static String highlights = "highlights";
        /**
         * sunburn  	    晒伤
         * */
        public static String sunburn = "sunburn";
        /**
         * flashing  	    闪烁
         * */
        public static String flashing = "flashing";
    }
    /**
     * 美妆滤镜
     * eyeball  	    美瞳
     * */
    public static class MAKEUP_EYEBALL{
        /**
         * firstSightGray  	初见灰
         * */
        public static String firstSightGray = "firstSightGray";
        /**
         * teardrop  	    泪珠闪
         * */
        public static String teardrop = "teardrop";
        /**
         * matteBrown  	    雾绒棕
         * */
        public static String matteBrown = "matteBrown";
    }
    /**
     * 美妆滤镜
     * eyebrow  	    眉毛
     * */
    public static class MAKEUP_EYEBROW{
        /**
         * goddessBrown 女神棕
         * */
        public static String goddessBrown = "goddessBrown";
        /**
         * sandei  	    山黛黑
         * */
        public static String sandei = "sandei";
        /**
         * willowPalm   柳叶棕
         * */
        public static String willowPalm = "willowPalm";
    }
    /**
     * 美妆滤镜
     * eyeliner  	    眼线
     * */
    public static class MAKEUP_EYELINER{
        /**
         * playful  	    小俏皮
         * */
        public static String playful = "playful";
        /**
         * wildcat  	    小野猫
         * */
        public static String wildcat = "wildcat";
    }
    /**
     * 美妆滤镜
     * eyeshadow  	    眼影
     * */
    public static class MAKEUP_EYESHADOW{
        /**
         * customPainted  	定制彩妆
         * */
        public static String customPainted = "customPainted";
        /**
         * peachBlossom  	桃花妆
         * */
        public static String peachBlossom = "peachBlossom";
        /**
         * sweetTangerine  	甜橘妆
         * */
        public static String sweetTangerine = "sweetTangerine";
        /**
         * flicker  	    细闪妆
         * */
        public static String flicker = "flicker";
        /**
         * diamond  	    贴钻妆
         * */
        public static String diamond = "diamond";
    }
    /**
     * 美妆滤镜
     * lips  	    口红
     * */
    public static class MAKEUP_LIPS{
        /**
         * missYou  	    想你色
         * */
        public static String missYou = "missYou";
        /**
         * cherryRed  	    樱桃红
         * */
        public static String cherryRed = "cherryRed";
        /**
         * caramelColor  	焦糖色
         * */
        public static String caramelColor = "caramelColor";
        /**
         * neutralCold  	中性冷酷
         * */
        public static String neutralCold = "neutralCold";
        /**
         * milkTeaColor  	奶茶色
         * */
        public static String milkTeaColor = "milkTeaColor";
        /**
         * loveLips  	    爱心嘴唇
         * */
        public static String loveLips = "loveLips";
    }
    /**
     * 美妆滤镜
     * trimming  	    修容
     * */
    public static class MAKEUP_TRIMMING{
        /**
         * europeAmerica  	欧美
         * */
        public static String europeAmerica = "europeAmerica";
        /**
         * waterLight  	    水光
         * */
        public static String waterLight = "waterLight";
    }
}
