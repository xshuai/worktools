package cn.ydxiaoshuai.sample.aisample.baidu.nlp;

import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.sample.aisample.baidu.common.BaiDuConts;
import cn.ydxiaoshuai.sample.aisample.baidu.model.NewsSummary;
import com.alibaba.fastjson.JSON;

/**
 * @author 小帅丶
 * @className newsSummarySample
 * @Description 语言处理技术-新闻摘要-示例
 * @Date 2021-10-11-13:52
 **/
public class NewsSummarySample {
    public static void main(String[] args) {
        //接口必要的AccessToken
        String accessToken = "";

        NewsSummary bean = new NewsSummary();
        bean.setContent("麻省理工学院的研究团队为无人机在仓库中使用RFID技术进行库存查找等工作，创造了一种聪明的新方式。它允许公司使用更小，更安全的无人机在巨型建筑物中找到之前无法找到的东西。使用RFID标签更换仓库中的条形码，将帮助提升自动化并提高库存管理的准确性。与条形码不同，RFID标签不需要对准扫描，标签上包含的信息可以更广泛和更容易地更改。它们也可以很便宜，尽管有优点，但是它具有局限性，对于跟踪商品没有设定RFID标准，“标签冲突”可能会阻止读卡器同时从多个标签上拾取信号。扫描RFID标签的方式也会在大型仓库内引起尴尬的问题。固定的RFID阅读器和阅读器天线只能扫描通过设定阈值的标签，手持式读取器需要人员出去手动扫描物品。几家公司已经解决了无人机读取RFID的技术问题。配有RFID读卡器的无人机可以代替库存盘点的人物，并以更少的麻烦更快地完成工作。一个人需要梯子或电梯进入的高箱，可以通过无人机很容易地达到，无人机可以被编程为独立地导航空间，并且他们比执行大规模的重复任务的准确性和效率要比人类更好。目前市场上的RFID无人机需要庞大的读卡器才能连接到无人机的本身。这意味着它们必须足够大，以支持附加硬件的尺寸和重量，使其存在坠机风险。麻省理工学院的新解决方案，名为Rfly，允许无人机阅读RFID标签，而不用捆绑巨型读卡器。相反，无人机配备了一个微小的继电器，它像Wi-Fi中继器一样。无人机接收从远程RFID读取器发送的信号，然后转发它读取附近的标签。由于继电器很小，这意味着可以使用更小巧的无人机，可以使用塑料零件，可以适应较窄的空间，不会造成人身伤害的危险。麻省理工学院的Rfly系统本质上是对现有技术的一个聪明的补充，它不仅消除了额外的RFID读取器，而且由于它是一个更轻的解决方案，允许小型无人机与大型无人机做同样的工作。研究团队正在马萨诸塞州的零售商测试该系统。");
        bean.setMax_summary_len(200);
        //body参数转JSON
        String body = JSON.toJSONString(bean);

        /** 1、GBK支持：默认按GBK进行编码，输入内容为GBK编码，输出内容为GBK编码，否则会接口报错编码错误
            2、UTF-8支持：若文本需要使用UTF-8编码，请在url参数中添加charset=UTF-8 （大小写敏感）
         */
        //拼接URL参数
        String params = "charset=UTF-8&access_token="+accessToken;

        String result = HttpUtil.post(BaiDuConts.NEWS_SUMMARY_URL + "?" + params, body);

        System.out.println("result = " + result);
    }
}
