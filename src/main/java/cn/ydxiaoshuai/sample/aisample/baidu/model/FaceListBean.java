package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;

import java.util.List;

/**
 * @author 小帅丶
 * @className FaceListBean
 * @Description 人脸图片
 * @Date 2021-07-26-11:00
 **/
@Data
public class FaceListBean {
    private List<String> image_base64;
    private String image_wrinkle;
    private String image_acnespotmole;
    private String image_eyesattr;
}
