package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;

/**
 * @author 小帅丶
 * @className SkinAnalyze
 * @Description 皮肤分析
 * @Date 2021-07-26-10:19
 **/
@Data
public class SkinAnalyze {
    private String appid;
    private String image;
    private String image_type;
    private String face_field;
    private Integer max_face_num;
}
