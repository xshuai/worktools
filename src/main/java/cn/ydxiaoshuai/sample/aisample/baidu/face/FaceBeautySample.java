package cn.ydxiaoshuai.sample.aisample.baidu.face;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.sample.aisample.baidu.common.BaiDuConts;
import cn.ydxiaoshuai.sample.aisample.baidu.model.FaceBeauty;
import cn.ydxiaoshuai.sample.aisample.baidu.common.FaceBeautyConts;
import cn.ydxiaoshuai.sample.aisample.baidu.model.FaceBeautyResponseBean;
import cn.ydxiaoshuai.util.DealDataUtil;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 小帅丶
 * @className FaceBeautySample
 * @Description 高清人像美化-示例代码
 * @Date 2021-07-08-10:26
 **/
public class FaceBeautySample {
    /** 接口必要的AccessToken */
    private static String access_token = "";
    /** 图片本地路径 */
    private static String imagePath = "F://testimg//0518cut.jpg";

    public static void main(String[] args){

        FaceBeauty faceBeauty = new FaceBeauty();
        faceBeauty.setImage(Base64.encode(FileUtil.readBytes(imagePath)));

        FaceBeauty.PropertyBean property = new FaceBeauty.PropertyBean();

        List<FaceBeauty.PropertyBean.EntiretyBean> entiretys = new ArrayList<>();
        FaceBeauty.PropertyBean.EntiretyBean entirety = new FaceBeauty.PropertyBean.EntiretyBean();
        //基础滤镜-少女
        entirety.setStyle(FaceBeautyConts.EntiretyStyle.gril);
        entirety.setValue(1.0);
        entiretys.add(entirety);

        List<FaceBeauty.PropertyBean.MakeupBean> makeups = new ArrayList<>();
        FaceBeauty.PropertyBean.MakeupBean makeup = new FaceBeauty.PropertyBean.MakeupBean();
        //美妆滤镜
        makeup.setKind(FaceBeautyConts.makeupKind.lips);
        //爱心嘴唇
        makeup.setStyle(FaceBeautyConts.MAKEUP_LIPS.loveLips);
        makeup.setValue(1.0);
        makeups.add(makeup);

        property.setEntirety(entiretys);
        property.setMakeup(makeups);

        faceBeauty.setProperty(property);

        String params = JSON.toJSONString(faceBeauty);

        String result = HttpUtil.post(BaiDuConts.FACE_BEAUTY_URL + "?access_token=" + access_token, params);

        FaceBeautyResponseBean faceBeautyResponseBean = JSON.parseObject(result, FaceBeautyResponseBean.class);

        if (null==faceBeautyResponseBean.getError_code()) {
            DealDataUtil.drawImageToFrame(faceBeautyResponseBean.getImage());
        } else {
            System.out.println("接口出错了 " + faceBeautyResponseBean.getError_code() + "-" +faceBeautyResponseBean.getError_msg());
        }
    }
}
