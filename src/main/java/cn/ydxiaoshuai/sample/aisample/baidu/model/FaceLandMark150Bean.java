package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className FaceLandMark150Bean
 * @Description 150关键点
 * @Date 2021-01-15-17:22
 **/
@NoArgsConstructor
@Data
public class FaceLandMark150Bean {

    private ResultBean result;
    private long log_id;
    private String error_msg;
    private int cached;
    private int error_code;
    private int timestamp;
    /** 图片的BASE64 */
    private String deal_image;


    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private int face_num;
        private List<FaceListBean> face_list;

        @NoArgsConstructor
        @Data
        public static class FaceListBean {

            private Landmark150Bean landmark150;
            private AngleBean angle;
            private String face_token;
            private LocationBean location;
            private int face_probability;

            @NoArgsConstructor
            @Data
            public static class Landmark150Bean {

                private EyeRightEyeballRightBean eye_right_eyeball_right;
                private MouthCornerRightInnerBean mouth_corner_right_inner;
                private CheekRight1Bean cheek_right_1;
                private CheekRight2Bean cheek_right_2;
                private CheekRight3Bean cheek_right_3;
                private CheekRight8Bean cheek_right_8;
                private CheekRight9Bean cheek_right_9;
                private CheekRight4Bean cheek_right_4;
                private CheekRight5Bean cheek_right_5;
                private CheekRight6Bean cheek_right_6;
                private CheekRight7Bean cheek_right_7;
                private MouthLipLowerOuter1Bean mouth_lip_lower_outer_1;
                private MouthLipLowerOuter2Bean mouth_lip_lower_outer_2;
                private MouthLipLowerOuter3Bean mouth_lip_lower_outer_3;
                private MouthLipLowerOuter4Bean mouth_lip_lower_outer_4;
                private EyeRightCornerLeftBean eye_right_corner_left;
                private MouthLipLowerOuter5Bean mouth_lip_lower_outer_5;
                private MouthCornerLeftOuterBean mouth_corner_left_outer;
                private EyebrowLeftCornerLeftBean eyebrow_left_corner_left;
                private NoseLeftContour7Bean nose_left_contour_7;
                private CheekRight11Bean cheek_right_11;
                private NoseLeftContour6Bean nose_left_contour_6;
                private EyeRightEyeballCenterBean eye_right_eyeball_center;
                private NoseLeftContour5Bean nose_left_contour_5;
                private NoseLeftContour4Bean nose_left_contour_4;
                private CheekLeft8Bean cheek_left_8;
                private CheekLeft7Bean cheek_left_7;
                private NoseLeftContour3Bean nose_left_contour_3;
                private CheekRight10Bean cheek_right_10;
                private NoseLeftContour2Bean nose_left_contour_2;
                private CheekLeft9Bean cheek_left_9;
                private NoseLeftContour1Bean nose_left_contour_1;
                private MouthLipLowerOuter6Bean mouth_lip_lower_outer_6;
                private CheekLeft4Bean cheek_left_4;
                private CheekLeft3Bean cheek_left_3;
                private MouthLipLowerOuter7Bean mouth_lip_lower_outer_7;
                private CheekLeft6Bean cheek_left_6;
                private MouthLipLowerOuter8Bean mouth_lip_lower_outer_8;
                private CheekLeft5Bean cheek_left_5;
                private MouthLipLowerOuter9Bean mouth_lip_lower_outer_9;
                private CheekLeft2Bean cheek_left_2;
                private CheekLeft1Bean cheek_left_1;
                private MouthLipUpperInner2Bean mouth_lip_upper_inner_2;
                private MouthLipUpperInner3Bean mouth_lip_upper_inner_3;
                private MouthLipUpperInner4Bean mouth_lip_upper_inner_4;
                private MouthLipUpperInner5Bean mouth_lip_upper_inner_5;
                private MouthCornerRightOuterBean mouth_corner_right_outer;
                private MouthLipUpperInner6Bean mouth_lip_upper_inner_6;
                private MouthLipUpperInner7Bean mouth_lip_upper_inner_7;
                private MouthLipUpperInner8Bean mouth_lip_upper_inner_8;
                private MouthLipUpperInner9Bean mouth_lip_upper_inner_9;
                private EyebrowLeftUpper1Bean eyebrow_left_upper_1;
                private EyebrowLeftUpper2Bean eyebrow_left_upper_2;
                private EyebrowLeftUpper5Bean eyebrow_left_upper_5;
                private EyebrowLeftUpper3Bean eyebrow_left_upper_3;
                private EyeLeftEyeballRightBean eye_left_eyeball_right;
                private EyebrowLeftUpper4Bean eyebrow_left_upper_4;
                private MouthLipUpperInner1Bean mouth_lip_upper_inner_1;
                private MouthLipUpperOuter11Bean mouth_lip_upper_outer_11;
                private MouthLipLowerInner7Bean mouth_lip_lower_inner_7;
                private MouthLipUpperOuter10Bean mouth_lip_upper_outer_10;
                private MouthLipLowerInner8Bean mouth_lip_lower_inner_8;
                private MouthLipLowerInner9Bean mouth_lip_lower_inner_9;
                private MouthLipLowerInner3Bean mouth_lip_lower_inner_3;
                private MouthLipLowerInner4Bean mouth_lip_lower_inner_4;
                private MouthLipLowerInner5Bean mouth_lip_lower_inner_5;
                private MouthLipLowerInner6Bean mouth_lip_lower_inner_6;
                private EyeLeftEyeballCenterBean eye_left_eyeball_center;
                private MouthLipLowerInner1Bean mouth_lip_lower_inner_1;
                private MouthLipLowerInner2Bean mouth_lip_lower_inner_2;
                private EyeRightCornerRightBean eye_right_corner_right;
                private MouthLipLowerOuter11Bean mouth_lip_lower_outer_11;
                private MouthLipLowerOuter10Bean mouth_lip_lower_outer_10;
                private EyeRightEyeballLeftBean eye_right_eyeball_left;
                private EyebrowRightLower2Bean eyebrow_right_lower_2;
                private EyebrowRightLower1Bean eyebrow_right_lower_1;
                private EyebrowRightLower3Bean eyebrow_right_lower_3;
                private MouthLipUpperOuter9Bean mouth_lip_upper_outer_9;
                private EyeLeftCornerRightBean eye_left_corner_right;
                private MouthLipUpperOuter5Bean mouth_lip_upper_outer_5;
                private MouthLipUpperOuter6Bean mouth_lip_upper_outer_6;
                private MouthLipUpperOuter7Bean mouth_lip_upper_outer_7;
                private MouthLipUpperOuter8Bean mouth_lip_upper_outer_8;
                private EyeLeftEyelidLower4Bean eye_left_eyelid_lower_4;
                private NoseRightContour6Bean nose_right_contour_6;
                private EyeLeftEyelidUpper5Bean eye_left_eyelid_upper_5;
                private EyeLeftEyelidUpper4Bean eye_left_eyelid_upper_4;
                private EyeLeftEyelidLower3Bean eye_left_eyelid_lower_3;
                private NoseRightContour7Bean nose_right_contour_7;
                private EyeLeftEyelidLower2Bean eye_left_eyelid_lower_2;
                private NoseRightContour4Bean nose_right_contour_4;
                private EyeLeftEyelidUpper7Bean eye_left_eyelid_upper_7;
                private EyeLeftEyelidUpper6Bean eye_left_eyelid_upper_6;
                private EyeLeftEyelidLower1Bean eye_left_eyelid_lower_1;
                private NoseRightContour5Bean nose_right_contour_5;
                private EyeLeftEyelidUpper1Bean eye_left_eyelid_upper_1;
                private MouthLipUpperOuter1Bean mouth_lip_upper_outer_1;
                private EyeLeftEyelidLower7Bean eye_left_eyelid_lower_7;
                private MouthLipUpperOuter2Bean mouth_lip_upper_outer_2;
                private EyebrowRightCornerLeftBean eyebrow_right_corner_left;
                private EyeLeftEyelidLower6Bean eye_left_eyelid_lower_6;
                private MouthLipUpperOuter3Bean mouth_lip_upper_outer_3;
                private EyeLeftEyelidUpper3Bean eye_left_eyelid_upper_3;
                private EyeLeftEyeballLeftBean eye_left_eyeball_left;
                private EyeLeftEyelidUpper2Bean eye_left_eyelid_upper_2;
                private EyeLeftEyelidLower5Bean eye_left_eyelid_lower_5;
                private MouthLipUpperOuter4Bean mouth_lip_upper_outer_4;
                private NoseTipBean nose_tip;
                private Chin3Bean chin_3;
                private Chin1Bean chin_1;
                private Chin2Bean chin_2;
                private EyebrowRightUpper1Bean eyebrow_right_upper_1;
                private EyeRightEyelidLower7Bean eye_right_eyelid_lower_7;
                private EyeRightEyelidLower6Bean eye_right_eyelid_lower_6;
                private EyebrowRightUpper5Bean eyebrow_right_upper_5;
                private EyebrowRightUpper4Bean eyebrow_right_upper_4;
                private EyebrowRightUpper3Bean eyebrow_right_upper_3;
                private EyebrowRightUpper2Bean eyebrow_right_upper_2;
                private MouthCornerLeftInnerBean mouth_corner_left_inner;
                private EyeRightEyelidUpper2Bean eye_right_eyelid_upper_2;
                private EyeRightEyelidUpper1Bean eye_right_eyelid_upper_1;
                private EyeRightEyelidUpper4Bean eye_right_eyelid_upper_4;
                private EyeRightEyelidUpper3Bean eye_right_eyelid_upper_3;
                private EyeRightEyelidLower1Bean eye_right_eyelid_lower_1;
                private EyeLeftCornerLeftBean eye_left_corner_left;
                private EyebrowLeftCornerRightBean eyebrow_left_corner_right;
                private EyeRightEyelidLower5Bean eye_right_eyelid_lower_5;
                private MouthLipUpperInner11Bean mouth_lip_upper_inner_11;
                private EyeRightEyelidLower4Bean eye_right_eyelid_lower_4;
                private EyeRightEyelidLower3Bean eye_right_eyelid_lower_3;
                private EyeRightEyelidLower2Bean eye_right_eyelid_lower_2;
                private MouthLipUpperInner10Bean mouth_lip_upper_inner_10;
                private MouthLipLowerInner10Bean mouth_lip_lower_inner_10;
                private MouthLipLowerInner11Bean mouth_lip_lower_inner_11;
                private EyebrowLeftLower3Bean eyebrow_left_lower_3;
                private CheekLeft10Bean cheek_left_10;
                private CheekLeft11Bean cheek_left_11;
                private EyebrowLeftLower2Bean eyebrow_left_lower_2;
                private EyebrowLeftLower1Bean eyebrow_left_lower_1;
                private NoseBridge1Bean nose_bridge_1;
                private NoseRightContour2Bean nose_right_contour_2;
                private NoseRightContour3Bean nose_right_contour_3;
                private NoseRightContour1Bean nose_right_contour_1;
                private NoseBridge2Bean nose_bridge_2;
                private NoseBridge3Bean nose_bridge_3;
                private EyebrowRightCornerRightBean eyebrow_right_corner_right;
                private NoseMiddleContourBean nose_middle_contour;
                private EyeRightEyelidUpper6Bean eye_right_eyelid_upper_6;
                private EyeRightEyelidUpper5Bean eye_right_eyelid_upper_5;
                private EyeRightEyelidUpper7Bean eye_right_eyelid_upper_7;

                @NoArgsConstructor
                @Data
                public static class EyeRightEyeballRightBean {
                    /**
                     * x : 138
                     * y : 239.55
                     */

                    private int x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthCornerRightInnerBean {
                    /**
                     * x : 164.09
                     * y : 336.51
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight1Bean {
                    /**
                     * x : 99.64
                     * y : 233.79
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight2Bean {
                    /**
                     * x : 100.68
                     * y : 247.63
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight3Bean {
                    /**
                     * x : 102.32
                     * y : 261.23
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight8Bean {
                    /**
                     * x : 127.24
                     * y : 330.89
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight9Bean {
                    /**
                     * x : 136.44
                     * y : 344.01
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight4Bean {
                    /**
                     * x : 105.09
                     * y : 275.38
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight5Bean {
                    /**
                     * x : 108.97
                     * y : 288.87
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight6Bean {
                    /**
                     * x : 113.8
                     * y : 303.02
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight7Bean {
                    /**
                     * x : 119.28
                     * y : 316.44
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter1Bean {
                    /**
                     * x : 165.24
                     * y : 341.2
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter2Bean {
                    /**
                     * x : 168.16
                     * y : 346.19
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter3Bean {
                    /**
                     * x : 172.25
                     * y : 349.92
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter4Bean {
                    /**
                     * x : 176.47
                     * y : 353.85
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightCornerLeftBean {
                    /**
                     * x : 162.62
                     * y : 246.55
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter5Bean {
                    /**
                     * x : 182.08
                     * y : 355.92
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthCornerLeftOuterBean {
                    /**
                     * x : 215.9
                     * y : 334.73
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftCornerLeftBean {
                    /**
                     * x : 267.54
                     * y : 214.74
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour7Bean {
                    /**
                     * x : 200.24
                     * y : 304.08
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight11Bean {
                    /**
                     * x : 161.12
                     * y : 370.53
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour6Bean {
                    /**
                     * x : 199.74
                     * y : 307.98
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyeballCenterBean {
                    /**
                     * x : 147.72
                     * y : 239.37
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour5Bean {
                    /**
                     * x : 204.63
                     * y : 310.47
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour4Bean {
                    /**
                     * x : 211.05
                     * y : 304.15
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft8Bean {
                    /**
                     * x : 254.17
                     * y : 330.69
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft7Bean {
                    /**
                     * x : 262.56
                     * y : 315.45
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour3Bean {
                    /**
                     * x : 205.38
                     * y : 285.55
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekRight10Bean {
                    /**
                     * x : 148.49
                     * y : 357.53
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour2Bean {
                    /**
                     * x : 201.87
                     * y : 266.01
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft9Bean {
                    /**
                     * x : 244.54
                     * y : 344.64
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseLeftContour1Bean {
                    /**
                     * x : 198.29
                     * y : 246.47
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter6Bean {
                    /**
                     * x : 187.95
                     * y : 355.61
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft4Bean {
                    /**
                     * x : 276.5
                     * y : 271.53
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft3Bean {
                    /**
                     * x : 279.26
                     * y : 256.54
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter7Bean {
                    /**
                     * x : 194.08
                     * y : 355.89
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft6Bean {
                    /**
                     * x : 268.15
                     * y : 301.03
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter8Bean {
                    /**
                     * x : 200.02
                     * y : 353.58
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft5Bean {
                    /**
                     * x : 273.24
                     * y : 285.84
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter9Bean {
                    /**
                     * x : 204.72
                     * y : 349.49
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft2Bean {
                    /**
                     * x : 280.73
                     * y : 242.11
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft1Bean {
                    /**
                     * x : 281.65
                     * y : 227.2
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner2Bean {
                    /**
                     * x : 170.97
                     * y : 336.84
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner3Bean {
                    /**
                     * x : 175.18
                     * y : 337.32
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner4Bean {
                    /**
                     * x : 179.54
                     * y : 337.63
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner5Bean {
                    /**
                     * x : 183.61
                     * y : 338.18
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthCornerRightOuterBean {
                    /**
                     * x : 162.6
                     * y : 336.15
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner6Bean {
                    /**
                     * x : 187.65
                     * y : 338.91
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner7Bean {
                    /**
                     * x : 192.23
                     * y : 338.12
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner8Bean {
                    /**
                     * x : 196.59
                     * y : 337.41
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner9Bean {
                    /**
                     * x : 201.22
                     * y : 336.84
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftUpper1Bean {
                    /**
                     * x : 205.43
                     * y : 215.18
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftUpper2Bean {
                    /**
                     * x : 220.02
                     * y : 206.81
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftUpper5Bean {
                    /**
                     * x : 267.54
                     * y : 212.99
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftUpper3Bean {
                    /**
                     * x : 235.49
                     * y : 202.3
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyeballRightBean {
                    /**
                     * x : 225.79
                     * y : 237.57
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftUpper4Bean {
                    /**
                     * x : 252.73
                     * y : 202.75
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner1Bean {
                    /**
                     * x : 167
                     * y : 336.52
                     */

                    private int x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter11Bean {
                    /**
                     * x : 211.5
                     * y : 333.19
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner7Bean {
                    /**
                     * x : 192.38
                     * y : 341.27
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter10Bean {
                    /**
                     * x : 206.66
                     * y : 332.02
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner8Bean {
                    /**
                     * x : 196.66
                     * y : 340.6
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner9Bean {
                    /**
                     * x : 201.27
                     * y : 340.05
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner3Bean {
                    /**
                     * x : 175.35
                     * y : 340.57
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner4Bean {
                    /**
                     * x : 179.52
                     * y : 340.98
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner5Bean {
                    /**
                     * x : 183.55
                     * y : 341.48
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner6Bean {
                    /**
                     * x : 187.81
                     * y : 342.19
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyeballCenterBean {
                    /**
                     * x : 235.31
                     * y : 236.06
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner1Bean {
                    /**
                     * x : 166.98
                     * y : 338.09
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner2Bean {
                    /**
                     * x : 171.01
                     * y : 339.19
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightCornerRightBean {
                    /**
                     * x : 122.26
                     * y : 241.18
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter11Bean {
                    /**
                     * x : 212.83
                     * y : 340.22
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerOuter10Bean {
                    /**
                     * x : 209.29
                     * y : 345.52
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyeballLeftBean {
                    /**
                     * x : 156.39
                     * y : 240.51
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightLower2Bean {
                    /**
                     * x : 133.83
                     * y : 215.1
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightLower1Bean {
                    /**
                     * x : 119.81
                     * y : 215.79
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightLower3Bean {
                    /**
                     * x : 147.83
                     * y : 217.2
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter9Bean {
                    /**
                     * x : 201.43
                     * y : 331.42
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftCornerRightBean {
                    /**
                     * x : 212.46
                     * y : 244.64
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter5Bean {
                    /**
                     * x : 183.17
                     * y : 331.24
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter6Bean {
                    /**
                     * x : 187.42
                     * y : 331.99
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter7Bean {
                    /**
                     * x : 192.19
                     * y : 330.77
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter8Bean {
                    /**
                     * x : 196.74
                     * y : 330.36
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower4Bean {
                    /**
                     * x : 235.52
                     * y : 246.06
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour6Bean {
                    /**
                     * x : 174.5
                     * y : 308.55
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper5Bean {
                    /**
                     * x : 239.12
                     * y : 228.2
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper4Bean {
                    /**
                     * x : 232.9
                     * y : 228.35
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower3Bean {
                    /**
                     * x : 229.46
                     * y : 246.07
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour7Bean {
                    /**
                     * x : 173.61
                     * y : 304.69
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower2Bean {
                    /**
                     * x : 223.32
                     * y : 245.8
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour4Bean {
                    /**
                     * x : 164.68
                     * y : 305.09
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper7Bean {
                    /**
                     * x : 250.13
                     * y : 232.58
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper6Bean {
                    /**
                     * x : 244.73
                     * y : 229.55
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower1Bean {
                    /**
                     * x : 218.15
                     * y : 244.6
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour5Bean {
                    /**
                     * x : 170.47
                     * y : 311.28
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper1Bean {
                    /**
                     * x : 216.68
                     * y : 238.35
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter1Bean {
                    /**
                     * x : 166.18
                     * y : 334.29
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower7Bean {
                    /**
                     * x : 250.5
                     * y : 239.28
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter2Bean {
                    /**
                     * x : 170.17
                     * y : 332.85
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightCornerLeftBean {
                    /**
                     * x : 162.31
                     * y : 219.18
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower6Bean {
                    /**
                     * x : 246.76
                     * y : 242.25
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter3Bean {
                    /**
                     * x : 174.61
                     * y : 331.96
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper3Bean {
                    /**
                     * x : 226.82
                     * y : 230.05
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyeballLeftBean {
                    /**
                     * x : 244.53
                     * y : 235.8
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidUpper2Bean {
                    /**
                     * x : 221.48
                     * y : 233.32
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftEyelidLower5Bean {
                    /**
                     * x : 241.38
                     * y : 244.48
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperOuter4Bean {
                    /**
                     * x : 178.9
                     * y : 330.9
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseTipBean {
                    /**
                     * x : 186.21
                     * y : 300.31
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class Chin3Bean {
                    /**
                     * x : 204.13
                     * y : 379.33
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class Chin1Bean {
                    /**
                     * x : 173.77
                     * y : 378.94
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class Chin2Bean {
                    /**
                     * x : 188.39
                     * y : 382.25
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightUpper1Bean {
                    /**
                     * x : 107.18
                     * y : 217.93
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower7Bean {
                    /**
                     * x : 157.57
                     * y : 246.99
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower6Bean {
                    /**
                     * x : 152.76
                     * y : 248.48
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightUpper5Bean {
                    /**
                     * x : 162.5
                     * y : 215.64
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightUpper4Bean {
                    /**
                     * x : 148.95
                     * y : 208.55
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightUpper3Bean {
                    /**
                     * x : 134.3
                     * y : 205.34
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightUpper2Bean {
                    /**
                     * x : 118.8
                     * y : 207.25
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthCornerLeftInnerBean {
                    /**
                     * x : 214.29
                     * y : 335.16
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper2Bean {
                    /**
                     * x : 131.62
                     * y : 233.55
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper1Bean {
                    /**
                     * x : 126.73
                     * y : 237.04
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper4Bean {
                    /**
                     * x : 142.77
                     * y : 231.17
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper3Bean {
                    /**
                     * x : 137
                     * y : 231.79
                     */

                    private int x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower1Bean {
                    /**
                     * x : 126.7
                     * y : 243.65
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeLeftCornerLeftBean {
                    /**
                     * x : 254.65
                     * y : 236.48
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftCornerRightBean {
                    /**
                     * x : 206.12
                     * y : 218.93
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower5Bean {
                    /**
                     * x : 146.93
                     * y : 249.12
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner11Bean {
                    /**
                     * x : 210.8
                     * y : 335.36
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower4Bean {
                    /**
                     * x : 141.01
                     * y : 249.49
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower3Bean {
                    /**
                     * x : 135.61
                     * y : 248.2
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidLower2Bean {
                    /**
                     * x : 130.2
                     * y : 246.49
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipUpperInner10Bean {
                    /**
                     * x : 206.2
                     * y : 336.03
                     */

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner10Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class MouthLipLowerInner11Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftLower3Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft10Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class CheekLeft11Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftLower2Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowLeftLower1Bean {
                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseBridge1Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour2Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour3Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseRightContour1Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseBridge2Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseBridge3Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyebrowRightCornerRightBean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class NoseMiddleContourBean {
                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper6Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper5Bean {

                    private double x;
                    private double y;
                }

                @NoArgsConstructor
                @Data
                public static class EyeRightEyelidUpper7Bean {

                    private double x;
                    private double y;
                }
            }

            @NoArgsConstructor
            @Data
            public static class AngleBean {

                private double roll;
                private double pitch;
                private double yaw;
            }

            @NoArgsConstructor
            @Data
            public static class LocationBean {
                private double top;
                private double left;
                private int rotation;
                private int width;
                private int height;
            }
        }
    }
}
