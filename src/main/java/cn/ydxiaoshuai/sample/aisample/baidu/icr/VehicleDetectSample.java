package cn.ydxiaoshuai.sample.aisample.baidu.icr;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.sample.aisample.baidu.common.BaiDuConts;
import cn.ydxiaoshuai.sample.aisample.baidu.model.VehicleDetect;
import cn.ydxiaoshuai.sample.aisample.baidu.model.VehicleDetectResponseBean;
import cn.ydxiaoshuai.util.DealDataUtil;
import cn.ydxiaoshuai.util.DrawRectUtil;
import com.alibaba.fastjson.JSON;

/**
 * @author 小帅丶
 * @className VehicleDetectSample
 * @Description 车辆检测
 * @Date 2021-07-23-9:34
 **/
public class VehicleDetectSample {
    /** 接口必要的AccessToken */
    private static String access_token = "";
    /** 图片本地路径 */
    private static String imagePath = "F://testimg//vd03.jpg";
    /** 图片URL路径 */
    private static String imageUrl = "https://aip.bdstatic.com/portal-pc-node/dist/1626958131168/images/technology/vehicle/detect/1.jpg";

    public static void main(String[] args) throws Exception {
        VehicleDetect bean = new VehicleDetect();
        //图片的BASE64
        bean.setImage(Base64.encode(FileUtil.readBytes(imagePath)));
        //图片的URL
        //bean.setUrl(imageUrl);
        //统计的矩形区域
        //bean.setArea("0,0,0,100");

        String result = HttpUtil.post(BaiDuConts.VEHICLE_DETECT_URL + "?access_token=" + access_token,
                bean.toParamString(bean));

        System.out.println("result = " + result);

        VehicleDetectResponseBean vehicleDetectResponseBean = JSON.parseObject(result, VehicleDetectResponseBean.class);
        if (null==vehicleDetectResponseBean.getError_code()) {
            //对图片进行处理
            String dealBase64 = DrawRectUtil.getReactByVehicleDetect(FileUtil.readBytes(imagePath), vehicleDetectResponseBean);
            //打开图片
            DealDataUtil.drawImageToFrame(dealBase64);
        } else {
            System.out.println("接口出错了 " + vehicleDetectResponseBean.getError_code() + "-" +vehicleDetectResponseBean.getError_msg());
        }
    }
}
