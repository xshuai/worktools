package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className SkinAnalyzeResponseBean
 * @Description 皮肤分析接口返回对象
 * @Date 2021-07-26-10:31
 **/
@NoArgsConstructor
@Data
public class SkinAnalyzeResponseBean  extends BaiDuBaseResponseBean {
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private int face_num;
        private List<FaceListBean> face_list;

        @NoArgsConstructor
        @Data
        public static class FaceListBean {

            private String face_token;
            private LocationBean location;
            private SkinBean skin;
            private AcnespotmoleBean acnespotmole;
            private WrinkleBean wrinkle;
            private EyesattrBean eyesattr;
            private BlackheadporeBean blackheadpore;
            private SkinfaceBean skinface;

            @NoArgsConstructor
            @Data
            public static class LocationBean {

                private double left;
                private double top;
                private int width;
                private int height;
                private int degree;
                private int prob;
                private int conf;
            }

            @NoArgsConstructor
            @Data
            public static class SkinBean {

                private int color;
                private int smooth;
            }

            @NoArgsConstructor
            @Data
            public static class AcnespotmoleBean {

                private int acne_num;
                private int speckle_num;
                private int mole_num;
                private List<AcneListBean> acne_list;
                private List<SpeckleListBean> speckle_list;
                private List<MoleListBean> mole_list;

                @NoArgsConstructor
                @Data
                public static class AcneListBean {

                    private int type;
                    private double score;
                    private double left;
                    private double top;
                    private double right;
                    private double bottom;
                    private int center_x;
                    private int center_y;
                    private int radius;
                }

                @NoArgsConstructor
                @Data
                public static class SpeckleListBean {

                    private int type;
                    private double score;
                    private double left;
                    private double top;
                    private double right;
                    private double bottom;
                    private List<PositionBean> position;

                    @NoArgsConstructor
                    @Data
                    public static class PositionBean {

                        private int x;
                        private int y;
                    }
                }

                @NoArgsConstructor
                @Data
                public static class MoleListBean {

                    private int type;
                    private double score;
                    private double left;
                    private double top;
                    private double right;
                    private double bottom;
                    private List<PositionBeanX> position;

                    @NoArgsConstructor
                    @Data
                    public static class PositionBeanX {

                        private int x;
                        private int y;
                    }
                }
            }

            @NoArgsConstructor
            @Data
            public static class WrinkleBean {

                private int wrinkle_num;
                private List<Integer> wrinkle_types;
                private List<List<WrinkleDataBean>> wrinkle_data;
                @NoArgsConstructor
                @Data
                public static class WrinkleDataBean {

                    private int x;
                    private int y;
                }
            }

            @NoArgsConstructor
            @Data
            public static class EyesattrBean {
                private List<List<DarkCircleLeftBean>> dark_circle_left;
                private List<List<DarkCircleRightBean>> dark_circle_right;
                private List<Integer> dark_circle_left_type;
                private List<Integer> dark_circle_right_type;
                private List<List<EyeBagsLeftBean>> eye_bags_left;//左眼眼袋
                private List<List<EyeBagsRightBean>> eye_bags_right;//右眼眼袋

                @NoArgsConstructor
                @Data
                public static class EyeBagsLeftBean {

                    private int x;//眼袋离左边界的距离
                    private int y;//眼袋离上边界的距离
                }
                @NoArgsConstructor
                @Data
                public static class EyeBagsRightBean {

                    private int x;//眼袋离左边界的距离
                    private int y;//眼袋离上边界的距离
                }

                @NoArgsConstructor
                @Data
                public static class DarkCircleLeftBean {

                    private int x;
                    private int y;
                }

                @NoArgsConstructor
                @Data
                public static class DarkCircleRightBean {

                    private int x;
                    private int y;
                }
            }

            @NoArgsConstructor
            @Data
            public static class BlackheadporeBean {
                private List<?> poly;
                private List<?> circles;
            }

            @NoArgsConstructor
            @Data
            public static class SkinfaceBean {

                private SkinHealthCheckImagesBean skin_health_check_images;

                @NoArgsConstructor
                @Data
                public static class SkinHealthCheckImagesBean {

                    private String src_pic;
                    private String gray_pic;
                    private String brown_pic;
                    private String red_pic;
                }
            }
        }
    }
}
