package cn.ydxiaoshuai.sample.aisample.baidu.model;

import cn.hutool.core.bean.BeanUtil;
import cn.ydxiaoshuai.util.ParamDealUtil;
import lombok.Data;

import java.util.Map;

/**
 * @author 小帅丶
 * @className VehicleDetect
 * @Description 请求对象
 * @Date 2021-07-23-9:37
 **/
@Data
public class VehicleDetect {
    /**
     * 图像数据，Base64编码字符串，不超过4M。
     * 最短边至少50px，最长边最多4096px。
     * 支持图片格式：jpg，bmp，png。
     * 注意：图片的base64编码是不包含图片头的，如（data:image/jpg;base64,）
     * */
    private String image;
    /**
     * 图片完整URL，
     * URL长度不超过1024字节，URL对应的图片base64编码后大小不超过4M，
     * 最短边至少50px，最长边最大4096px，
     * 支持jpg/png/bmp格式，当image字段存在时url字段失效。
     * */
    private String url;
    /**
     * 只统计该矩形区域内的车辆数，缺省时为全图统计。
     * 逗号分隔，
     * */
    private String area;

    public String toParamString(VehicleDetect bean) throws Exception{
        Map<String, Object> map = BeanUtil.beanToMap(bean);
        String paramStr = ParamDealUtil.getParamStr(map);
        return paramStr;
    }
}
