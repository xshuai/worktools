package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className BaiDuBaseResponseBean
 * @Description 基础类
 * @Date 2021-07-08-11:00
 **/
@NoArgsConstructor
@Data
public class BaiDuBaseResponseBean {

    private long log_id;
    private String error_msg;
    private String error_code;
    private int timestamp;
    private int cached;
}
