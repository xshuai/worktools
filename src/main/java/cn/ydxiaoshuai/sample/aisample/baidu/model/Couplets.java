package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;

/**
 * @author 小帅丶
 * @className Couplets
 * @Description 春联参数对象
 * @Date 2022-01-27-11:23
 **/
@Data
public class Couplets {
    /**
     * 字符串（限5字符数以内）即春联的主题
     */
    private String text;
    /**
     * 整数 默认为数值为0，即第一幅春联。每换一次，数值加1即可，一定数量后会返回之前的春联结果。
     */
    private Integer index;
}
