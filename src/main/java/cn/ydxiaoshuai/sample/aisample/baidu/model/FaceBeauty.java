package cn.ydxiaoshuai.sample.aisample.baidu.model;

import cn.ydxiaoshuai.sample.aisample.baidu.common.FaceBeautyConts;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className FaceBeauty
 * @Description 请求对象
 * @Date 2021-06-28-13:34
 **/
@NoArgsConstructor
@Data
public class FaceBeauty {
    /**
     * 图片base64编码
     * */
    private String image;
    /**
     * 输出的图片质量(10~100)，默认80
     * */
    private Integer image_quality = 80;
    /**
     * 属性对象
     * */
    private PropertyBean property;

    @NoArgsConstructor
    @Data
    public static class PropertyBean {
        /**
         * 滤镜
         * */
        private List<EntiretyBean> entirety;
        /**
         * 美型
         * */
        private List<ShapeBean> shape;
        /**
         * 美颜
         * */
        private List<SkinBean> skin;
        /**
         * vis祛眼圈、眼袋、法令纹，（注意：该部分参数请求耗时较长，图片分辨率<4K）
         * */
        private List<VisskinBean> visskin;
        /**
         * 画质颜色
         * */
        private List<ColorBean> color;
        /**
         * 美妆
         * */
        private List<MakeupBean> makeup;

        @NoArgsConstructor
        @Data
        public static class EntiretyBean {
            /**
             * 风格
             * */
            private FaceBeautyConts.EntiretyStyle style;
            /**
             * 风格值
             * */
            private double value;
        }

        @NoArgsConstructor
        @Data
        public static class ShapeBean {
            /**
             * 风格
             * */
            private FaceBeautyConts.shapeStyle style = FaceBeautyConts.shapeStyle.defaultFace;
            /**
             * 风格值
             * */
            private double value;
        }

        @NoArgsConstructor
        @Data
        public static class SkinBean {
            /**
             * 风格
             * */
            private FaceBeautyConts.skinStyle style;
            /**
             * 风格值
             * */
            private double value;
        }

        @NoArgsConstructor
        @Data
        public static class VisskinBean {
            /**
             * 风格
             * */
            private FaceBeautyConts.visskinStyle style;
            /**
             * 风格值
             * */
            private double value;
        }

        @NoArgsConstructor
        @Data
        public static class ColorBean {
            /**
             * 风格
             * */
            private FaceBeautyConts.colorStyle style;
            /**
             * 风格值
             * */
            private double value;
        }

        @NoArgsConstructor
        @Data
        public static class MakeupBean {
            /**
             * 美妆类型
             * */
            private FaceBeautyConts.makeupKind kind;
            /**
             * 风格
             * */
            private String style;
            /**
             * 风格值
             * */
            private double value;
        }
    }
}
