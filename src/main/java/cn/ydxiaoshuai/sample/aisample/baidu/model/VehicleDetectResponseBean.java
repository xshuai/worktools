package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className VehicleDetectResponseBean
 * @Description 车辆检测接口返回对象
 * @Date 2021年7月23日10:05:36
 **/
@Data
@NoArgsConstructor
public class VehicleDetectResponseBean extends BaiDuBaseResponseBean {


    private VehicleNumBean vehicle_num;
    private List<VehicleInfoBean> vehicle_info;

    @NoArgsConstructor
    @Data
    public static class VehicleNumBean {
        private int motorbike;
        private int bus;
        private int carplate;
        private int car;
        private int truck;
        private int tricycle;
    }

    @NoArgsConstructor
    @Data
    public static class VehicleInfoBean {

        private double probability;
        private LocationBean location;
        private String type;

        @NoArgsConstructor
        @Data
        public static class LocationBean {

            private int top;
            private int left;
            private int width;
            private int height;
        }
    }
}
