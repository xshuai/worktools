package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className CoupletsResponse
 * @Description 智能春联返回对象
 * @Date 2022-01-27-11:33
 **/
@NoArgsConstructor
@Data
public class CoupletsResponse {

    private CoupletsBean couplets;
    private long log_id;

    @NoArgsConstructor
    @Data
    public static class CoupletsBean {
        private String center;
        private String first;
        private String second;
    }
}
