package cn.ydxiaoshuai.sample.aisample.baidu.face;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.sample.aisample.baidu.common.BaiDuConts;
import cn.ydxiaoshuai.sample.aisample.baidu.model.FaceListBean;
import cn.ydxiaoshuai.sample.aisample.baidu.model.SkinAnalyze;
import cn.ydxiaoshuai.sample.aisample.baidu.model.SkinAnalyzeResponseBean;
import cn.ydxiaoshuai.util.DealDataUtil;
import cn.ydxiaoshuai.util.DrawRectUtil;
import com.alibaba.fastjson.JSON;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * @author 小帅丶
 * @className SkinAnalyze
 * @Description 皮肤分析
 * @Date 2021-07-26-10:17
 **/
public class SkinAnalyzeSample {
    /** 接口必要的AccessToken */
    private static String access_token = "";
    /** 图片本地路径 */
    private static String imagePath = "F://testimg//doudou.jpg";
    /** 应用APPID */
    private static String appid = "";

    public static void main(String[] args) throws Exception{
        SkinAnalyze bean = new SkinAnalyze();
        byte[] bytes = FileUtil.readBytes(imagePath);
        String base64 = Base64.encode(bytes);
        bean.setAppid(appid);
        bean.setImage(base64);
        bean.setImage_type("BASE64");
        bean.setMax_face_num(10);
        bean.setFace_field("color,smooth,acnespotmole,wrinkle,eyesattr,blackheadpore,skinface");

        String params = JSON.toJSONString(bean);

        String result = HttpUtil.post(BaiDuConts.SKIN_ANALYZE_URL + "?access_token=" + access_token, params);

        System.out.println("result = " + result);

        SkinAnalyzeResponseBean faceBeautyResponseBean = JSON.parseObject(result, SkinAnalyzeResponseBean.class);

        if (faceBeautyResponseBean.getError_code().equals("0")) {
            FaceListBean dealData = DrawRectUtil.cuttingBySkinAnalyzeDetect(bytes, faceBeautyResponseBean);
            //自带方法的裁剪
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            BufferedImage image = ImageIO.read(byteArrayInputStream);
            SkinAnalyzeResponseBean.ResultBean.FaceListBean faceListBean = faceBeautyResponseBean.getResult().getFace_list().get(0);
            int left = (int) faceListBean.getLocation().getLeft();
            int top = (int) faceListBean.getLocation().getTop();
            int width = faceListBean.getLocation().getWidth();
            int height = faceListBean.getLocation().getHeight();
            //返回由指定矩形区域定义的子图像
            BufferedImage subImage = image.getSubimage(left, top, width, height);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(subImage, "jpg", outputStream);
            String base64Img = Base64.encode(outputStream.toByteArray());
            DealDataUtil.drawImageToFrame(base64Img, "裁剪的人脸");

            //图片标注
            DealDataUtil.drawImageToFrame(dealData.getImage_acnespotmole(),"斑痘痣");
            DealDataUtil.drawImageToFrame(dealData.getImage_wrinkle(),"皱纹");
            DealDataUtil.drawImageToFrame(dealData.getImage_eyesattr(),"黑眼圈");
            List<String> image_base64 = dealData.getImage_base64();
            for (int i = 0; i < image_base64.size(); i++) {
                //其他方法图片裁剪
                DealDataUtil.drawImageToFrame(image_base64.get(i), "裁剪的人脸"+(i+1));
            }
        } else {
            System.out.println("接口出错了 " + faceBeautyResponseBean.getError_code() + "-" +faceBeautyResponseBean.getError_msg());
        }
    }
}
