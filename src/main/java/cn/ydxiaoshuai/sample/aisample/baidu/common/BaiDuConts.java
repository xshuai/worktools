package cn.ydxiaoshuai.sample.aisample.baidu.common;

/**
 * @author 小帅丶
 * @className BaiDuConts
 * @Description 接口地址
 * @Date 2021-07-08-10:25
 **/
public class BaiDuConts {
    /** 人脸检查-接口地址 */
    public static final String FACE_DETECT_URL = "https://aip.baidubce.com/rest/2.0/face/v3/detect";
    /** 智能春联-接口地址 */
    public static final String COUPLETS_URL = "https://aip.baidubce.com/rpc/2.0/creation/v1/couplets";
    /** 高清人像美化-接口地址 */
    public static String FACE_BEAUTY_URL = "https://aip.baidubce.com/rest/2.0/face/v1/FaceBeauty";
    /** 皮肤分析-接口地址 */
    public static String SKIN_ANALYZE_URL = "https://aip.baidubce.com/rest/2.0/face/v1/skin_analyze";
    /** 车辆检测-接口地址 */
    public static String VEHICLE_DETECT_URL = "https://aip.baidubce.com/rest/2.0/image-classify/v1/vehicle_detect";
    /** 语言处理技术-新闻摘要-接口地址 */
    public static String NEWS_SUMMARY_URL = "https://aip.baidubce.com/rpc/2.0/nlp/v1/news_summary";
}
