package cn.ydxiaoshuai.sample.aisample.baidu.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceBeautyResponseBean
 * @Description 高清人像美化接口返回对象
 * @Date 2021-07-08-10:58
 **/
@NoArgsConstructor
@Data
public class FaceBeautyResponseBean extends BaiDuBaseResponseBean {

    private String image;
}
