package cn.ydxiaoshuai.sample.aisample.baidu.face;


import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.sample.aisample.baidu.common.BaiDuConts;
import cn.ydxiaoshuai.sample.aisample.baidu.model.FaceLandMark150Bean;
import cn.ydxiaoshuai.util.DealDataUtil;
import com.alibaba.fastjson.JSON;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

/**
 * @author 小帅丶
 * @className FaceBeautySample
 * @Description 情侣拼脸-示例代码
 * @Date 2021年10月27日
 **/
public class CPFaceSample {

    /** 接口必要的AccessToken */
    private static String access_token = "";
    /** 宽 */
    private static int height;
    /** 高 */
    private static int width;


    public static void main(String[] args) throws Exception{
        /** 图片本地路径 - 女方 */
        String imagePathG = "F://testimg//cpface//girl02.jpg";
        /** 图片本地路径 - 男方 */
        String imagePathB = "F://testimg//cpface//boy02.jpg";
        /** 图片保存路径 */
         String imagePath = "F:\\testimg\\cpface\\";

        long startTime = System.currentTimeMillis();
        BufferedImage cpFace = getCPFace(imagePathB, imagePathG);
        //保存到本地
        ImageIO.write(cpFace,"jpg",new File(imagePath+System.currentTimeMillis()+".jpg"));
        long endTime = System.currentTimeMillis();
        System.out.println("耗时:"+(endTime-startTime));
    }
    /**
     * @Author 小帅丶
     * @Description 情侣拼脸
     *              建议两张图片宽高一致、比例一致哦~
     * @Date  2021-10-27 13:42
     * @param leftPath - 左边的人脸照片本地路径
     * @param rightPath - 右边的人脸照片本地路径
     * @return java.awt.image.BufferedImage
     **/
    public static BufferedImage getCPFace(String leftPath,String rightPath) throws Exception{
        //进行左右脸检测并分割成新的图片
        BufferedImage subImageLeft = getLeftFace(leftPath);
        BufferedImage subImageRight = getRightFace(rightPath);
        //计算高度，如果一致则height取其一即可
        if(subImageLeft.getHeight()>subImageRight.getHeight()){
            height = subImageLeft.getHeight();
        }else{
            height = subImageRight.getHeight();
        }
        //计算图片总宽度
        width = subImageLeft.getWidth() + subImageRight.getWidth();
        //创建一个BufferedImage对象
        BufferedImage bufferedImage = new BufferedImage(width,height,
                BufferedImage.TYPE_INT_RGB);
        //创建一个Graphics2D绘图对象
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(subImageLeft, 0, 0, null);
        graphics2D.drawImage(subImageRight, subImageLeft.getWidth(), 0, null);
        graphics2D.dispose();
        return bufferedImage;
    }
    /**
     * @Author 小帅丶
     * @Description 获取男方的左脸
     * @Date  2021-10-27 9:32
     * @param imagePath - 人脸图片
     * @return java.lang.String
     **/
    public static BufferedImage getLeftFace(String imagePath) throws Exception{
        FaceLandMark150Bean bean = getFaceMark(imagePath);
        //获取鼻梁第一个坐标
        double noseX = bean.getResult().getFace_list().get(0).getLandmark150().getNose_bridge_1().getX();
        //裁剪图片
        BufferedImage bufferedImage = DealDataUtil.base64ToBufferedImage(bean.getDeal_image());
        BufferedImage subImage = bufferedImage.getSubimage(0, 0,
                (int) noseX, bufferedImage.getHeight());
        return subImage;
    }
    /**
     * @Author 小帅丶
     * @Description 获取女方的右脸
     * @Date  2021-10-27 9:32
     * @param imagePath - 人脸图片
     * @return java.lang.String
     **/
    public static BufferedImage getRightFace(String imagePath){
        FaceLandMark150Bean bean = getFaceMark(imagePath);
        //获取鼻梁第一个坐标
        double noseX = bean.getResult().getFace_list().get(0).getLandmark150().getNose_bridge_1().getX();
        //裁剪图片
        BufferedImage bufferedImage = DealDataUtil.base64ToBufferedImage(bean.getDeal_image());

        BufferedImage subImage = bufferedImage.getSubimage((int) noseX, 0,
                bufferedImage.getWidth()-(int) noseX, bufferedImage.getHeight());
        return subImage;
    }

    /**
     * @Author 小帅丶
     * @Description 获取人脸检测数据
     * @Date  2021-10-27 10:49
     * @param imagePath - 人脸图片
     * @return cn.ydxiaoshuai.sample.aisample.baidu.model.FaceLandMark150Bean
     **/
    private static FaceLandMark150Bean getFaceMark(String imagePath) {
        byte [] image = FileUtil.readBytes(imagePath);
        String base64 = Base64.encode(image);
        //获取人脸信息
        HashMap<String,String> options = new HashMap<>();
        options.put("max_face_num", "10");
        options.put("face_field", "landmark150");
        options.put("image_type", "BASE64");
        options.put("image", base64);
        String result = HttpUtil.post(BaiDuConts.FACE_DETECT_URL + "?access_token=" + access_token,
                JSON.toJSONString(options));
        FaceLandMark150Bean bean = JSON.parseObject(result,
                FaceLandMark150Bean.class);
        bean.setDeal_image(base64);
        return bean;
    }
}
