package cn.ydxiaoshuai.sample.aisample.baidu.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author 小帅丶
 * @className NewsSummary
 * @Description 请求对象
 * @Date 2021-10-11-13:56
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewsSummary {
    /** 字符串（限200字符数）字符串仅支持GBK编码，
     *      长度需小于200字符数（即400字节），请输入前确认字符数没有超限，若字符数超长会返回错误。
     *      标题在算法中具有重要的作用，若文章确无标题，不必输入此参数  */
    private String title;
    /** 字符串（限3000字符数以内）
            字符串仅支持GBK编码，长度需小于3000字符数（即6000字节），
            请输入前确认字符数没有超限，若字符数超长会返回错误。
            正文中如果包含段落信息，请使用"\n"分隔，段落信息算法中有重要的作用，请尽量保留  */
    private String content;
    /** 此数值将作为摘要结果的最大长度。
     *      例如：原文长度1000字，本参数设置为150，则摘要结果的最大长度是150字；
     *      推荐最优区间：200-500字 */
    private Integer max_summary_len;
}
