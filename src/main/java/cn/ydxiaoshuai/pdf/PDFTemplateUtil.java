package cn.ydxiaoshuai.pdf;

import cn.hutool.core.convert.Convert;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;


/**
 * Description 生成指定格式的PDF
 * ProjectName worktools
 * Created by 小帅丶 on 2022-12-20 17:51.
 * Version 1.0
 */

public class PDFTemplateUtil {
    //制表符
    private static String TAB_STR = "\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0";

    //一年12个月计算
    private static Integer YEAR_MONTH = 12;

    /**
     * @Author 小帅丶
     * @Description 数据填充
     * @Date  2022/12/21 15:40
     * @param name - 姓名
     * @param idCard - 身份证号码
     * @param idCardAddress - 身份证地址
     * @param orgName - 组织名称
     * @param orgCode - 组织注册号
     * @param orgAddress  - 组织注册地址
     * @param prodBizz - 生产企业
     * @param distrBizz - 经销企业
     * @param machineMoel - 机具型号
     * @param buyDate - 购机日期
     * @param invoiceNum - 发票号
     * @param buyNum - 购置数量
     * @param salePrice - 销售单价
     * @param saleTotalAmount - 销售总额
     * @param factoryCode - 出厂编号
     * @param machineCode - 发动机出厂编号
     * @param engineModel - 发动机型号
     * @param loanAmount - 贷款金额
     * @param loanTerm - 贷款月份
     * @param loanStartAndEndDate - 贷款起止日期
     * @param loanYearRate - 贷款年利率
     * @param loanInterest - 贷款利息
     * @param subsidyRate - 贴息利率
     * @param subsidyMonth - 贴息期限 月份
     * @param subsidyScale - 贴息比例
     * @return java.io.ByteArrayOutputStream
     **/
    public static ByteArrayOutputStream fillDataToPDF(String name, String idCard, String idCardAddress,
                                                      String orgName, String orgCode, String orgAddress,
                                                      String prodBizz, String distrBizz, String machineMoel,
                                                      String buyDate, String invoiceNum, String buyNum,
                                                      String salePrice, String saleTotalAmount,
                                                      String factoryCode, String machineCode, String engineModel,
                                                      String loanAmount, String loanTerm, String loanStartAndEndDate,
                                                      String loanYearRate, String loanInterest, BigDecimal subsidyRate,
                                                      Integer subsidyMonth,
                                                      BigDecimal subsidyScale) throws Exception{
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        long startTime = System.currentTimeMillis();
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document,outputStream);
        document.open();
        document.addAuthor("码小帅");
        document.addCreationDate();
        document.addCreator("https://www.ydxiaoshuai.cn/");
        document.addKeywords("小帅丶 一个即将淘汰的程序员");
        document.addTitle("小帅丶 一个即将淘汰的程序员");
        document.addSubject("小帅丶 一个即将淘汰的程序员");
        //顶部说明
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(102);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        Font font = PDFFontUtil.getColorFont(BaseColor.BLACK, 18, FontCustomerFamily.DENGXIAN_BOLD);
        PdfPCell cell = new PdfPCell(new Phrase("信息表",font));
        cell.setPaddingTop(30f);
        cell.setPaddingBottom(20f);
        cell.setMinimumHeight(40);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        document.add(table);

        //数据表格
        PdfPTable dataTable = new PdfPTable(7);
        dataTable.setWidthPercentage(102);
        //申请者基础信
        PdfPCell cellA = new PdfPCell(new Phrase("申请者基础信\n\n" +
                "息", PDFFontUtil.getColorFont(BaseColor.BLACK,
                10.6f, FontCustomerFamily.DENGXIAN_BOLD)));
        cellA = getBaseCell(cellA,1,20,0.1f);
        cellA.setRowspan(4);
        PdfPCell cellApplyPerson = getDataBaseCell("申请者为个人",3,20,10.6f,
                FontCustomerFamily.DENGXIAN_BOLD,null);
        PdfPCell cellApplyOrg = getDataBaseCell("申请者为农业生产经营组织",3,20,10.6f,
                FontCustomerFamily.DENGXIAN_BOLD,null);

        PdfPCell cellName = getDataBaseCell("姓名",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellNameContent = getDataBaseCell(name,2,FontCustomerFamily.DENGXIAN);
        PdfPCell cellOrg = getDataBaseCell("组织名称",1,FontCustomerFamily.DENGXIAN);
        PdfPCell cellOrgContent = getDataBaseCell(orgName ,2,FontCustomerFamily.DENGXIAN);
        PdfPCell cellIdCard = getDataBaseCell("身份证号码",1,35,7.9f,
                FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellIdCardContent = getDataBaseCell(idCard,2,35,7.9f,
                FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellOrgCode = getDataBaseCell("组织机构代码证/工\n\n" +
                "商营业执照注册号",1,35,7.9f, FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellOrgCodeContent = getDataBaseCell(orgCode ,2,35,7.9f,
                FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellIdCardAddress = getDataBaseCell("身份证地址",1,25,7.9f,
                FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellIdCardAddressContent = getDataBaseCell(idCardAddress,2,25,7.9f,
                FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellOrgAddress = getDataBaseCell("注册地",1,25,7.9f,
                FontCustomerFamily.DENGXIAN,null);
        PdfPCell cellOrgAddressContent = getDataBaseCell(orgAddress,2,25,7.9f,
                FontCustomerFamily.DENGXIAN,null);

        dataTable.addCell(cellA);
        dataTable.addCell(cellApplyPerson);
        dataTable.addCell(cellApplyOrg);

        dataTable.addCell(cellName);
        dataTable.addCell(cellNameContent);
        dataTable.addCell(cellOrg);
        dataTable.addCell(cellOrgContent);

        dataTable.addCell(cellIdCard);
        dataTable.addCell(cellIdCardContent);
        dataTable.addCell(cellOrgCode);
        dataTable.addCell(cellOrgCodeContent);

        dataTable.addCell(cellIdCardAddress);
        dataTable.addCell(cellIdCardAddressContent);
        dataTable.addCell(cellOrgAddress);
        dataTable.addCell(cellOrgAddressContent);

        //申请贷款贴息机具信息
        PdfPCell cellB = new PdfPCell(new Phrase("申请贷款贴息\n\n" +
                "机具信息", PDFFontUtil.getColorFont(BaseColor.BLACK,
                10.6f, FontCustomerFamily.DENGXIAN_BOLD)));
        cellB = getBaseCell(cellB,1,20,0.1f);
        cellB.setRowspan(6);

        PdfPCell cellME = getDataBaseCell("生产企业",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellMEContent = getDataBaseCell(prodBizz ,2, FontCustomerFamily.DENGXIAN);
        PdfPCell cellDE = getDataBaseCell("经销企业",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellDEContent = getDataBaseCell(distrBizz ,2, FontCustomerFamily.DENGXIAN);

        PdfPCell cellModel = getDataBaseCell("机具型号",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellModelContent = getDataBaseCell(machineMoel,1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellBuyDate = getDataBaseCell("购机日期",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellBuyDateContent = getDataBaseCell(buyDate,1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellInvoiceNum = getDataBaseCell("发票号",1,FontCustomerFamily.DENGXIAN);
        PdfPCell cellInvoiceNumContent = getDataBaseCell(invoiceNum,1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellBuyNum = getDataBaseCell("购置数量",1,FontCustomerFamily.DENGXIAN);
        PdfPCell cellBuyNumContent = getDataBaseCell(buyNum,1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellSalePrice = getDataBaseCell("销售单价（元）",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellSalePriceContent = getDataBaseCell(salePrice,1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellSaleTotalPrice = getDataBaseCell("销售总额（元）",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellSaleTotalPriceContent = getDataBaseCell(saleTotalAmount,1,FontCustomerFamily.DENGXIAN);
        PdfPCell cellFactoryCode = getDataBaseCell("整机出厂编号",1, FontCustomerFamily.DENGXIAN);
        PdfPCell cellFactoryCodeContent = getDataBaseCell(factoryCode,5, FontCustomerFamily.DENGXIAN);
        PdfPCell cellEngineCode = getDataBaseCell("发动机出厂编号",1,FontCustomerFamily.DENGXIAN);
        PdfPCell cellEngineCodeContent = getDataBaseCell(machineCode ,5,FontCustomerFamily.DENGXIAN);
        PdfPCell cellEngineModel = getDataBaseCell("发动机型号",1,FontCustomerFamily.DENGXIAN);
        PdfPCell cellEngineModelContent = getDataBaseCell(engineModel,5,FontCustomerFamily.DENGXIAN);

        dataTable.addCell(cellB);
        dataTable.addCell(cellME);
        dataTable.addCell(cellMEContent);
        dataTable.addCell(cellDE);
        dataTable.addCell(cellDEContent);

        dataTable.addCell(cellModel);
        dataTable.addCell(cellModelContent);
        dataTable.addCell(cellBuyDate);
        dataTable.addCell(cellBuyDateContent);
        dataTable.addCell(cellInvoiceNum);
        dataTable.addCell(cellInvoiceNumContent);

        dataTable.addCell(cellBuyNum);
        dataTable.addCell(cellBuyNumContent);
        dataTable.addCell(cellSalePrice);
        dataTable.addCell(cellSalePriceContent);
        dataTable.addCell(cellSaleTotalPrice);
        dataTable.addCell(cellSaleTotalPriceContent);

        dataTable.addCell(cellFactoryCode);
        dataTable.addCell(cellFactoryCodeContent);
        dataTable.addCell(cellEngineCode);
        dataTable.addCell(cellEngineCodeContent);
        dataTable.addCell(cellEngineModel);
        dataTable.addCell(cellEngineModelContent);

        //贷款信息
        PdfPCell cellC = new PdfPCell(new Phrase("贷款信息", PDFFontUtil.getColorFont(BaseColor.BLACK,
                10.6f, FontCustomerFamily.DENGXIAN_BOLD)));
        cellC = getBaseCell(cellC,1,20,0.1f);
        cellC.setRowspan(4);

        PdfPCell cellLoanAmount = getDataBaseCell("贷款金额",1, FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);
        String loanAmountStr = " （大写）："+ Convert.digitToChinese(new Double(loanAmount)) +TAB_STR+"（小写）："+loanAmount;
        PdfPCell cellLoanAmountContent = getDataBaseCell(loanAmountStr,5, FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);

        PdfPCell cellLoanTerm = getDataBaseCell("贷款期限（大写）",1,FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);
        PdfPCell cellLoanTermContent = getDataBaseCell(TAB_STR+loanTerm+ "个月，"+loanStartAndEndDate,5,
                FontCustomerFamily.DENGXIAN,Element.ALIGN_LEFT);

        PdfPCell cellLoanYearRate = getDataBaseCell("贷款年利率",1, FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);
        PdfPCell cellLoanYearRateContent = getDataBaseCell(TAB_STR+loanYearRate +"%",1,
                FontCustomerFamily.DENGXIAN,Element.ALIGN_LEFT);
        PdfPCell cellRMBLPR = getDataBaseCell("人民币贷款基准利率（1 年）",3, FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);
        PdfPCell cellRMBLPRContent = getDataBaseCell(TAB_STR+"4.35%",1, FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);

        PdfPCell cellLoanInterest = getDataBaseCell("贷款利息",1,FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);
        String loanInterestStr = " （大写）："+ Convert.digitToChinese(new Double(loanInterest )) +TAB_STR+"（小写）："+loanInterest;
        PdfPCell cellLoanInterestContent = getDataBaseCell(loanInterestStr,5, FontCustomerFamily.DENGXIAN,
                Element.ALIGN_LEFT);

        dataTable.addCell(cellC);
        dataTable.addCell(cellLoanAmount);
        dataTable.addCell(cellLoanAmountContent);

        dataTable.addCell(cellLoanTerm);
        dataTable.addCell(cellLoanTermContent);

        dataTable.addCell(cellLoanYearRate);
        dataTable.addCell(cellLoanYearRateContent);
        dataTable.addCell(cellRMBLPR);
        dataTable.addCell(cellRMBLPRContent);

        dataTable.addCell(cellLoanInterest);
        dataTable.addCell(cellLoanInterestContent);

        //相关承诺事项
        PdfPCell cellD = new PdfPCell(new Phrase("相关承诺事项", PDFFontUtil.getColorFont(BaseColor.BLACK,
                10.6f, FontCustomerFamily.DENGXIAN_BOLD)));
        cellD = getBaseCell(cellD,1,20,0.1f);
        cellD.setRowspan(3);
        PdfPCell cellLoanOrg = getDataBaseCell("贷款机构",3,20,10.6f,
                FontCustomerFamily.DENGXIAN_BOLD,null);
        cellLoanOrg.setBorderWidthBottom(0);
        cellLoanOrg.setBorderWidthLeft(0);
        cellLoanOrg.setBorderWidthRight(0);
        PdfPCell cellApply = getDataBaseCell("申请者",3,20,10.6f,
                FontCustomerFamily.DENGXIAN_BOLD,null);
        PdfPCell cellLoanOrgContentA = new PdfPCell();
        cellLoanOrgContentA.setColspan(3);
        cellLoanOrgContentA.setBorderWidthBottom(0);
        cellLoanOrgContentA.setBorderWidthLeft(0);
        cellLoanOrgContentA.setBorderWidthRight(0);
        Element elementA = getBasePhrase("1.本机构给申请者贷款用于购买农机产品，对发放贷款的真实性承担相应的法律责任；",
                FontCustomerFamily.DENGXIAN);
        Element elementB = getBasePhrase("2.本机构对提供的上述信息和材料真实性负责，并承担法律责任；",
                FontCustomerFamily.DENGXIAN);
        Element elementC = getBasePhrase("3.如发现申请者虚假申请、提前还款将及时主动报告当地财政部门或农机化主管部门。",
                FontCustomerFamily.DENGXIAN);
        Element elementD = getBasePhrase("贷款银行或信用社（公章）：",FontCustomerFamily.DENGXIAN);
        cellLoanOrgContentA.addElement(elementA);
        cellLoanOrgContentA.addElement(elementB);
        cellLoanOrgContentA.addElement(elementC);
        cellLoanOrgContentA.addElement(elementD);
        cellLoanOrgContentA.addElement(new Phrase("\n"));
        PdfPCell cellApplyContentA = new PdfPCell();
        cellApplyContentA.setColspan(3);
        cellApplyContentA.setBorderWidthBottom(0);
        Element elementRA =  getBasePhrase("1.本人已知政策规定，对提供的上述信息和材料真实性负责，并承担法律责任；",
                FontCustomerFamily.DENGXIAN);
        Element elementRB =  getBasePhrase("2.如有提前还款，将主动想农机化主管部门报告。申请者签字（手印）：",
                FontCustomerFamily.DENGXIAN);
        cellApplyContentA.addElement(elementRA);
        cellApplyContentA.addElement(elementRB);
        cellApplyContentA.addElement(new Phrase("\n"));

        PdfPCell cellYMDL = getDataBaseCell("      年     月     日"+TAB_STR,3,20,
                9.1f, FontCustomerFamily.DENGXIAN,Element.ALIGN_RIGHT);
        cellYMDL.setBorder(Rectangle.RIGHT);
        PdfPCell cellYMDR = getDataBaseCell("      年     月     日"+TAB_STR,3, 20,
                9.1f,FontCustomerFamily.DENGXIAN,Element.ALIGN_RIGHT);
        cellYMDR.setBorder(Rectangle.RIGHT);


        dataTable.addCell(cellD);
        dataTable.addCell(cellLoanOrg);
        dataTable.addCell(cellApply);

        dataTable.addCell(cellLoanOrgContentA);
        dataTable.addCell(cellApplyContentA);

        dataTable.addCell(cellYMDL);
        dataTable.addCell(cellYMDR);

        //县级农机化主管部门意见
        PdfPCell cellE = new PdfPCell(new Phrase("县级农机化主\n\n" +
                "管部门意见", PDFFontUtil.getColorFont(BaseColor.BLACK,
                10.6f, FontCustomerFamily.DENGXIAN_BOLD)));
        cellE = getBaseCell(cellE,1,20,0.1f);
        cellE.setRowspan(2);
        PdfPCell cellEContent = new PdfPCell();
        cellEContent.setColspan(6);
        cellEContent.setBorderWidthBottom(0);
        BigDecimal loanAmountBD = new BigDecimal(loanAmount);
        BigDecimal subsidyRatePoint = subsidyRate.divide(BigDecimal.valueOf(100));
        BigDecimal subsidyScalePoint = subsidyScale.divide(BigDecimal.valueOf(100));
        BigDecimal subsidyAmountBD = loanAmountBD.multiply(subsidyRatePoint).multiply(subsidyScalePoint);
        if(subsidyMonth>YEAR_MONTH || subsidyMonth<YEAR_MONTH){
            BigDecimal divide = subsidyAmountBD.divide(BigDecimal.valueOf(YEAR_MONTH));
            subsidyAmountBD = divide.multiply(BigDecimal.valueOf(subsidyMonth));
        }
        subsidyAmountBD = subsidyAmountBD.setScale(2, BigDecimal.ROUND_HALF_UP);
        Element elementEA = getBasePhrase("按照贴息方案贷款贴息额计算方式，" +
                "金额 "+loanAmount +" 元 X 贴息利率 "+subsidyRate+"% X 贴息期限 "+subsidyMonth +"月 X "+
                subsidyScale +"% ="+subsidyAmountBD+" 元，" +"确认贷款贴息额为（大写）："+
                Convert.digitToChinese(subsidyAmountBD)+"（小写）："+subsidyAmountBD+" 元",FontCustomerFamily.DENGXIAN);
        Element elementEB = getBasePhrase("县级农机化主管部门（公章）：",FontCustomerFamily.DENGXIAN);
        cellEContent.addElement(elementEA);
        cellEContent.addElement(elementEB);

        PdfPCell cellEYMDR = getDataBaseCell("      年     月     日"+TAB_STR,6, 20,
                9.1f,FontCustomerFamily.DENGXIAN,Element.ALIGN_RIGHT);
        cellEYMDR.setBorder(Rectangle.RIGHT);

        dataTable.addCell(cellE);
        dataTable.addCell(cellEContent);
        dataTable.addCell(cellEYMDR);

        //最后一行
        PdfPCell cellFooter = new PdfPCell(new Phrase("表格说明", PDFFontUtil.getColorFont(BaseColor.BLACK,
                10.6f, FontCustomerFamily.DENGXIAN_BOLD)));
        cellFooter = getBaseCell(cellFooter,1,20,0.1f);
        PdfPCell cellFooterContent = new PdfPCell(new Phrase("在办理贷款贴息过程中，农机化主管部门需留一份原件存档。",
                PDFFontUtil.getColorFont(BaseColor.BLACK,
                9.1f, FontCustomerFamily.DENGXIAN)));
        cellFooterContent.setColspan(6);
        cellFooterContent.setMinimumHeight(20);
        cellFooterContent.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellFooterContent.setVerticalAlignment(Element.ALIGN_MIDDLE);
        dataTable.addCell(cellFooter);
        dataTable.addCell(cellFooterContent);
        document.add(dataTable);
        document.add(Chunk.NEWLINE);
        System.out.println("PDF created ====> 耗时:"+(System.currentTimeMillis()-startTime));
        document.close();
        return outputStream;
    }
    /**
     * @Author 小帅丶
     * @Description cell添加内容
     * @Date  2022/12/21 15:31
     * @param content - 内容
     * @param fontSize - 字体大小
     * @param leading - 行间距
     * @param fontCustomerFamily - 字体
     * @return com.itextpdf.text.Element
     **/
    private static Element getBasePhrase(String content, float fontSize, float leading, FontCustomerFamily fontCustomerFamily) {
        Phrase phrase = new Phrase(content, PDFFontUtil.getColorFont(BaseColor.BLACK,
                fontSize,fontCustomerFamily));
        phrase.setLeading(leading);
        return phrase;
    }

    /**
     * @Author 小帅丶
     * @Description cell添加内容
     * @Date  2022/12/21 15:31
     * @param content - 内容
     * @param fontCustomerFamily - 字体
     * @return com.itextpdf.text.Element
     **/
    private static Element getBasePhrase(String content, FontCustomerFamily fontCustomerFamily) {
        return getBasePhrase(content,9.1f,15f,fontCustomerFamily);
    }

    /**
     * @Description cell内容填充
     * @param content - 内容
     * @param colspan - 合并几列
     * @param fontCustomerFamily - 字体
     * @Author 小帅丶
     * @Date  2022/12/21 15:33
     * @return com.itextpdf.text.pdf.PdfPCell
     **/
    public static PdfPCell getDataBaseCell(String content, int colspan,
                                            FontCustomerFamily fontCustomerFamily) {
        return getDataBaseCell(content,colspan,25,7.9f,fontCustomerFamily,null);
    }

    /**
     * @Description cell内容填充
     * @param content - 内容
     * @param colspan - 合并几列
     * @param fontCustomerFamily - 字体
     * @param horizontalAlign - 文本对齐方式
     * @Author 小帅丶
     * @Date  2022/12/21 15:33
     * @return com.itextpdf.text.pdf.PdfPCell
     **/
    private static PdfPCell getDataBaseCell(String content, int colspan,
                                            FontCustomerFamily fontCustomerFamily,Integer horizontalAlign) {
        return getDataBaseCell(content,colspan,25,7.9f,fontCustomerFamily,horizontalAlign);
    }

    /**
     * @Description cell内容填充
     * @param content - 内容
     * @param colspan - 合并几列
     * @param cellHeight - 行高
     * @param fontSize - 字体大小
     * @param fontCustomerFamily - 字体
     * @param horizontalAlign - 文本对齐方式
     * @Author 小帅丶
     * @Date  2022/12/21 15:33
     * @return com.itextpdf.text.pdf.PdfPCell
     **/
    public static PdfPCell getDataBaseCell(String content, int colspan, int cellHeight,float fontSize,
                                            FontCustomerFamily fontCustomerFamily,Integer horizontalAlign) {
        Phrase phrase = new Phrase(content, PDFFontUtil.getColorFont(BaseColor.BLACK,
                fontSize, fontCustomerFamily));
        PdfPCell cell = new PdfPCell(phrase);
        cell.setColspan(colspan);
        cell.setMinimumHeight(cellHeight);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        if(null==horizontalAlign){
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        } else {
            cell.setHorizontalAlignment(horizontalAlign);
        }
        return cell;
    }

    /**
     * @Description cell内容填充
     * @param cell - 内容
     * @param colspan - 合并几列
     * @param cellHeight - 行高
     * @param borderWidth - 边框宽度
     * @Author 小帅丶
     * @Date  2022/12/21 15:33
     * @return com.itextpdf.text.pdf.PdfPCell
     **/
    public static PdfPCell getBaseCell(PdfPCell cell,int colspan, int cellHeight, float borderWidth) {
        cell.setColspan(colspan);
        cell.setMinimumHeight(cellHeight);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.BOX);
        cell.setBorderColor(BaseColor.BLACK);
        cell.setBorderWidth(borderWidth);
        return cell;
    }
}
