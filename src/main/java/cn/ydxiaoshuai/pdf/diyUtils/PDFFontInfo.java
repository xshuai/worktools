package cn.ydxiaoshuai.pdf.diyUtils;


import org.apache.fontbox.FontBoxFont;
import org.apache.fontbox.ttf.OTFParser;
import org.apache.fontbox.ttf.TTFParser;
import org.apache.fontbox.type1.Type1Font;
import org.apache.pdfbox.pdmodel.font.CIDSystemInfo;
import org.apache.pdfbox.pdmodel.font.FontFormat;
import org.apache.pdfbox.pdmodel.font.FontInfo;
import org.apache.pdfbox.pdmodel.font.PDPanoseClassification;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 自定义pdf字体信息
 *
 * @author xwt
 * @date 2022/10/19 17:23
 */
public class PDFFontInfo extends FontInfo {

    private final String postScriptName;
    private final FontFormat format;
    private final CIDSystemInfo cidSystemInfo;
    private final int usWeightClass;
    private final int sFamilyClass;
    private final int ulCodePageRange1;
    private final int ulCodePageRange2;
    private final int macStyle;
    private final File file;
    private final FontBoxFont boxFont;

    public PDFFontInfo(File file, FontFormat format, String postScriptName,
                       CIDSystemInfo cidSystemInfo, int usWeightClass, int sFamilyClass,
                       int ulCodePageRange1, int ulCodePageRange2, int macStyle) {
        this.file = file;
        this.format = format;
        this.postScriptName = postScriptName;
        this.cidSystemInfo = cidSystemInfo;
        this.usWeightClass = usWeightClass;
        this.sFamilyClass = sFamilyClass;
        this.ulCodePageRange1 = ulCodePageRange1;
        this.ulCodePageRange2 = ulCodePageRange2;
        this.macStyle = macStyle;
        try {
            switch (format) {
                case PFB:
                    this.boxFont = Type1Font.createWithPFB(new FileInputStream(file));
                    break;
                case OTF:
                    this.boxFont = new OTFParser(false, true).parse(file);
                    break;
                case TTF:
                    this.boxFont = new TTFParser(false, true).parse(file);
                    break;
                default:
                    throw new RuntimeException("can't happen");
            }
        } catch (IOException e) {
            throw new RuntimeException("pdfbox自定义字体失败！");
        }
    }

    public PDFFontInfo(File file, FontFormat format, String postScriptName) {
        this(file, format, postScriptName, null, 0, 0, 0, 0, 0);
    }

    @Override
    public String getPostScriptName() {
        return postScriptName;
    }

    @Override
    public FontFormat getFormat() {
        return format;
    }

    @Override
    public CIDSystemInfo getCIDSystemInfo() {
        return cidSystemInfo;
    }

    /**
     * {@inheritDoc}
     * <p>
     * The method returns null if there is there was an error opening the font.
     */
    @Override
    public FontBoxFont getFont() {
        return this.boxFont;
    }

    @Override
    public int getFamilyClass() {
        return sFamilyClass;
    }

    @Override
    public int getWeightClass() {
        return usWeightClass;
    }

    @Override
    public int getCodePageRange1() {
        return ulCodePageRange1;
    }

    @Override
    public int getCodePageRange2() {
        return ulCodePageRange2;
    }

    @Override
    public int getMacStyle() {
        return macStyle;
    }

    @Override
    public PDPanoseClassification getPanose() {
        return null;
    }

    @Override
    public String toString() {
        return super.toString() + " " + file;
    }
}
