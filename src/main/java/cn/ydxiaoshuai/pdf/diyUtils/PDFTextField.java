package cn.ydxiaoshuai.pdf.diyUtils;

import com.itextpdf.text.Rectangle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName PDFTextField
 * @description:
 * @author: 小帅丶
 * @create: 2024-11-19 15:23
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PDFTextField {
    //文本域位置
    private Rectangle rectangle;
    //文本域名称
    private String fieldName;
    //文本域所在页码
    private Integer pageNum;
}
