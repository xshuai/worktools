package cn.ydxiaoshuai.pdf.diyUtils;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName PDFSignData
 * @description: PDF签名数据
 * @author: 小帅丶
 * @create: 2024-11-19 14:05
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
public class PDFSignData {
    //签名要放在的页数
    private Integer pageNum;
    //签名图片地址
    private String imgUrl;
    //签名图片裁剪后的宽度
    private float fitWidth;
    //签名图片裁剪后的高度
    private float fitHeight;
    //签名图片绝对位置X坐标
    private float absoluteX;
    //签名图片绝对位置Y坐标
    private float absoluteY;
}
