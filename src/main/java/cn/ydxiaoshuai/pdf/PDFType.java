package cn.ydxiaoshuai.pdf;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2022-05-12 15:12.
 * Version 1.0
 */

public enum PDFType {
    INVOICE {
        int toPDFType() {
            return 1;
        }
    },
    TRAVEL_ITINERARY {
        int toPDFType() {
            return 2;
        }
    },
    GENERAL_INVOICE {
        int toPDFType() { return 3; }
    },
}
