package cn.ydxiaoshuai.pdf;


import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2022-05-12 19:04.
 * Version 1.0
 */

public class PDFMergerSample {

    public static void main(String[] args) throws Exception {
        String SAVE_PATH = "F:\\testfile\\testpdf\\";
        FileOutputStream fileOutputStream = new FileOutputStream(SAVE_PATH + "20220513183505.pdf");
        List<PDFParams> pdfParams = new ArrayList<>();
        PDFParams invoice = new PDFParams(SAVE_PATH + "invoice.pdf", PDFType.INVOICE);
        PDFParams travel = new PDFParams(SAVE_PATH + "xcd.pdf", PDFType.TRAVEL_ITINERARY);
        pdfParams.add(invoice);
        pdfParams.add(travel);
        ByteArrayOutputStream byteArrayOutputStream = PDFMergeUtil.mergeDiDiInvoiceAndTraveToOnePDF(pdfParams,
                true, true);
        fileOutputStream.write(byteArrayOutputStream.toByteArray());
    }
}
