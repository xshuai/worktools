package cn.ydxiaoshuai.pdf;

import cn.hutool.core.date.DateUtil;
import com.spire.pdf.FileFormat;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.exporting.PdfImageInfo;
import com.spire.pdf.graphics.PdfBitmap;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.rmi.ServerException;

/**
 * @ClassName PDFCompressUtil
 * @description: POF压缩 耗时较久
 * @author: 小帅丶
 * @create: 2025-01-23 14:49
 * @Version 1.0
 **/
@Slf4j
public class PDFCompressUtil {
    /**
     * @Description POF压缩
     * @param srcPath - 源PDF路径
     * @Author 小帅丶
     * @Date  2025/1/23 15:11:36
     * @return java.io.ByteArrayOutputStream
     **/
    public ByteArrayOutputStream compressPDF(String srcPath) throws Exception{
        return compressPDF(srcPath,20L);
    }
    /**
     * @Description POF压缩
     * @param srcPath - 源PDF路径
     * @param quality - 压缩质量值 0-100 越大质量越高 推荐 20
     * @Author 小帅丶
     * @Date  2025/1/23 15:11:36
     * @return java.io.ByteArrayOutputStream
     **/
    public ByteArrayOutputStream compressPDF(String srcPath,Long quality) throws Exception{
        long startTime = System.currentTimeMillis();
        log.info("[{}][compressPDF]======>参数:srcPath {} quality {}",this.getClass().getSimpleName(),srcPath,quality);
        log.info("[{}][compressPDF]======>开始时间:{}",this.getClass().getSimpleName(),DateUtil.now());
        //Load a sample PDF document
        PdfDocument document = new PdfDocument();
        document.loadFromFile(srcPath);
        int pageNum = document.getPages().getCount();
        if(pageNum > 10){
            log.info("[{}][compressPDF]======>页数超范围:{}",this.getClass().getSimpleName(),pageNum);
           throw new ServerException("PDF页数大于10,无法进行压缩");
        }else{
            if (quality <= 100L && quality >= 0L) {
                document.getFileInfo().setIncrementalUpdate(false);
                for (int i = 0; i < document.getPages().getCount(); i++) {
                    PdfPageBase page = document.getPages().get(i);
                    PdfImageInfo[] images = page.getImagesInfo();
                    if (images != null && images.length > 0)
                        for (int j = 0; j < images.length; j++) {
                            PdfImageInfo image = images[j];
                            PdfBitmap bp = new PdfBitmap(image.getImage());
                            bp.setQuality(quality);
                            page.replaceImage(j, bp);
                        }
                }
                ByteArrayOutputStream[] byteArrayOutputStreams = document.saveToStream(FileFormat.PDF);
                log.info("[{}][compressPDF]======>耗时:{}",this.getClass().getSimpleName(),(System.currentTimeMillis()-startTime));
                log.info("[{}][compressPDF]======>结束时间:{}",this.getClass().getSimpleName(),DateUtil.now());
                return byteArrayOutputStreams[0];
            } else {
                throw new ServerException("质量应在0到100的范围内");
            }
        }
    }
}
