package cn.ydxiaoshuai.pdf;

/**
 * Description 自定义字体枚举
 * ProjectName worktools
 * Created by 小帅丶 on 2022-12-21 08:59.
 * Version 1.0
 */

public enum  FontCustomerFamily {

    DENGXIAN,
    DENGXIAN_BOLD,
    SIMHEI,
    MSYH,
    MSYHL,
    MSYHBD
}
