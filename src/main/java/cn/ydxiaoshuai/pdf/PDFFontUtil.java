package cn.ydxiaoshuai.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import lombok.extern.slf4j.Slf4j;

/**
 * Description 字体设置
 * ProjectName worktools
 * Created by 小帅丶 on 2022-12-21 09:05.
 * Version 1.0
 */
@Slf4j
public class PDFFontUtil {
    public static final String OS_NAME_LINUX ="LINUX";//LINUX系统
    public static final String OS_NAME_WINDOWS ="WINDOWS";//WINDOWS系统
    //字体目录自行定义哦
    public static String FONT_PATH_LINUX_DENGXIAN = "/opt/datian/conf/Deng.ttf";
    public static String FONT_PATH_LINUX_DENGXIAN_BOLD = "/opt/datian/conf/Dengb.ttf";

    public static String FONT_PATH_WINDOWS_DENGXIAN = "C:\\Windows\\Fonts\\Deng.ttf";
    public static String FONT_PATH_WINDOWS_DENGXIAN_BOLD = "C:\\Windows\\Fonts\\Dengb.ttf";

    private static String getFontPath(boolean isBold) {
        String osName = System.getProperty("os.name").toLowerCase();
        if(osName.toUpperCase().contains(OS_NAME_LINUX)){
            if(isBold){
                return FONT_PATH_LINUX_DENGXIAN_BOLD;
            }else{
                return FONT_PATH_LINUX_DENGXIAN;
            }
        }else if(osName.toUpperCase().contains(OS_NAME_WINDOWS)){
            if(isBold){
                return FONT_PATH_WINDOWS_DENGXIAN_BOLD;
            }else{
                return FONT_PATH_WINDOWS_DENGXIAN;
            }
        }else {
            return FONT_PATH_LINUX_DENGXIAN;
        }
    }

    /**
     * @Description 指定颜色字体 默认处理中文显示
     * @Author 小帅丶
     * @Date  2019/7/12 14:05
     * @param color 字体颜色
     * @param fontSize 字体大小
     * @param fontFamily 字体
     * @return com.itextpdf.text.Font
     **/
    public static Font getColorFont(BaseColor color, float fontSize, FontCustomerFamily fontFamily) {
        Font font;
        if (fontFamily.equals(FontCustomerFamily.DENGXIAN_BOLD)) {
            font = new Font(getDengXianBoldFont());
        }else{
            font = new Font(getDengXianFont());
        }
        font.setColor(color);
        font.setSize(fontSize);
        return font;
    }

    /**
     * @Description 设置中文指定字体支持
     * @Author 小帅丶
     * @Date  2019/7/11 10:33
     * @return com.itextpdf.text.pdf.BaseFont
     **/
    public static BaseFont getDengXianBoldFont() {
        BaseFont bf = null;
        try {
            bf = BaseFont.createFont(getFontPath(true),BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            log.info("getDengXianBoldFont error ======>{}",e.getMessage());
        }
        return bf;
    }

    /**
     * @Description 设置中文支持
     * @Author 小帅丶
     * @Date  2019/7/11 10:33
     * @return com.itextpdf.text.pdf.BaseFont
     **/
    public static BaseFont getFont() {
        BaseFont bf = null;
        try {
            bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            log.info("getFont error ======>{}",e.getMessage());
        }
        return bf;
    }

    /**
     * @Description 设置中文指定字体支持
     * @param fontPath - 字体路径
     * @Author 小帅丶
     * @Date  2019/7/11 10:33
     * @return com.itextpdf.text.pdf.BaseFont
     **/
    public static BaseFont getFont(String fontPath) {
        BaseFont bf = null;
        try {
            bf = BaseFont.createFont(fontPath,BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            log.info("getFont error ======>{}",e.getMessage());
        }
        return bf;
    }

    /**
     * @Description 设置中文指定字体支持
     * @Author 小帅丶  Deng.ttf
     * @Date  2019/7/11 10:33
     * @return com.itextpdf.text.pdf.BaseFont
     **/
    public static BaseFont getDengXianFont() {
        BaseFont bf = null;
        try {
            bf = BaseFont.createFont(getFontPath(false),BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            log.info("getDengXianFont error ======>{}",e.getMessage());
        }
        return bf;
    }
}
