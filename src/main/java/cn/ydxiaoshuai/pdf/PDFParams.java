package cn.ydxiaoshuai.pdf;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description PDF合并参数对象
 * ProjectName worktools
 * Created by 小帅丶 on 2022-05-12 15:15.
 * Version 1.0
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PDFParams {
    private String filePath;
    private PDFType pdfType;
}
