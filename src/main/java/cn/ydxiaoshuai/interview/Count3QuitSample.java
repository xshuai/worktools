package cn.ydxiaoshuai.interview;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 小帅丶
 * @className Count3QuitSample
 * @Description 遇三重新计数
 * 有100只羊围成一圈，然后从中任意一只开始数1，2，3，
 * 到第三只的时候，牵出来，从下一只开始从头数，以此往复，
 * 直到只剩下一只为止。然后按照牵出顺序进行打印。
 * @Date 2021-06-09-16:53
 **/
public class Count3QuitSample {
    public static void main(String[] args) {
        Count3Quit(1);
    }

    /**
     * @Description 遇三重新计数
     * @param startPosition - 开始的位置
     * @Author 小帅丶
     * @Date 2021-06-09 17:14
     * @return void
     **/
    public static void Count3Quit(Integer startPosition) {
        //List存100只羊
        List<Integer> sheepList = new ArrayList<>();
        List<Integer> sheepQuitList = new ArrayList<>();
        for (Integer i = 1; i < 101; i++) {
            sheepList.add(i);
        }
        //看一下所有元素
        System.out.println("全部元素"+sheepList);
        /**********开始遇3重新计数**********/
        //从第X个开始
        int start = startPosition;
        //第X个索引为减1
        int index = startPosition - 1;
        while (sheepList.size() != 1) {
            //到第三个就移除元素
            if (start % 3 == 0) {
                sheepQuitList.add(sheepList.get(index));
                System.out.println("每次移除元素"+sheepQuitList);
                sheepList.remove(index);
                //删除元素后 index索引也需要减1
                --index;
            }
            start++;
            //每当index指向了最后一只羊，就重新开始
            if (index == sheepList.size() - 1) {
                index = 0;
                //每数完一次就打印剩余的羊 可以知道每一次拿出来的是哪些羊
                System.out.println("每次操作后元素"+sheepList);
            } else {
                index++;
            }
        }
        //看一下操作完的元素
        System.out.println("最终元素"+sheepList);
        System.out.println("最终移除元素"+sheepQuitList);
    }
}
