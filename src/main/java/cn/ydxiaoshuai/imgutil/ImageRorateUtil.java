package cn.ydxiaoshuai.imgutil;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifDirectoryBase;

import java.io.File;

/**
 * Description 获取图片倾斜角度
 * ProjectName worktools
 * Created by 小帅丶 on 2023-07-25 14:47.
 * Version 1.0
 */

public class ImageRorateUtil {

    /**
     * @Description 获取图片倾斜角度
     * @param file - 文件
     * @Author 小帅丶
     * @Date  2023/7/25 14:49
     * @return void
     **/
    public static void getImgRotateAngle(File file) {
        try {
            //检查是否有旋转角度导致ImageIO获取宽高相反
            Metadata metadata = ImageMetadataReader.readMetadata(file);
            StringBuilder description = new StringBuilder();
            metadata.getDirectories().forEach(directory -> {
                directory.getTags().forEach(tag -> {
                    if (tag.getTagType() == ExifDirectoryBase.TAG_ORIENTATION) {
                        description.append(tag.getDescription().replaceAll(" ", ""));
                    }
                });
            });
            if (description.length() > 0) {
                int rotateIndex = description.indexOf("Rotate");
                int cwIndex = description.indexOf("CW");
                System.out.println("旋转角度 = " + rotateIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
