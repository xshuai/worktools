package cn.ydxiaoshuai.render;

import org.bytedeco.javacv.*;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @ClassName RenderUtil
 * @description:
 * @author: 小帅丶
 * @create: 2024-08-12 13:56
 * @Version 1.0
 **/
public class RenderUtil {
    /**
     * @Description 图片合并为视频文件
     * @param outputFile 输出文件路径
     * @param imgWidth 图片宽度
     * @param imgHeight 图片高度
     * @param imagePath 图片路径
     * @param outputFileFormat 输出文件格式
     * @param fps 视频帧率
     * @Author 小帅丶
     * @Date  2024/8/12 16:12:38
     * @return void
     **/
    public void createVideoFromImages(String outputFile,
                                      Integer imgWidth,
                                      Integer imgHeight,
                                      String imagePath,
                                      String outputFileFormat,
                                      int fps) {
        File[] files = new File(imagePath).listFiles();
        // 确保图片按名称排序
        if (files != null) {
            java.util.Arrays.sort(files);
        }
        try (FrameRecorder recorder = new FFmpegFrameRecorder(outputFile, imgWidth, imgHeight)) {
            // 使用H264编解码器
            recorder.setVideoCodec(org.bytedeco.ffmpeg.global.avcodec.AV_CODEC_ID_H264);
            //使用AAC音频编解码器
            recorder.setAudioCodec(org.bytedeco.ffmpeg.global.avcodec.AV_CODEC_ID_AAC);
            // 设置视频格式
            recorder.setFormat(outputFileFormat.toLowerCase());
            recorder.setFrameRate(fps);
            recorder.start();
            for (File file : files) {
                if (file.getName().endsWith("."+outputFileFormat.toLowerCase())) {
                    continue;
                }
                // 从图片创建一个Frame对象
                BufferedImage bufferedImage = ImageIO.read(file);
                Java2DFrameConverter java2DFrameConverter = new Java2DFrameConverter();
                Frame frame = java2DFrameConverter.convert(bufferedImage);
                // 将Frame写入视频
                recorder.record(frame);
            }
            recorder.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
