package cn.ydxiaoshuai.word;


import cn.hutool.core.util.ObjectUtil;
import com.spire.doc.*;
import com.spire.doc.collections.RowCollection;
import com.spire.doc.documents.WatermarkLayout;

import java.awt.*;
import java.util.StringJoiner;

/**
 * @author 小帅丶
 * @Description word读取工具类
 * @date 2023/9/20  10:23:21
 * https://repo.e-iceblue.cn/#browse/search
 */
public class WordTableUtil {
    /**
     * @Description 读取word表格并输出内容
     * @param filePath - 文件路径
     * @Author 小帅丶
     * @Date  2023/9/22 18:47:24
     * @return void
     **/
    public static void getWorkTableContent(String filePath){
        //打开文档
        Document document = new Document(filePath);
        //获取段落
        int sectionCount = document.getSections().getCount();
        System.err.println("sectionCount = " + sectionCount);
        for (int t = 0; t < sectionCount; t++) {
            Section section = document.getSections().get(t);
            //获取第一个表格
            int tableCount = section.getTables().getCount();
            System.err.println("tableCount = " + tableCount);
            if(tableCount>0){
                for (int tt = 0; tt < tableCount; tt++) {
                    Table table = section.getTables().get(tt);
                    if(ObjectUtil.isNotEmpty(table)){
                        RowCollection rows = table.getRows();
                        int rowsCount = rows.getCount();
                        //每行
                        for (int i = 0; i < rowsCount; i++) {
                            TableRow tableRow = rows.get(i);
                            //第几列
                            StringJoiner tableRowContent = new StringJoiner("\t");
                            for (int j = 0; j < tableRow.getCells().getCount(); j++) {
                                TableCell tableCell = tableRow.getCells().get(j);
                                int cellCont = tableCell.getParagraphs().getCount();
                                if(cellCont>0){
                                    for (int cc = 0; cc < cellCont; cc++) {
                                        String text = tableCell.getParagraphs().get(cc).getText();
                                        tableRowContent.add(text);
                                    }
                                }
                            }
                            System.out.println(tableRowContent.toString());
                        }
                    }
                }
            }
        }
    }
    /**
     * @Description word增加文本水印
     * @Author 小帅丶
     * @Date  2023/9/22 16:57:10
     * @return void
     **/
    public static void insertTextWatermark(String filePath,String saveFilePath) {
        //打开文档
        Document document = new Document(filePath);
        //获取段落
        Section section = document.getSections().get(0);
        //插入文本水印
        TextWatermark txtWatermark = new TextWatermark();
        txtWatermark.setText("小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶小帅丶");
        txtWatermark.setFontSize(25);
        txtWatermark.setSemitransparent(true);
        txtWatermark.setColor(new Color(96,96,96));
        txtWatermark.setLayout(WatermarkLayout.Diagonal);
        section.getDocument().setWatermark(txtWatermark);
        //保存文档
        document.saveToFile(saveFilePath, FileFormat.Docx);
    }
}
