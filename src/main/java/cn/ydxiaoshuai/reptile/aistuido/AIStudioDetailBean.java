package cn.ydxiaoshuai.reptile.aistuido;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2023-01-03 10:57.
 * Version 1.0
 */

@NoArgsConstructor
@Data
public class AIStudioDetailBean {


    private String logId;
    private int errorCode;
    private String errorMsg;
    private int timestamp;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        //战力值
        private int exp;
        //排名
        private int rank;
        //等级
        private int level;
        private int expLack;
        private int badgeCount;
        private int projectNumUpperLimit;
        //点亮徽章
        private List<BadgeListBean> badgeList;

        @NoArgsConstructor
        @Data
        public static class BadgeListBean {
            private int id;
            //徽章名称
            private String badgeName;
            private String badgeDesc;
            //点亮的图片
            private String sampleUrl;
            //未点亮的
            private String imgUrl;
            private int showType;
            private boolean isGain;
        }
    }
}
