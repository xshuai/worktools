package cn.ydxiaoshuai.reptile.aistuido;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2023-01-03 11:10.
 * Version 1.0
 */

public class AIStudioConstant {

    public static class BadgesGain {

        public static String XLKR = "https://ai.bdstatic.com/file/89FF97A26F7443A9B305B7F5BDD5B666";
        public static String XMDR = "https://ai.bdstatic.com/file/55F2B2B29D124700AF920A032066AB70";
        public static String SJJZJ = "https://ai.bdstatic.com/file/FA23DC7130EC44D3A7D961E4D382FB71";
        public static String AIXXXX = "https://ai.bdstatic.com/file/674EDA59C724491F8B5D99ECD3FE17DA";
        public static String PLJ = "https://ai.bdstatic.com/file/838619634694414F87E4B64AC5490540";
        public static String XHR = "https://ai.bdstatic.com/file/E101ECEC47794A04973C159E52EAB854";
        public static String SJXNS = "https://ai-studio-static-online.bj.bcebos.com/badges/073l.png";
        public static String AIXXDR = "https://ai.bdstatic.com/file/99D4A9CEFDE74693AAB310CFCB57168F";
        public static String SDXXCJRZ = "https://ai-studio-static-online.bj.bcebos.com/certify-exam/badge/012.png";
        public static String SDXXZJRZ = "https://ai-studio-static-online.bj.bcebos.com/certify-exam/badge/022.png";
        public static String BSXX = "https://ai.bdstatic.com/file/87EB689DB4414D508DA44F3CD4FA2C28";
        public static String BSDR = "https://ai.bdstatic.com/file/B7B2ABF7F2634D22B8BE3314BBBAC081";
        public static String PPDE = "https://ai.bdstatic.com/file/EB52BCA357374F4D886CE3FE1FFB8776";
        public static String CJJPXMZZ = "https://ai-studio-static-online.bj.bcebos.com/master_page/public/png/quality_junior.png";
    }

    public static Integer SUCCESS_CODE = 0;
    public static Integer WIDTH_HEIGHT = 110;
    public static Integer WIDTH_HEIGHT_60 = 60;
    public static Integer WIDTH_HEIGHT_80 = 80;
    public static Integer WIDTH_HEIGHT_40 = 40;

    public static String FONT_FAMILY_HWXK = "华文行楷";
    public static String FONT_FAMILY_HEITI = "黑体";
}
