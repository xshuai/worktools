package cn.ydxiaoshuai.reptile.aistuido;

import lombok.Data;

import java.util.List;

/**
 * Description
 * ProjectName worktools
 * Created by 小帅丶 on 2023-01-03 14:55.
 * Version 1.0
 */
@Data
public class AIStudioYearReportBean {
    //头像地址
    private String portrait;
    //昵称
    private String nickname;
    //加入日期
    private String joinDate;
    //数据日期
    private String dataDate;
    //加入天数
    private Integer joinDays;
    //关注
    private Integer followeeCount;
    //粉丝
    private Integer followerCount;
    //战力值
    private Integer exp;
    //排名
    private Integer rank;
    //等级
    private Integer level;
    //点亮徽章
    private List<AIStudioDetailBean.ResultBean.BadgeListBean> badgeList;
    //关注者里面战力最高的前5个
    private List<AIStudioInfoBean.ResultBean.UserListBean> userList;
    //项目浏览总次数
    private int viewCount;
    //项目fork总次数
    private int forkCount;
    //参加课程总次数
    private int classCount;
    //参加比赛总次数
    private int matchCount;
    //数据集引用总次数
    private int datasetCount;
    //Fork最多
    private AIStudioStatisticsBean.ResultBean.ProjectBean project;
    //引用最多
    private AIStudioStatisticsBean.ResultBean.DatasetBean dataset;
    //最近学习
    private AIStudioStatisticsBean.ResultBean.MatchBean match;
    //最近参加
    private AIStudioStatisticsBean.ResultBean.CourseBean course;
}
