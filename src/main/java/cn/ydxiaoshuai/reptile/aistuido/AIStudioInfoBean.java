package cn.ydxiaoshuai.reptile.aistuido;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Description 基础信息
 * ProjectName worktools
 * Created by 小帅丶 on 2023-01-03 14:37.
 * Version 1.0
 */

@NoArgsConstructor
@Data
public class AIStudioInfoBean {

    private String logId;
    private int errorCode;
    private String errorMsg;
    private int timestamp;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private String abs;
        //头像地址
        private String portrait;
        //昵称
        private String nickname;
        private String userLocationCity;
        private String userLocationProvince;
        //加入AIStudio的天数
        private int joinDays;
        private boolean followExpValid;
        private boolean followedExpValid;
        //关注
        private int followeeCount;
        //粉丝
        private int followerCount;
        private int followed;
        private int projectForkCount;
        private int datasetUseCount;
        private int totalCollectCount;
        private int totalUserPoints;
        private List<Integer> centerUserType;
        //关注者里面战力最高的前5个
        private List<UserListBean> userList;

        @NoArgsConstructor
        @Data
        public static class UserListBean {

            private int userId;
            private String portrait;
            private String userName;
            private String nickname;
            private int level;
            private int exp;
        }
    }
}
