package cn.ydxiaoshuai.reptile.aistuido;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Description 荣誉统计Bean
 * ProjectName worktools
 * Created by 小帅丶 on 2023-01-03 10:26.
 * Version 1.0
 */

@NoArgsConstructor
@Data
public class AIStudioStatisticsBean {

    private String logId;
    private int errorCode;
    private String errorMsg;
    private int timestamp;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        //项目浏览总次数
        private int viewCount;
        //项目fork总次数
        private int forkCount;
        //参加课程总次数
        private int classCount;
        //参加比赛总次数
        private int matchCount;
        //数据集引用总次数
        private int datasetCount;
        //Fork最多
        private ProjectBean project;
        //引用最多
        private DatasetBean dataset;
        //最近学习
        private MatchBean match;
        //最近参加
        private CourseBean course;

        @NoArgsConstructor
        @Data
        public static class ProjectBean {
            private int projectId;
            //项目名称
            private String projectName;
            private String projectAbs;
            private int projectType;
            private int projectEnvironment;
            private int projectFramework;
            private Object projectEnvText;
            private Object projectFraText;
            private String pythonVersion;
            private String paddleVersion;
            private int userId;
            private String createTime;
            private String updateTime;
            private long createTimeStamp;
            private boolean fork;
            private boolean collect;
            private Object projectFileId;
            private int statusCode;
            //总fork次数
            private int forkCount;
            private int collectCount;
            private int commentCount;
            private int projectWeight;
            private int resourceAlloc;
            private String userName;
            private String nickname;
            private int templateId;
            private boolean running;
            private int deviceType;
            private Object orgGroupName;
            private int graphRunningSubmitId;
            private Object portrait;
            private Object versionId;
            private int viewCount;
            private int channel;
            private String keywords;
            private boolean selected;
            private boolean sharedModelServing;
            private Object rankDate;
            private int baseInfoScore;
            private int qualityScore;
            private int userGradeScore;
            private int publicTimeScore;
            private int interventionScore;
            private Object esSort;
            private Object topicTags;
            private int initStatus;
            private int projectState;
            private Object gjsParentId;
            private Object gjsToken;
            private Object groupId;
            private int openCourseMapping;
            private Object isParticipant;
            private Object groupOnline;
            private Object noteBook;
            private Object level;
            private Object badgeCount;
            private Object spaId;
            private Object source;
            private int ideType;
            private boolean selfProject;
            private List<?> projectDataset;
            private List<Integer> tags;
        }

        @NoArgsConstructor
        @Data
        public static class DatasetBean {

            private int datasetId;
            //数据集名称
            private String datasetName;
            private String datasetAbs;
            private String createTime;
            private int datasetType;
            private int userId;
            private Object datasetContent;
            private boolean collect;
            private int statusCode;
            //引用次数
            private int useCount;
            private int collectCount;
            private int commentCount;
            private int datasetWeight;
            private String updateTime;
            private int selectType;
            private int protocolId;
            private String userName;
            private String nickname;
            private String portrait;
            private int viewCount;
            private Object level;
            private Object badgeCount;
            private int downloadCount;
            private int rankDate;
            private int matchId;
            private String authorName;
            private Object badgeList;
            private boolean selfDataset;
            private List<?> fileList;
            private List<?> fileIds;
            private List<?> fileAbsList;
            private List<?> topicTags;
            private List<?> tags;
        }

        @NoArgsConstructor
        @Data
        public static class MatchBean {

            private int id;
            //比赛名称
            private String matchName;
            private String matchAbs;
            private int statusCode;
            private String createTime;
            private String updateTime;
            private String matchKey;
            private int processId;
            private int projectId;
            private String tags;
            private String reward;
            private int signupCount;
            private String startTime;
            private String endTime;
            private int processCode;
            private String processText;
            private String logo;
            private String sponsorLogo;
            private String forumUrl;
            private String processName;
            private int tokenObtain;
            private String tokenReason;
            private int routine;
            private int userSignup;
            private int teamUserLimit;
            private int signupType;
            private String signupLink;
            private String btnSignupText;
            private int btnSignupStatus;
            private String btnJoinText;
            private int btnJoinStatus;
            private String btnInviteText;
            private int btnInviteStatus;
            private String submitText1;
            private String btnSubmitText1;
            private String submitText2;
            private String btnSubmitText2;
            private String enSubmitText1;
            private String enBtnSubmitText1;
            private String enSubmitText2;
            private String enBtnSubmitText2;
            private String previewCode;
            private List<?> matchContent;
            private List<?> processList;
        }

        @NoArgsConstructor
        @Data
        public static class CourseBean {

            private int id;
            private int groupId;
            private int orgId;
            private int userId;
            private int memberId;
            private int groupType;
            private int joinType;
            private String groupName;
            private int groupCapacity;
            private int groupSize;
            private long beginDate;
            private long endDate;
            private String groupAbs;
            private String groupPortrait;
            private String orgName;
            private int statusCode;
            private int state;
            private long applyTime;
            private int publicState;
            private long onlineTime;
            private String qrCode;
            private String fee;
            private int free;
            private String rejectReason;
            private int copyState;
            private int saveType;
            private String groupKeywords;
        }
    }
}
