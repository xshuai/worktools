package cn.ydxiaoshuai.reptile.aistuido;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;

/**
 * Description 年度报告生成工具类
 * ProjectName worktools
 * Created by 小帅丶 on 2023-01-03 10:15.
 * Version 1.0
 */
@Slf4j
public class AIStudioYearReport {
    //基础信息
    private static String BASE_INFO_URL = "https://aistudio.baidu.com/studio/user/center/public/info";
    //荣誉统计
    private static String GLORY_STATISTICS_URL = "https://aistudio.baidu.com/studio/glory/statistics";
    //战力、排行榜计算
    private static String GLORY_DETAIL_URL = "https://aistudio.baidu.com/studio/glory/detail";
    //aistudio 头部参数
    private static String X_STUDIO_TOKEN = "x-studio-token";
    //海报图
    private static String POSTER_PATH = "";

    /**
     * @Author 小帅丶
     * @Description 年度报告生成工具类
     * @Date  2023/1/4 14:47
     * @param bean - 数据
     * @param outputPath - 生成后的图片保存路径
     * @return void
     **/
    public static void generalYearReportPoster(AIStudioYearReportBean bean,String outputPath) throws Exception {
        long startTime = System.currentTimeMillis();
        if(null==POSTER_PATH||"".equals(POSTER_PATH)){
            throw new Exception("请先设置海报图所在路径");
        }
        //填充后的图片
        BufferedImage image = ImageIO.read(new File(POSTER_PATH));
        String portrait = bean.getPortrait();
        if(StrUtil.isEmpty(portrait)){
            BufferedImage avatar = AIStudioUtil.cutHeadImages(portrait);
            //头像填充
            image = AIStudioUtil.mergePendant(image, avatar, 75, 485, 1);
        }
        //昵称填充 转成2d画板 对图片进行文本内容填充
        Graphics2D g2D = (Graphics2D) image.getGraphics();
        g2D = fillNickNameAndJoinDays(g2D, bean);
        //战力 排行填充
        g2D = fillExpRank(g2D, bean);
        //总访问 最多fork等填充
        g2D = fillViewFork(g2D, bean);
        //粉丝 关注 被关注填充
        g2D = fillFans(g2D, bean);
        //粉丝 大佬
        Integer fansTopSix[] = new Integer[]{220, 950};
        g2D = AIStudioUtil.fillTextContent(g2D, "你关注 Top6 飞桨开发者",
                AIStudioConstant.FONT_FAMILY_HWXK,
                40,
                fansTopSix,
                false,
                0);
        int size = bean.getUserList().size();
        if(size>0){
            for (int i = size-1; i>=0; i--) {
                String portraitFans = bean.getUserList().get(i).getPortrait();
                if(StrUtil.isNotEmpty(portraitFans)){
                    BufferedImage fansLevel = AIStudioUtil.cutHeadImages(portraitFans,
                            AIStudioConstant.WIDTH_HEIGHT_80);
                    int x = 515-(((size-1)-i)*65);
                    image = AIStudioUtil.mergePendant(image,fansLevel, x, 980, 1);
                }else{
                    continue;
                }
            }
        }
        //数据集引用 参加课程 参加比赛 1100
        g2D = fillDatasetMatchCourse(g2D, bean);
        //Fork最多的项目
        g2D = fillForkProject(g2D,bean);
        //徽章
        Integer badgeText[] = new Integer[]{220, 1300};
        g2D = AIStudioUtil.fillTextContent(g2D, "已获得 成就 身份 徽章",
                AIStudioConstant.FONT_FAMILY_HWXK,
                40,
                badgeText,
                false,
                0);
        for (int i = 0; i < bean.getBadgeList().size(); i++) {
            BufferedImage badgeImage = AIStudioUtil.cutHeadImages(bean.getBadgeList().get(i).getSampleUrl(),
                    AIStudioConstant.WIDTH_HEIGHT_80);
            if(i>6){
                int x = (i-6)*100==100?50:((i)*100)+50;
                image = AIStudioUtil.mergePendant(image,badgeImage, x, 1420, 1);
                Integer badgeNameCoord[] = new Integer[]{x+10, 1520};
                g2D = AIStudioUtil.fillTextContent(g2D, bean.getBadgeList().get(i).getBadgeName(),
                        AIStudioConstant.FONT_FAMILY_HWXK,
                        15,
                        badgeNameCoord,
                        false,
                        0);
            }else{
                int x = (i+1)*100==100?50:((i)*100)+50;
                image = AIStudioUtil.mergePendant(image,badgeImage, x, 1320, 1);
                Integer badgeNameCoord[] = new Integer[]{x+10, 1420};
                g2D = AIStudioUtil.fillTextContent(g2D, bean.getBadgeList().get(i).getBadgeName(),
                        AIStudioConstant.FONT_FAMILY_HWXK,
                        15,
                        badgeNameCoord,
                        false,
                        0);
            }
        }
        Integer projectForkA[] = new Integer[]{300, 1820};
        g2D = AIStudioUtil.fillTextContent(g2D, "以上数据统计日期为 " +bean.getDataDate(),
                AIStudioConstant.FONT_FAMILY_HEITI,
                20,
                projectForkA,
                false,
                0);
        ImageIO.write(image, "png", new File(outputPath));
        System.out.println("poster总耗时======>" + (System.currentTimeMillis() - startTime));
    }

    private static Graphics2D fillForkProject(Graphics2D g2D, AIStudioYearReportBean bean) throws Exception{
        Integer projectForkA[] = new Integer[]{290, 1190};
        g2D = AIStudioUtil.fillTextContent(g2D, "Fork最多的项目",
                AIStudioConstant.FONT_FAMILY_HWXK,
                40,
                projectForkA,
                false,
                0);
        Integer projectForkB[] = new Integer[]{20, 1230};
        g2D = AIStudioUtil.fillTextContent(g2D, bean.getProject().getProjectName(),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                25,
                projectForkB,
                true,
                30);
        return g2D;
    }

    /**
     * @Description 昵称填充
     * @param g2D  - g2d对象
     * @param bean - 数据
     * @return java.awt.Graphics2D
     * @Author 小帅丶
     * @Date 2023/1/3 18:52
     **/
    private static Graphics2D fillNickNameAndJoinDays(Graphics2D g2D, AIStudioYearReportBean bean) throws Exception {
        Integer nickNameCoords[] = new Integer[]{200, 550};
        g2D = AIStudioUtil.fillTextContent(g2D, bean.getNickname() + "  的年度盘点~",
                AIStudioConstant.FONT_FAMILY_HWXK,
                40,
                nickNameCoords,
                false,
                0);
        //加入AIStudio的天数填充
        Integer joinDaysCoordsDate[] = new Integer[]{100, 650};
        g2D = AIStudioUtil.fillTextContent(g2D, bean.getJoinDate(),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                35,
                joinDaysCoordsDate,
                false,
                0);
        Integer joinDaysCoordsA[] = new Integer[]{320, 650};
        g2D = AIStudioUtil.fillTextContent(g2D, " 那天一定很特别,你加入了AI Studio",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                joinDaysCoordsA,
                false,
                0);
        Integer joinDaysCoordsB[] = new Integer[]{100, 710};
        g2D = AIStudioUtil.fillTextContent(g2D, "截止今日已加入AI Studio ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                joinDaysCoordsB,
                false,
                0);
        Integer joinDaysCoordsC[] = new Integer[]{430, 710};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getJoinDays()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                joinDaysCoordsC,
                false,
                0);
        Integer joinDaysCoordsD[] = new Integer[]{490, 710};
        g2D = AIStudioUtil.fillTextContent(g2D, " 天",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                joinDaysCoordsD,
                false,
                0);
        return g2D;
    }

    /**
     * @Description 战力排行填充
     * @param g2D  - g2d对象
     * @param bean - 数据
     * @return java.awt.Graphics2D
     * @Author 小帅丶
     * @Date 2023/1/3 18:52
     **/
    private static Graphics2D fillExpRank(Graphics2D g2D, AIStudioYearReportBean bean) throws Exception {
        Integer expCoordsA[] = new Integer[]{100, 770};
        g2D = AIStudioUtil.fillTextContent(g2D, "当前战力值 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                expCoordsA,
                false,
                0);
        Integer expCoordsB[] = new Integer[]{260, 770};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getExp()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                expCoordsB,
                false,
                0);
        Integer expCoordsC[] = new Integer[]{350, 770};
        g2D = AIStudioUtil.fillTextContent(g2D, "排名 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                expCoordsC,
                false,
                0);
        Integer expCoordsD[] = new Integer[]{420, 770};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getRank()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                expCoordsD,
                false,
                0);
        return g2D;
    }

    /**
     * @Description 访问 fork填充
     * @param g2D  - g2d对象
     * @param bean - 数据
     * @return java.awt.Graphics2D
     * @Author 小帅丶
     * @Date 2023/1/3 18:52
     **/
    private static Graphics2D fillViewFork(Graphics2D g2D, AIStudioYearReportBean bean) throws Exception {
        Integer expCoordsA[] = new Integer[]{100, 830};
        g2D = AIStudioUtil.fillTextContent(g2D, "项目总访问量 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                expCoordsA,
                false,
                0);
        Integer expCoordsB[] = new Integer[]{290, 830};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getViewCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                expCoordsB,
                false,
                0);
        Integer expCoordsC[] = new Integer[]{400, 830};
        g2D = AIStudioUtil.fillTextContent(g2D, "总fork数 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                expCoordsC,
                false,
                0);
        Integer expCoordsD[] = new Integer[]{520, 830};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getForkCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                expCoordsD,
                false,
                0);
        return g2D;
    }

    /**
     * @Description 粉丝填充
     * @param g2D  - g2d对象
     * @param bean - 数据
     * @return java.awt.Graphics2D
     * @Author 小帅丶
     * @Date 2023/1/3 18:52
     **/
    private static Graphics2D fillFans(Graphics2D g2D, AIStudioYearReportBean bean) throws Exception {
        Integer fansA[] = new Integer[]{100, 890};
        g2D = AIStudioUtil.fillTextContent(g2D, "你关注了 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                fansA,
                false,
                0);
        Integer fansB[] = new Integer[]{230, 890};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getFolloweeCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                fansB,
                false,
                0);
        Integer fansC[] = new Integer[]{280, 890};
        g2D = AIStudioUtil.fillTextContent(g2D, "你有粉丝 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                fansC,
                false,
                0);
        Integer fansD[] = new Integer[]{410, 890};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getFollowerCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                fansD,
                false,
                0);
        return g2D;
    }



    private static Graphics2D fillDatasetMatchCourse(Graphics2D g2D, AIStudioYearReportBean bean) throws Exception{
        Integer dmcA[] = new Integer[]{100, 1120};
        g2D = AIStudioUtil.fillTextContent(g2D, "数据集引用 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                dmcA,
                false,
                0);
        Integer dmcB[] = new Integer[]{260, 1120};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getDatasetCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                dmcB,
                false,
                0);
        Integer dmcC[] = new Integer[]{340, 1120};
        g2D = AIStudioUtil.fillTextContent(g2D, "参加课程 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                dmcC,
                false,
                0);
        Integer dmcD[] = new Integer[]{480, 1120};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getClassCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                dmcD,
                false,
                0);
        Integer dmcE[] = new Integer[]{560, 1120};
        g2D = AIStudioUtil.fillTextContent(g2D, "参加比赛 ",
                AIStudioConstant.FONT_FAMILY_HWXK,
                30,
                dmcE,
                false,
                0);
        Integer dmcF[] = new Integer[]{700, 1120};
        g2D = AIStudioUtil.fillTextContent(g2D, String.valueOf(bean.getMatchCount()),
                AIStudioConstant.FONT_FAMILY_HWXK,
                new Color(41, 50, 225),
                40,
                dmcF,
                false,
                0);
        return g2D;
    }
    /**
     * @param cookie       - 请求头数据
     * @param xStudioToken - studio额外的token
     * @return void
     * @Description 生成飞桨年度海报
     * @Author 小帅丶
     * @Date 2023/1/3 13:58
     **/
    public static void generalYearReportPoster(String cookie, String xStudioToken, String userId) {
        long startTime = System.currentTimeMillis();
        AIStudioYearReportBean finalBean = new AIStudioYearReportBean();
        String baseResult = HttpRequest.post(BASE_INFO_URL)
                .cookie(cookie)
                .header(X_STUDIO_TOKEN, xStudioToken)
                .form("userId", userId)
                .execute().body();
        AIStudioInfoBean infoBean = JSON.parseObject(baseResult, AIStudioInfoBean.class);
        if (AIStudioConstant.SUCCESS_CODE.equals(infoBean.getErrorCode())) {
            finalBean.setNickname(infoBean.getResult().getNickname());
            finalBean.setPortrait(infoBean.getResult().getPortrait());
            finalBean.setJoinDays(infoBean.getResult().getJoinDays());
            String joinDate = DateUtil.format(DateUtil.offsetDay(new Date(), (~(finalBean.getJoinDays() - 1))),
                    DatePattern.CHINESE_DATE_PATTERN);
            finalBean.setJoinDate(joinDate);
            finalBean.setFolloweeCount(infoBean.getResult().getFolloweeCount());
            finalBean.setFollowerCount(infoBean.getResult().getFollowerCount());
            finalBean.setUserList(infoBean.getResult().getUserList());
        }
        String statisticsResult = HttpRequest.post(GLORY_STATISTICS_URL)
                .cookie(cookie)
                .header(X_STUDIO_TOKEN, xStudioToken)
                .execute().body();
        AIStudioStatisticsBean statisticsBean = JSON.parseObject(statisticsResult, AIStudioStatisticsBean.class);
        if (AIStudioConstant.SUCCESS_CODE.equals(statisticsBean.getErrorCode())) {
            finalBean.setViewCount(statisticsBean.getResult().getViewCount());
            finalBean.setForkCount(statisticsBean.getResult().getForkCount());
            finalBean.setClassCount(statisticsBean.getResult().getClassCount());
            finalBean.setMatchCount(statisticsBean.getResult().getMatchCount());
            finalBean.setDatasetCount(statisticsBean.getResult().getDatasetCount());
            finalBean.setProject(statisticsBean.getResult().getProject());
            finalBean.setDataset(statisticsBean.getResult().getDataset());
            finalBean.setMatch(statisticsBean.getResult().getMatch());
            finalBean.setCourse(statisticsBean.getResult().getCourse());
        }
        String detailResult = HttpRequest.post(GLORY_DETAIL_URL)
                .cookie(cookie)
                .header(X_STUDIO_TOKEN, xStudioToken)
                .execute().body();
        AIStudioDetailBean detailBean = JSON.parseObject(detailResult, AIStudioDetailBean.class);
        if (AIStudioConstant.SUCCESS_CODE.equals(detailBean.getErrorCode())) {
            finalBean.setExp(detailBean.getResult().getExp());
            finalBean.setRank(detailBean.getResult().getRank());
            finalBean.setLevel(detailBean.getResult().getLevel());
            finalBean.setBadgeList(detailBean.getResult().getBadgeList());
        }
        log.info("总耗时======>{}", (System.currentTimeMillis() - startTime));
        System.out.println(JSON.toJSONString(finalBean));
    }
}
