package cn.ydxiaoshuai.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @author 小帅丶
 * @className ParamDealUtil
 * @Description 获取KV形式的参数
 * @Date 2021-07-23-9:54
 **/
public class ParamDealUtil {
    /**
     * 获取KV形式的参数-JAVA版本
     *
     * @param params 请求参数集，所有参数必须已转换为字符串类型
     * @return String
     * @throws IOException
     */
    public static String getParamStr(Map<String, Object> params) throws IOException {
        // 遍历排序后的字典，将所有参数按"key=value"格式拼接在一起
        StringBuilder baseString = new StringBuilder();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            //空值就不提交请求了
            if (param.getValue() != null && !"".equals(param.getKey().trim())) {
                baseString.append(param.getKey().trim())
                        .append("=")
                        .append(URLEncoder.encode(param.getValue().toString().trim(), "UTF-8"))
                        .append("&");
            }
        }
        if (baseString.length() > 0) {
            baseString.deleteCharAt(baseString.length() - 1);
        }
        return baseString.toString();
    }

}
