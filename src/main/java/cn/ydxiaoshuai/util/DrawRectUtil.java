package cn.ydxiaoshuai.util;


import cn.hutool.core.codec.Base64;
import cn.ydxiaoshuai.sample.aisample.baidu.model.FaceListBean;
import cn.ydxiaoshuai.sample.aisample.baidu.model.SkinAnalyzeResponseBean;
import cn.ydxiaoshuai.sample.aisample.baidu.model.VehicleDetectResponseBean;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * @author 小帅丶
 * @className DrawRectUtil
 * @Description 图片处理工具类
 * @Date 2020/4/10-14:55
 **/
public class DrawRectUtil {
    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2021年7月23日10:45:23
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     *             width = right-left
     *             height = bootom-top
     * @return String 处理后的base64
     **/
    public static FaceListBean cuttingBySkinAnalyzeDetect(byte[] imageBytes, SkinAnalyzeResponseBean bean) throws Exception{
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageBytes);
        FaceListBean cut = new FaceListBean();
        BufferedImage image = ImageIO.read(byteArrayInputStream);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.BOLD, 24);
        if (bean.getResult().getFace_num()>0) {
            for (int i = 0; i < bean.getResult().getFace_list().size(); i++) {
                SkinAnalyzeResponseBean.ResultBean.FaceListBean faceListBean = bean.getResult().getFace_list().get(i);
                Color color = getZDColor(i);
                int left = (int)faceListBean.getLocation().getLeft();
                int top = (int)faceListBean.getLocation().getTop();
                int width = faceListBean.getLocation().getWidth();
                int height = faceListBean.getLocation().getHeight();
                g2d.setColor(color);
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawRect(left,top,width,height);
                g2d.setColor(Color.BLACK);
                g2d.drawString(""+(i+1),left,top);
                g2d.setFont(font);
                //单人脸图
                cut = OperateImage.cut(imageBytes, faceListBean);
            }
            //皱纹
            String reactByWrinkle = getReactByWrinkle(imageBytes, bean);
            cut.setImage_wrinkle(reactByWrinkle);
            //斑痘痣
            String reactByAcnespotmole = getReactByAcnespotmole(imageBytes, bean);
            cut.setImage_acnespotmole(reactByAcnespotmole);
            //黑眼圈
            String reactEyesAttr = getReactEyesAttr(imageBytes, bean);
            cut.setImage_eyesattr(reactEyesAttr);
        }
        return cut;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片进行标注-皱纹
     * @Date  2020年6月17日14:29:38
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     * @return String 处理后的base64
     **/
    public static String getReactByWrinkle(byte[] imageBytes, SkinAnalyzeResponseBean bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.setColor(Color.RED);
        g2d.setFont(font);
        List<List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.WrinkleBean.WrinkleDataBean>> wrinkle_data = bean.getResult().getFace_list().get(0).getWrinkle().getWrinkle_data();
        for (int i = 0; i < wrinkle_data.size(); i++) {
            for (int j = 0; j <wrinkle_data.get(i).size() ; j++) {
                List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.WrinkleBean.WrinkleDataBean> wrinkleDataBeans = wrinkle_data.get(i);
                int j2 = j+1;
                if(j2<wrinkleDataBeans.size()){
                    g2d.drawLine(wrinkleDataBeans.get(j).getX(), wrinkleDataBeans.get(j).getY(), wrinkleDataBeans.get(j2).getX(), wrinkleDataBeans.get(j2).getY());
                }
            }
        }
        g2d.setFont(font);
        g2d.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64.encode(outputStream.toByteArray());
        return base64Img;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片增加框-斑痘痣
     * @Date  2020年4月10日14:57:47
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     *             width = right-left
     *             height = bootom-top
     * @return String 处理后的base64
     **/
    public static String getReactByAcnespotmole(byte[]  imageBytes, SkinAnalyzeResponseBean bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.AcnespotmoleBean.AcneListBean> acne_list = bean.getResult().getFace_list().get(0).getAcnespotmole().getAcne_list();
        List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.AcnespotmoleBean.SpeckleListBean> speckle_list = bean.getResult().getFace_list().get(0).getAcnespotmole().getSpeckle_list();
        List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.AcnespotmoleBean.MoleListBean> mole_list = bean.getResult().getFace_list().get(0).getAcnespotmole().getMole_list();
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        //痘信息
        for (int i = 0; i < acne_list.size(); i++) {
            g2d.setColor(new Color(0, 129, 255));
            int width  = (int)acne_list.get(i).getRight()-(int)acne_list.get(i).getLeft();
            int height = (int)acne_list.get(i).getBottom()-(int)acne_list.get(i).getTop();
            g2d.setStroke(new BasicStroke(2.0f));
            g2d.drawRect((int)acne_list.get(i).getLeft(), (int)acne_list.get(i).getTop(),width, height);
        }
        //斑信息
        for (int i = 0; i < speckle_list.size(); i++) {
            g2d.setColor(new Color(57, 181, 74));
            int speckleWidth  = (int)speckle_list.get(i).getRight()-(int)speckle_list.get(i).getLeft();
            int speckleHeight = (int)speckle_list.get(i).getBottom()-(int)speckle_list.get(i).getTop();
            g2d.setStroke(new BasicStroke(2.0f));
            g2d.drawRect((int)speckle_list.get(i).getLeft(), (int)speckle_list.get(i).getTop(),speckleWidth, speckleHeight);
        }
        //痣信息
        for (int i = 0; i < mole_list.size(); i++) {
            g2d.setColor(new Color(229, 77, 66));
            int width  = (int)mole_list.get(i).getRight()-(int)mole_list.get(i).getLeft();
            int height = (int)mole_list.get(i).getBottom()-(int)mole_list.get(i).getTop();
            g2d.setStroke(new BasicStroke(2.0f));
            g2d.drawRect((int)mole_list.get(i).getLeft(), (int)mole_list.get(i).getTop(),width, height);
        }
        g2d.setFont(font);
        g2d.setColor(Color.white);
        //g.drawString(acne_list.size()+"个痘,"+speckle_list.size()+"个斑,"+mole_list.size()+"个痣",0,20);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64.encode(outputStream.toByteArray());
        return base64Img;
    }


    /**
     * @Author 小帅丶
     * @Description 给图片增加框-黑眼圈眼袋
     * @Date  2020年8月26日17:49:05
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top
     * @return void
     **/
    public static String getReactEyesAttr(byte[] imageBytes, SkinAnalyzeResponseBean bean) throws Exception{
        long startTime = System.currentTimeMillis();
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics2D g = (Graphics2D) image.getGraphics();
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        g.setStroke(new BasicStroke(2.5f));
        g.setColor(Color.RED);
        g.setFont(font);
        //左右黑眼圈对象
        List<List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleLeftBean>> dark_circle_left = bean.getResult().getFace_list().get(0).getEyesattr().getDark_circle_left();
        List<List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleRightBean>> dark_circle_right = bean.getResult().getFace_list().get(0).getEyesattr().getDark_circle_right();
        //左右眼袋对象
        List<List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsLeftBean>> eye_bags_left = bean.getResult().getFace_list().get(0).getEyesattr().getEye_bags_left();
        List<List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsRightBean>> eye_bags_right = bean.getResult().getFace_list().get(0).getEyesattr().getEye_bags_right();
        //左黑眼圈
        for (int i = 0; i < dark_circle_left.size(); i++) {
            for (int j = 0; j <dark_circle_left.get(i).size() ; j++) {
                List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleLeftBean> darkCircleLeftBeans = dark_circle_left.get(i);
                int j2 = j+1;
                if(j2<darkCircleLeftBeans.size()){
                    g.drawLine(darkCircleLeftBeans.get(j).getX(), darkCircleLeftBeans.get(j).getY(), darkCircleLeftBeans.get(j2).getX(), darkCircleLeftBeans.get(j2).getY());
                }
            }
        }
        //右黑眼圈
        for (int i = 0; i < dark_circle_right.size(); i++) {
            for (int j = 0; j <dark_circle_right.get(i).size() ; j++) {
                List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleRightBean> darkCircleRightBeans = dark_circle_right.get(i);
                int j2 = j+1;
                if(j2<darkCircleRightBeans.size()){
                    g.drawLine(darkCircleRightBeans.get(j).getX(), darkCircleRightBeans.get(j).getY(), darkCircleRightBeans.get(j2).getX(), darkCircleRightBeans.get(j2).getY());
                }
            }
        }
        //左眼袋
        for (int i = 0; i < eye_bags_left.size(); i++) {
            for (int j = 0; j <eye_bags_left.get(i).size() ; j++) {
                List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsLeftBean> eyeBagsLeftBeans = eye_bags_left.get(i);
                int j2 = j+1;
                if(j2<eyeBagsLeftBeans.size()){
                    g.drawLine(eyeBagsLeftBeans.get(j).getX(), eyeBagsLeftBeans.get(j).getY(), eyeBagsLeftBeans.get(j2).getX(), eyeBagsLeftBeans.get(j2).getY());
                }
            }
        }
        //右眼袋
        for (int i = 0; i < eye_bags_right.size(); i++) {
            for (int j = 0; j <eye_bags_right.get(i).size() ; j++) {
                List<SkinAnalyzeResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsRightBean> eyeBagsRightBeans = eye_bags_right.get(i);
                int j2 = j+1;
                if(j2<eyeBagsRightBeans.size()){
                    g.drawLine(eyeBagsRightBeans.get(j).getX(), eyeBagsRightBeans.get(j).getY(), eyeBagsRightBeans.get(j2).getX(), eyeBagsRightBeans.get(j2).getY());
                }
            }
        }
        g.setFont(font);
        g.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64.encode(outputStream.toByteArray());
        long endTime = System.currentTimeMillis();
        System.out.println("画框耗时:"+(endTime-startTime));
        return base64Img;
    }
    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2021年7月23日10:45:23
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     *             width = right-left
     *             height = bootom-top
     * @return String 处理后的base64
     **/
    public static String getReactByVehicleDetect(byte[] imageBytes, VehicleDetectResponseBean bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.PLAIN, 16);
        bean.getVehicle_info().forEach(info->{
            String type = info.getType();
            //小汽车-car
            if(type.equals("car")){
                VehicleDetectResponseBean.VehicleInfoBean.LocationBean location = info.getLocation();
                g2d.setColor(getZDColor(1));
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawString("小汽车",location.getLeft(),location.getTop());
                g2d.setFont(font);
                g2d.drawRect(location.getLeft(),location.getTop(),location.getWidth(),location.getHeight());
            }
            //卡车-truck
            if(type.equals("truck")){
                VehicleDetectResponseBean.VehicleInfoBean.LocationBean location = info.getLocation();
                g2d.setColor(getZDColor(2));
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawString("卡车",location.getLeft(),location.getTop());
                g2d.setFont(font);
                g2d.drawRect(location.getLeft(),location.getTop(),location.getWidth(),location.getHeight());
            }
            //巴士-bus
            if(type.equals("bus")){
                VehicleDetectResponseBean.VehicleInfoBean.LocationBean location = info.getLocation();
                g2d.setColor(getZDColor(3));
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawString("巴士",location.getLeft(),location.getTop());
                g2d.setFont(font);
                g2d.drawRect(location.getLeft(),location.getTop(),location.getWidth(),location.getHeight());
            }
            //摩托车-motorbike
            if(type.equals("motorbike")){
                VehicleDetectResponseBean.VehicleInfoBean.LocationBean location = info.getLocation();
                g2d.setColor(getZDColor(4));
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawString("摩托车",location.getLeft(),location.getTop());
                g2d.setFont(font);
                g2d.drawRect(location.getLeft(),location.getTop(),location.getWidth(),location.getHeight());
            }
            //三轮车-tricycle
            if(type.equals("tricycle")){
                VehicleDetectResponseBean.VehicleInfoBean.LocationBean location = info.getLocation();
                g2d.setColor(getZDColor(5));
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawString("三轮车",location.getLeft(),location.getTop());
                g2d.setFont(font);
                g2d.drawRect(location.getLeft(),location.getTop(),location.getWidth(),location.getHeight());
            }
            //车牌的数量-tricycle
            if(type.equals("carplate")){
                VehicleDetectResponseBean.VehicleInfoBean.LocationBean location = info.getLocation();
                g2d.setColor(getZDColor(6));
                g2d.setStroke(new BasicStroke(3.0f));
                g2d.drawString("车牌",location.getLeft(),location.getTop());
                g2d.setFont(font);
                g2d.drawRect(location.getLeft(),location.getTop(),location.getWidth(),location.getHeight());
            }
        });
        Font fontText = new Font("微软雅黑", Font.BOLD, 18);
        g2d.setFont(fontText);
        g2d.setColor(Color.white);
        g.drawString(bean.getVehicle_num().getCar()+" 辆小汽车,"
                +bean.getVehicle_num().getTruck()+" 辆卡车,"
                +bean.getVehicle_num().getBus()+" 辆巴士,"
                +bean.getVehicle_num().getMotorbike()+" 辆摩托车,"
                +bean.getVehicle_num().getTricycle()+" 辆三轮车,"
                +bean.getVehicle_num().getCarplate()+" 个车牌,",10,20);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64.encode(outputStream.toByteArray());
        return base64Img;
    }




    /**
     * @Author 小帅丶
     * @Description 根据索引给颜色
     * @Date  2020/10/10 11:56
     * @param i 索引值
     * @return java.awt.Color
     **/
    private static Color getZDColor(int i) {
        Color color = null;
        if(i==0){
            color = new Color(141,198,63);
        }
        if(i==1){
            color = new Color(0,129,255);
        }
        if(i==2){
            color = new Color(103,57,182);
        }
        if(i==3){
            color = new Color(224,57,151);
        }
        if(i==4){
            color = new Color(165,103,63);
        }
        if(i==5){
            color = new Color(57,181,74);
        }
        if(i==6){
            color = new Color(229,77,66);
        }
        if(i==7){
            color = new Color(243,123,29);
        }
        if(i==8){
            color = new Color(251,189,8);
        }
        if(i==9){
            color = new Color(28,187,180);
        }
        return color;
    }
}
