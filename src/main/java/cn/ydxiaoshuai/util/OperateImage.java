package cn.ydxiaoshuai.util;

import cn.hutool.core.codec.Base64;
import cn.ydxiaoshuai.sample.aisample.baidu.model.FaceListBean;
import cn.ydxiaoshuai.sample.aisample.baidu.model.SkinAnalyzeResponseBean;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author 小帅丶
 * @className OperateImage
 * @Description 图像裁剪
 * @Date 2020/10/10-11:44
 **/
public class OperateImage {
    /**
     * 对图片裁剪，并把裁剪完成新图片保存 。
     */
    public static FaceListBean cut(byte[] imageBytes, SkinAnalyzeResponseBean.ResultBean.FaceListBean faceListBean) throws IOException {
        FaceListBean bean = new FaceListBean();
        List<String> image_base64 = new ArrayList<>();
        // 读取图片文件
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageBytes);
        ImageInputStream iis = null;
        Rectangle rect;
        try {
            /*
             * 返回包含所有当前已注册 ImageReader 的 Iterator，这些 ImageReader
             * 声称能够解码指定格式。 参数：formatName - 包含非正式格式名称 .
             *（例如 "jpeg" 或 "tiff"）等 。
             */
            Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName("jpg");
            ImageReader reader = it.next();
            // 获取图片流
            iis = ImageIO.createImageInputStream(byteArrayInputStream);
            /*
             * <p>iis:读取源.true:只向前搜索 </p>.将它标记为 ‘只向前搜索'。
             * 此设置意味着包含在输入源中的图像将只按顺序读取，可能允许 reader
             * 避免缓存包含与以前已经读取的图像关联的数据的那些输入部分。
             */
            reader.setInput(iis, true);
            /*
             * <p>描述如何对流进行解码的类<p>.用于指定如何在输入时从 Java Image I/O
             * 框架的上下文中的流转换一幅图像或一组图像。用于特定图像格式的插件
             * 将从其 ImageReader 实现的 getDefaultReadParam 方法中返回
             * ImageReadParam 的实例。
             */
            ImageReadParam param = reader.getDefaultReadParam();

            /*
             * 图片裁剪区域。Rectangle 指定了坐标空间中的一个区域，通过 Rectangle 对象
             * 的左上顶点的坐标（x，y）、宽度和高度可以定义这个区域。
             */
            int left = (int) faceListBean.getLocation().getLeft();
            int top = (int) faceListBean.getLocation().getTop();
            int width = faceListBean.getLocation().getWidth();
            int height = faceListBean.getLocation().getHeight();
            rect = new Rectangle(left, top, width, height);
            // 提供一个 BufferedImage，将其用作解码像素数据的目标。
            param.setSourceRegion(rect);
            /*
             * 使用所提供的 ImageReadParam 读取通过索引 imageIndex 指定的对象，并将
             * 它作为一个完整的 BufferedImage 返回。
             */
            BufferedImage bi = reader.read(0, param);
            // 保存新图片
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(bi, "jpg", outputStream);
            String base64Img = Base64.encode(outputStream.toByteArray());
            image_base64.add(base64Img);
        } finally {
            if (iis != null) {
                iis.close();
            }
        }
        bean.setImage_base64(image_base64);
        return bean;
    }
}
