package cn.ydxiaoshuai.gis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Description
 * ProjectName workdemo
 * Created by 小帅丶 on 2023-04-17 17:09.
 * Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CoordinateBean {
    private BigDecimal coordLngX;
    private BigDecimal coordLatY;
}
